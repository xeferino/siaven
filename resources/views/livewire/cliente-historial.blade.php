<div class="p-6 border-t-4 border-orange-400 shadow-md sm:rounded-lg">
    <x-table>
        <x-slot name="title">
            <a href="{{ route('clientes') }}" wire:navigate>
                <button type="button" class="text-white bg-orange-400 hover:bg-orange-500 focus:ring-4 focus:outline-none focus:ring-orange-300 font-medium rounded-lg text-xs px-2 py-2 text-center inline-flex items-center me-2">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M9 15 3 9m0 0 6-6M3 9h12a6 6 0 0 1 0 12h-3" />
                      </svg>                      
                </button> 
            </a>  
            {{ 'Cliente #'.str_pad($cliente->id, 6, "0", STR_PAD_LEFT). ' - '.$cliente->razon_social }} 
            <p class="mt-1 text-sm font-normal text-gray-500">Explora la lista de historial de compra.</p>
        </x-slot>
        <x-slot name="head">
            <th scope="col" class="px-6 py-3">
                Venta
            </th>
            <th scope="col" class="px-6 py-3">
                Fecha
            </th>
            <th scope="col" class="px-6 py-3">
                Tipo documento
            </th>
            <th scope="col" class="px-6 py-3">
                Nro documento
            </th>
            <th scope="col" class="px-6 py-3">
                Sucursal
            </th>
            <th scope="col" class="px-6 py-3">
                Neto
            </th>
            <th scope="col" class="px-6 py-3">
                Descuento
            </th>
            <th scope="col" class="px-6 py-3">
                Total
            </th>
            <th scope="col" class="px-6 py-3">
                Pagada
            </th>
            <th scope="col" class="px-6 py-3">
                Forma de pago
            </th>
            <th scope="col" class="px-6 py-3">
                Fecha de pago
            </th>
            <th scope="col" class="px-6 py-3">
                <span class="">Acciones</span>
            </th>
        </x-slot>
        <x-slot name="body">
            @php
                $totalVenta = 0;
            @endphp
            @forelse ($ventas as $item)
                <tr class="bg-white border-b  {{ $item->estado ? 'bg-red-200' : 'hover:bg-orange-100' }}">
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ '#'.str_pad($item->id, 6, "0", STR_PAD_LEFT) }} 
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ \Carbon\Carbon::parse($item->fecha)->format('d/m/Y') }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ $item->documento->nombre ?? 'Sin documento' }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ $item->nro_doc }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ $item->sucursal->nombre ?? null }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{moneda($item->total($item->id)) }}
                    </td>
                    <td scope="row"  class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ moneda($item->descuento ?? 0) }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ moneda($item->total($item->id)-$item->descuento) }}
                    </td>
                    <td scope="row" wire:click="alerta('{{ $item->id }}', '{{ $item->total($item->id)-$item->descuento }}')" class="px-6 py-4 text-gray-900 hover:bg-orange-300 whitespace-nowrap">
                        {{ $item->pagada ? 'Si' : 'No' }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ $item->pago->nombre ?? 'Credito' }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ \Carbon\Carbon::parse($item->fecha_pago)->format('d/m/Y') }}
                    </td>           
                    <td class="px-2 py-2 text-right cursor-pointer text-orange-600 inline-block">
                       
                        <a href="{{ route('ventas.detalles', ['id' => $item->id]) }}" wire:navigate>
                            <svg   xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                                <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                            </svg>
                        </a>
                    </td>
                </tr>
                @php
                    $totalVenta+=($item->total($item->id)-$item->descuento);
                @endphp
            @empty
                <tr class="bg-white text-center">
                    <td colspan="12" class="px-6 py-4">
                        No hay ventas registradas
                    </td>
                </tr>
            @endforelse
            <tr>
                <th colspan="7" class="py-1 px-1 text-sm">
                    Total 
                </th>
                <th  colspan="6" class="py-1 px-1 text-sm">
                    {{ moneda($totalVenta) }}
                </th>
            </tr>
        </x-slot>
    </x-table>
</div>
@push('js')
<script>
    tailwindButton = Swal.mixin({
    customClass: {
        confirmButton: "px-4 py-2 mr-2 bg-orange-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-orange-600 focus:bg-orange-600 active:bg-orange-600 focus:outline-none focus:ring-orange-500 transition ease-in-out duration-150",
        cancelButton: "px-4 py-2 bg-gray-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-600 focus:bg-gray-600 active:bg-gray-600 focus:outline-none focus:ring-gray-500 transition ease-in-out duration-150",
    },
    buttonsStyling: false
    });

    window.addEventListener('alerta', event => {
        Swal.fire(event.detail[0]);
    });

    window.addEventListener('compra-alerta', event => {
        tailwindButton.fire(event.detail[0]).then((result) => {
            if (result.isConfirmed) {
                @this.dispatch('pagada');
            }
        });
    });

</script>
@endpush