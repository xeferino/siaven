<div class="p-6 border-t-4 border-orange-400 shadow-md sm:rounded-lg">
    <div class="mb-2">
        <h1 class="mb-2 text-lg font-semibold text-left rtl:text-right text-gray-900 bg-white">
            Solicitudes de productos sucursales 
             @if (!empty($resultados))
             <button onclick="printJS({
                printable: 'printJS-informe', 
                header: '<center><h3>{{ 'Solicitudes de productos desde  '.\Carbon\Carbon::parse($desde)->format('d/m/Y'). ' hasta '. \Carbon\Carbon::parse($hasta)->format('d/m/Y')}}</h3></center>',
                type: 'html',
                style: 'table {font-family: Be Vietnam, ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont; color:#383739; width:100%; border-width: 1px; border-color: #0099A8; border-collapse: collapse;} table th {font-size:10px;background-color:#0099A8; color:#fff; padding: 5px; text-align:left; border:none;}  table tr {background-color:#fff; } table td {font-size:10px; padding: 8px; border:none;}'
            })"
                type="button" class="text-white bg-orange-400 hover:bg-orange-500 focus:ring-4 focus:outline-none focus:ring-orange-300 font-medium rounded-lg text-sm px-1 py-1 text-left inline-flex items-center">                
                Imprimir informe
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M6.72 13.829c-.24.03-.48.062-.72.096m.72-.096a42.415 42.415 0 0 1 10.56 0m-10.56 0L6.34 18m10.94-4.171c.24.03.48.062.72.096m-.72-.096L17.66 18m0 0 .229 2.523a1.125 1.125 0 0 1-1.12 1.227H7.231c-.662 0-1.18-.568-1.12-1.227L6.34 18m11.318 0h1.091A2.25 2.25 0 0 0 21 15.75V9.456c0-1.081-.768-2.015-1.837-2.175a48.055 48.055 0 0 0-1.913-.247M6.34 18H5.25A2.25 2.25 0 0 1 3 15.75V9.456c0-1.081.768-2.015 1.837-2.175a48.041 48.041 0 0 1 1.913-.247m10.5 0a48.536 48.536 0 0 0-10.5 0m10.5 0V3.375c0-.621-.504-1.125-1.125-1.125h-8.25c-.621 0-1.125.504-1.125 1.125v3.659M18 10.5h.008v.008H18V10.5Zm-3 0h.008v.008H15V10.5Z" />
                </svg>
                                
            </button>
             @endif
             <p class="mt-1 text-sm font-normal text-gray-500">
                Stock solicitado de productos @if (!empty($resultados)) {{ \Carbon\Carbon::parse($desde)->format('d/m/Y'). ' - '.\Carbon\Carbon::parse($hasta)->format('d/m/Y') }} @endif
             </p>
        </h1>
    </div>
    <div class="grid grid-cols-3 gap-4 mb-4">
        <div>
            <x-input-label for="desde" :value="__('Desde')" />
            <x-text-input wire:model.live="desde" id="desde" class="block w-full" type="date" name="desde" required autofocus autocomplete="desde" />
            <x-input-error :messages="$errors->get('desde')" class="mt-2" />
        </div> 
        <div>
            <x-input-label for="hasta" :value="__('Hasta')" />
            <x-text-input wire:model.live="hasta" id="hasta" class="block w-full" type="date" name="hasta" required autofocus autocomplete="hasta" />
            <x-input-error :messages="$errors->get('hasta')" class="mt-2" />
        </div>      
        <div>
            <x-primary-button class="mt-6"  wire:click="buscar" wire:loading.attr="disabled" wire:loading.remove>
                {{ __('Buscar') }}
            </x-primary-button>
            <x-primary-button class="mt-6" wire:loading wire:click="buscar" wire:loading.attr="disabled">
                {{ __('Cargando...') }}
            </x-primary-button>
        </div>
    </div>
    @if (!empty($resultados))
        <div id="printJS-informe">
            <x-table>
                <x-slot name="title">
                </x-slot>
                <x-slot name="head">

                    <th scope="col" class="px-6 py-3">
                        Producto
                    </th>
                   
                    @foreach ($sucursales as $item)
                        <th scope="col" class="px-6 py-3">
                            {{ $item->nombre }}
                        </th>
                    @endforeach      
                    <th scope="col" class="px-6 py-3">
                        Total
                    </th>
                    
                </x-slot>
                <x-slot name="body">
                    @php
                        $totalInventario = 0;
                    @endphp
                    @foreach ($resultados as $item)
                        <tr class="bg-white border-b hover:bg-orange-100">
                            <td scope="col" class="px-6 py-3">
                                {{ $item['producto'] }}
                            </td>
                            
                            @foreach ($item['sucursales'] as $sucursal)
                                    <td scope="col" class="px-6 py-3">
                                        {{ $sucursal['cantidad'] ?? 0 }}
                                    </td>
                                    
                            @endforeach
                            <td scope="col" class="px-6 py-3">
                                {{ $item['total'] }}
                            </td>
                        </tr>
                    @endforeach
                </x-slot>
            </x-table>
        </div>
    @endif
</div>

@push('js')
    <script>
         document.addEventListener('livewire:navigated', () => {
        tailwindButton = Swal.mixin({
        customClass: {
            confirmButton: "px-4 py-2 mr-2 bg-orange-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-orange-600 focus:bg-orange-600 active:bg-orange-600 focus:outline-none focus:ring-orange-500 transition ease-in-out duration-150",
            cancelButton: "px-4 py-2 bg-gray-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-600 focus:bg-gray-600 active:bg-gray-600 focus:outline-none focus:ring-gray-500 transition ease-in-out duration-150",
        },
        buttonsStyling: false
        });

        window.addEventListener('alerta', event => {
            Swal.fire(event.detail[0]);
        });
    });
    </script>
@endpush