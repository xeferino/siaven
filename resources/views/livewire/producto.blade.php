<div class="p-6 border-t-4 border-orange-400 shadow-md sm:rounded-lg">
    <x-table>
        <x-slot name="title">
            <button wire:click="modalAction('open')" type="button" class="float-right text-white bg-orange-400 hover:bg-orange-500 focus:ring-4 focus:outline-none focus:ring-orange-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center me-2">
                <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 0 0-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 0 0-16.536-1.84M7.5 14.25 5.106 5.272M6 20.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0Zm12.75 0a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0Z" />
                </svg>
                Nuevo producto
            </button>   
            Productos
            <p class="mt-1 text-sm font-normal text-gray-500">Explora la lista de productos en el sistema.</p>
            <div class="grid grid-cols-3 gap-4 mb-4 px-1 mt-2">
                <div>
                    <x-input-label for="producto_id" :value="__('Sucursales')" />
                    <select wire:model.live="producto_id" id="producto_id" class="block w-full text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="producto_id" required autofocus autocomplete="producto_id" >
                        <option value="0">Todos los productos</option>
                        @foreach ($productosf as $item)
                            <option value="{{  $item->id }}" {{ ($producto_id ==  $item->id) ? 'selected' : '' }}>{{ $item->codigo. ' '. $item->nombre }}</option>
                        @endforeach
                    </select>
                    <x-input-error :messages="$errors->get('producto_id')" class="mt-2" />
                </div>
            </div>
        </x-slot>
        <x-slot name="head">
            <th scope="col" class="px-6 py-3">
                Codigo
            </th>
            <th scope="col" class="px-6 py-3">
                Nombre
            </th>
            <th scope="col" class="px-6 py-3">
                Descripcion
            </th>
            <th scope="col" class="px-6 py-3">
                Categoria
            </th>
            <th scope="col" class="px-6 py-3">
                Precio de compra
            </th>
            <th scope="col" class="px-6 py-3">
                Precio de venta
            </th>
            <th scope="col" class="px-6 py-3">
                Fecha de precio venta
            </th>
            <th scope="col" class="px-6 py-3">
                Margen
            </th>
            <th scope="col" class="px-6 py-3">
                Stock Actual
            </th>
            <th scope="col" class="px-6 py-3">
                Valorizaci&oacute;n
            </th>
            {{-- <th scope="col" class="px-6 py-3">
                Sucursal
            </th>
            <th scope="col" class="px-6 py-3">
                Proveedor
            </th> --}}
            <th scope="col" class="px-6 py-3">
                <span class="">Acciones</span>
            </th>
        </x-slot>
        <x-slot name="body">
            @php
                $totalInventario = 0;
            @endphp
            @forelse ($productos as $item)
                <tr class="bg-white border-b hover:bg-orange-100">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ $item->codigo }}
                    </th>
                    <td class="px-6 py-4">
                        {{ $item->nombre }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->descripcion }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->categoria->nombre ?? null }}
                    </td>
                    </td>
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ moneda(round($item->precio_compra ?? 0, 2)) }}
                    </th>
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ moneda(round($item->precios->precio_venta ?? 0, 2)) }}
                    </th>
                    <td class="px-6 py-4">
                        {{ \Carbon\Carbon::parse($item->precios->fecha)->format('d/m/Y') }}
                    </td>
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ moneda(round($item->precios->precio_venta-$item->precio_compra, 2)) }}
                    </th>
                    
                    <th wire:click="alerta('{{ $item->id }}', 'stock')" scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap hover:bg-gray-300">
                        {{ round($item->stock($item->id), 2) }}
                    </th>
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ moneda(round($item->precio_compra*$item->stock($item->id), 2))  }}
                    </th>
                    {{-- <td class="px-6 py-4">
                        {{ $item->sucursal->nombre ?? null }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->proveedor->razon_social ?? null }}
                    </td> --}}
                    <td class="px-2 py-2 text-right cursor-pointer text-orange-600 inline-block">
                        {{-- <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 18 9 11.25l4.306 4.306a11.95 11.95 0 0 1 5.814-5.518l2.74-1.22m0 0-5.94-2.281m5.94 2.28-2.28 5.941" />
                        </svg>
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 6 9 12.75l4.286-4.286a11.948 11.948 0 0 1 4.306 6.43l.776 2.898m0 0 3.182-5.511m-3.182 5.51-5.511-3.181" />
                        </svg> --}} 
                        <svg  wire:click.prevent="precio('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 18.75a60.07 60.07 0 0 1 15.797 2.101c.727.198 1.453-.342 1.453-1.096V18.75M3.75 4.5v.75A.75.75 0 0 1 3 6h-.75m0 0v-.375c0-.621.504-1.125 1.125-1.125H20.25M2.25 6v9m18-10.5v.75c0 .414.336.75.75.75h.75m-1.5-1.5h.375c.621 0 1.125.504 1.125 1.125v9.75c0 .621-.504 1.125-1.125 1.125h-.375m1.5-1.5H21a.75.75 0 0 0-.75.75v.75m0 0H3.75m0 0h-.375a1.125 1.125 0 0 1-1.125-1.125V15m1.5 1.5v-.75A.75.75 0 0 0 3 15h-.75M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Zm3 0h.008v.008H18V10.5Zm-12 0h.008v.008H6V10.5Z" />
                        </svg> 
                        <svg wire:click="editar('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L10.582 16.07a4.5 4.5 0 0 1-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 0 1 1.13-1.897l8.932-8.931Zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0 1 15.75 21H5.25A2.25 2.25 0 0 1 3 18.75V8.25A2.25 2.25 0 0 1 5.25 6H10" />
                        </svg>
                        <svg wire:click="historialPrecios('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M15.666 3.888A2.25 2.25 0 0 0 13.5 2.25h-3c-1.03 0-1.9.693-2.166 1.638m7.332 0c.055.194.084.4.084.612v0a.75.75 0 0 1-.75.75H9a.75.75 0 0 1-.75-.75v0c0-.212.03-.418.084-.612m7.332 0c.646.049 1.288.11 1.927.184 1.1.128 1.907 1.077 1.907 2.185V19.5a2.25 2.25 0 0 1-2.25 2.25H6.75A2.25 2.25 0 0 1 4.5 19.5V6.257c0-1.108.806-2.057 1.907-2.185a48.208 48.208 0 0 1 1.927-.184" />
                        </svg>
                        <svg wire:click.prevent="alerta('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                        </svg>
                    </td>
                </tr>
                @php
                    $totalInventario+=round($item->stock($item->id)*$item->precio_compra, 2);
                @endphp
                @empty
                    <tr class="bg-white text-center">
                        <td colspan="11" scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                            No hay productos registrados
                        </td>
                    </tr>
                @endforelse
                @if (count($productos)>0)
                    <tr class="bg-white">
                        <th colspan="9" scope="row" class="px-6 py-4 font-medium text-right text-gray-900 whitespace-nowrap">
                            Valor Inventario
                        </th>
                        <th colspan="2" scope="row" class="px-6 py-4 text-left font-medium text-gray-900 whitespace-nowrap">
                            {{ moneda(round($totalInventario), 2) }}
                        </th>
                    </tr>
                @endif
        </x-slot>
    </x-table>

    <div class="mt-2">
        {{ $productos->links() }}
    </div>
    @if ($vista == 'historial')
        <x-modal :show="$modal">
            <x-slot name="title">
                <h3 class="text-lg font-semibold text-gray-900">
                    Historial de precios
                </h3>
            </x-slot>
            <x-slot name="body">
                @livewire('historial', ['id' => $id, 'tipo' => 'producto'])
            </x-slot>
            <x-slot name="footer">
                <x-secondary-button wire:click="modalAction('close')">
                    Close
                </x-secondary-button>
            </x-slot>
        </x-modal>
    @endif

    @if ($vista == 'precio')
    <x-modal :show="$modal">
        <x-slot name="title">
            <h3 class="text-lg font-semibold text-gray-900">
                Precio Producto
            </h3>
        </x-slot>
        <x-slot name="body">
            <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                <div class="mt-2">
                    <x-input-label for="fecha" :value="__('Fecha de precio')" />
                    <x-text-input wire:model.blur="fecha" id="fecha" class="block mt-1 w-full" type="date" name="fecha" required autofocus autocomplete="fecha" />
                    <x-input-error :messages="$errors->get('fecha')" class="mt-2" />
                </div>
                <div class="mt-2">
                    <x-input-label for="precio_venta" :value="__('Precio de venta')" />
                    <x-text-input wire:model.blur="precio_venta" id="precio_venta" class="block mt-1 w-full" type="number" min="1" name="precio_venta" required autofocus autocomplete="precio_venta" />
                    <x-input-error :messages="$errors->get('precio_venta')" class="mt-2" />
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-secondary-button wire:click="modalAction('close')">
                Close
            </x-secondary-button>
            <x-primary-button wire:click="nuevoPrecio">
                Agregar
            </x-primary-button>
        </x-slot>
    </x-modal>
    @endif

    @if ($vista == null)
        <x-modal :show="$modal">
            <x-slot name="title">
                <h3 class="text-lg font-semibold text-gray-900">
                    {{ ($id) ? 'Editar Producto' : 'Nuevo Producto' }}
                </h3>
            </x-slot>
            <x-slot name="body">
                <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                    {{-- <div class="mt-1">
                        <x-input-label for="sucursal_id" :value="__('Sucursal')" />
                        <select wire:model.blur="sucursal_id" id="sucursal_id" class="block w-full mt-1 text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="sucursal_id" required autofocus autocomplete="sucursal_id" >
                            <option>--Seleccione--</option>
                            @foreach ($sucursales as $item)
                                <option value="{{  $item->id }}" {{ ($sucursal_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('sucursal_id')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="proveedor_id" :value="__('Proveedor')" />
                        <select wire:model.blur="proveedor_id" id="proveedor_id" class="block w-full mt-1 text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="proveedor_id" required autofocus autocomplete="proveedor_id" >
                            <option>--Seleccione--</option>
                            @foreach ($proveedores as $item)
                                <option value="{{  $item->id }}" {{ ($proveedor_id ==  $item->id) ? 'selected' : '' }}>{{  $item->razon_social }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('proveedor_id')" class="mt-2" />
                    </div> --}}
                    <div class="mt-1">
                        <x-input-label for="categoria_id" :value="__('Categorias')" />
                        <select wire:model.blur="categoria_id" id="categoria_id" class="block w-full mt-1 text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="categoria_id" required autofocus autocomplete="categoria_id" >
                            <option>--Seleccione--</option>
                            @foreach ($categorias as $item)
                                <option value="{{  $item->id }}" {{ ($categoria_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('categoria_id')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="codigo" :value="__('Codigo')" />
                        <x-text-input wire:model.blur="codigo" id="codigo" class="block mt-1 w-full" type="text" name="codigo" required autofocus autocomplete="codigo" />
                        <x-input-error :messages="$errors->get('codigo')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="nombre" :value="__('Nombre')" />
                        <x-text-input wire:model.blur="nombre" id="nombre" class="block mt-1 w-full" type="text" name="nombre" required autofocus autocomplete="nombre" />
                        <x-input-error :messages="$errors->get('nombre')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="descripcion" :value="__('Descripcion')" />
                        <x-text-input wire:model.blur="descripcion" id="descripcion" class="block mt-1 w-full" type="text" name="descripcion" required autofocus autocomplete="descripcion" />
                        <x-input-error :messages="$errors->get('descripcion')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="precio_venta" :value="__('Precio de venta')" />
                        <x-text-input wire:model.blur="precio_venta" id="precio_venta" class="block mt-1 w-full" type="number" min="1" name="precio_venta" required autofocus autocomplete="precio_venta" />
                        <x-input-error :messages="$errors->get('precio_venta')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="precio_compra" :value="__('Precio ultima compra')" />
                        <x-text-input wire:model.blur="precio_compra" id="precio_compra" class="block mt-1 w-full" type="number" min="1" name="precio_compra" required autofocus autocomplete="precio_compra" />
                        <x-input-error :messages="$errors->get('precio_compra')" class="mt-2" />
                    </div>
                </div>
            </x-slot>
            <x-slot name="footer">
                <x-secondary-button wire:click="modalAction('close')">
                    Close
                </x-secondary-button>
                <x-primary-button wire:click="guardar">
                    {{ ($id) ? 'Editar' : 'Guardar' }}
                </x-primary-button>
            </x-slot>
        </x-modal>
    @endif
</div>
@push('js')
<script>
    tailwindButton = Swal.mixin({
    customClass: {
        confirmButton: "px-4 py-2 mr-2 bg-orange-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-orange-600 focus:bg-orange-600 active:bg-orange-600 focus:outline-none focus:ring-orange-500 transition ease-in-out duration-150",
        cancelButton: "px-4 py-2 bg-gray-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-600 focus:bg-gray-600 active:bg-gray-600 focus:outline-none focus:ring-gray-500 transition ease-in-out duration-150",
    },
    buttonsStyling: false
    });

    window.addEventListener('alerta', event => {
        Swal.fire(event.detail[0]);
    });

    window.addEventListener('eliminar-producto', event => {
        tailwindButton.fire(event.detail[0]).then((result) => {
            if (result.isConfirmed) {
                @this.dispatch('eliminar');
            }
        });
    });

    window.addEventListener('stock-producto', event => {
        tailwindButton.fire(event.detail[0]).then((result) => {
            if (result.isConfirmed) {
                @this.dispatch('stock');
                @this.set('cantidad', result.value);
            }
        });
    });

</script>
@endpush