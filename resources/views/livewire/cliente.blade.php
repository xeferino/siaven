<div class="p-6 border-t-4 border-orange-400 shadow-md sm:rounded-lg">
    <x-table>
        <x-slot name="title">
            <button wire:click="modalAction('open')" type="button" class="float-right text-white bg-orange-400 hover:bg-orange-500 focus:ring-4 focus:outline-none focus:ring-orange-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center me-2">
                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                    <path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path>
                </svg>
                Nuevo cliente
            </button>   
            Clientes
            <p class="mt-1 text-sm font-normal text-gray-500">Explora la lista de clientes en el sistema.</p>
            <div class="grid grid-cols-3 gap-4 mb-4 px-1 mt-2">
                <div>
                    <x-input-label for="cliente_id" :value="__('Sucursales')" />
                    <select wire:model.live="cliente_id" id="cliente_id" class="block w-full text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="cliente_id" required autofocus autocomplete="cliente_id" >
                        <option value="0">Todos los clientes</option>
                        @foreach ($clientesf as $item)
                            <option value="{{  $item->id }}" {{ ($cliente_id ==  $item->id) ? 'selected' : '' }}>{{ $item->rut. ' '. $item->razon_social }}</option>
                        @endforeach
                    </select>
                    <x-input-error :messages="$errors->get('cliente_id')" class="mt-2" />
                </div>
            </div>
        </x-slot>
        <x-slot name="head">
            <th scope="col" class="px-6 py-3">
                RUT
            </th>
            <th scope="col" class="px-6 py-3">
                Razon Social
            </th>
            <th scope="col" class="px-6 py-3">
                Direccion
            </th>
            <th scope="col" class="px-6 py-3">
                Giro
            </th>
            <th scope="col" class="px-6 py-3">
                Contacto
            </th>
            <th scope="col" class="px-6 py-3">
                Email
            </th>
            <th scope="col" class="px-6 py-3">
                Sucursal
            </th>
            <th scope="col" class="px-6 py-3">
                Monto Credito
            </th>
            <th scope="col" class="px-6 py-3">
                Telefonos
            </th>
            <th scope="col" class="px-6 py-3">
                <span class="">Acciones</span>
            </th>
        </x-slot>
        <x-slot name="body">
            @forelse ($clientes as $item)
                <tr class="bg-white border-b hover:bg-orange-100">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ $item->rut }}
                    </th>
                    <td class="px-6 py-4">
                        {{ $item->razon_social }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->ciudad. ' /'.$item->direccion }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->giro }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->contacto }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->email }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->sucursal->nombre ?? null }}
                    </td>
                    <td class="px-2 py-2">
                        {{ moneda($item->credito->monto ?? 0) }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->telefono1. ' / '.$item->telefono2 }}
                    </td>
                    <td class="px-6 py-4 cursor-pointer text-orange-600 inline-block">
                        <svg wire:click="editar('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L10.582 16.07a4.5 4.5 0 0 1-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 0 1 1.13-1.897l8.932-8.931Zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0 1 15.75 21H5.25A2.25 2.25 0 0 1 3 18.75V8.25A2.25 2.25 0 0 1 5.25 6H10" />
                        </svg>
                        <a href="{{ route('clientes.historial', ['id' => $item->id]) }}" wire:navigate>
                            <svg   xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                                <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                            </svg>
                        </a>
                        <svg wire:click="agregarCredito('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M12 6v12m-3-2.818.879.659c1.171.879 3.07.879 4.242 0 1.172-.879 1.172-2.303 0-3.182C13.536 12.219 12.768 12 12 12c-.725 0-1.45-.22-2.003-.659-1.106-.879-1.106-2.303 0-3.182s2.9-.879 4.006 0l.415.33M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                        </svg>
                        <svg wire:click="historialCredito('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M15.666 3.888A2.25 2.25 0 0 0 13.5 2.25h-3c-1.03 0-1.9.693-2.166 1.638m7.332 0c.055.194.084.4.084.612v0a.75.75 0 0 1-.75.75H9a.75.75 0 0 1-.75-.75v0c0-.212.03-.418.084-.612m7.332 0c.646.049 1.288.11 1.927.184 1.1.128 1.907 1.077 1.907 2.185V19.5a2.25 2.25 0 0 1-2.25 2.25H6.75A2.25 2.25 0 0 1 4.5 19.5V6.257c0-1.108.806-2.057 1.907-2.185a48.208 48.208 0 0 1 1.927-.184" />
                        </svg>
                        <svg wire:click.prevent="alerta('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                        </svg>
                    </td>
                </tr>
            @empty
                <tr class="bg-white text-center">
                    <td colspan="10" scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        No hay clientes registrados
                    </td>
                </tr> 
            @endforelse
        </x-slot>
    </x-table>

    <div class="mt-2">
        {{ $clientes->links() }}
    </div>
    
    @if ($vista == 'historial')
        <x-modal :show="$modal">
            <x-slot name="title">
            </x-slot>
            <x-slot name="body">
                @livewire('historial', ['id' => $id, 'tipo' => 'cliente'])
            </x-slot>
            <x-slot name="footer">
                <x-secondary-button wire:click="modalAction('close')">
                    Close
                </x-secondary-button>
            </x-slot>
        </x-modal>
    @elseif ($vista == 'credito')
        <x-modal :show="$modal">
            <x-slot name="title">
                <h3 class="text-lg font-semibold text-gray-900">
                    Agregar credito
                </h3>
            </x-slot>
            <x-slot name="body">
                <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                    <div class="mt-2">
                        <x-input-label for="credito" :value="__('Monto del credito')" />
                        <x-text-input wire:model.blur="credito" id="credito" class="block mt-1 w-full" type="number" min="1" name="credito" required autofocus autocomplete="credito" />
                        <x-input-error :messages="$errors->get('credito')" class="mt-2" />
                    </div>
                </div>
            </x-slot>
            <x-slot name="footer">
                <x-secondary-button wire:click="modalAction('close')">
                    Close
                </x-secondary-button>
                <x-primary-button wire:click="guadarCredito">
                    Agregar Credito
                </x-primary-button>
            </x-slot>
        </x-modal>
    @else
        <x-modal :show="$modal">
            <x-slot name="title">
                <h3 class="text-lg font-semibold text-gray-900">
                    {{ ($id) ? 'Editar Cliente' : 'Nuevo Cliente' }}
                </h3>
            </x-slot>
            <x-slot name="body">
                <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                        {{-- <div class="mt-1">
                            <x-input-label for="sucursal_id" :value="__('Sucursal')" />
                            <select wire:model.blur="sucursal_id" id="sucursal_id" class="block w-full mt-1 text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="sucursal_id" required autofocus autocomplete="sucursal_id" >
                                <option>--Seleccione--</option>
                                @foreach ($sucursales as $item)
                                    <option value="{{  $item->id }}" {{ ($sucursal_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                                @endforeach
                            </select>
                            <x-input-error :messages="$errors->get('sucursal_id')" class="mt-2" />
                        </div> --}}
                        <div class="mt-2">
                            <x-input-label for="rut" :value="__('RUT')" />
                            <x-text-input wire:model.blur="rut" id="rut" class="block mt-1 w-full" type="text" name="rut" required autofocus autocomplete="rut" />
                            <x-input-error :messages="$errors->get('rut')" class="mt-2" />
                        </div>
                        <div class="mt-2">
                            <x-input-label for="razon_social" :value="__('Razon Social')" />
                            <x-text-input wire:model.blur="razon_social" id="razon_social" class="block mt-1 w-full" type="text" name="razon_social" required autofocus autocomplete="razon_social" />
                            <x-input-error :messages="$errors->get('razon_social')" class="mt-2" />
                        </div>
                        <div class="mt-2">
                            <x-input-label for="ciudad" :value="__('Ciudad')" />
                            <x-text-input wire:model.blur="ciudad" id="ciudad" class="block mt-1 w-full" type="text" name="ciudad" required autofocus autocomplete="ciudad" />
                            <x-input-error :messages="$errors->get('ciudad')" class="mt-2" />
                        </div>
                        <div class="mt-2">
                            <x-input-label for="giro" :value="__('Giro')" />
                            <x-text-input wire:model.blur="giro" id="giro" class="block mt-1 w-full" type="text" name="giro" required autofocus autocomplete="giro" />
                            <x-input-error :messages="$errors->get('giro')" class="mt-2" />
                        </div>
                        <div class="mt-2">
                            <x-input-label for="contacto" :value="__('Contacto')" />
                            <x-text-input wire:model.blur="contacto" id="contacto" class="block mt-1 w-full" type="text" name="contacto" required autofocus autocomplete="contacto" />
                            <x-input-error :messages="$errors->get('contacto')" class="mt-2" />
                        </div>
                        <div class="mt-2">
                            <x-input-label for="email" :value="__('Email')" />
                            <x-text-input wire:model.blur="email" id="email" class="block mt-1 w-full" type="text" name="email" required autofocus autocomplete="email" />
                            <x-input-error :messages="$errors->get('email')" class="mt-2" />
                        </div>
                        <div class="mt-2">
                            <x-input-label for="direccion" :value="__('Direccion')" />
                            <x-text-input wire:model.blur="direccion" id="direccion" class="block mt-1 w-full" type="text" name="direccion" required autofocus autocomplete="direccion" />
                            <x-input-error :messages="$errors->get('direccion')" class="mt-2" />
                        </div>
                        <div class="mt-2">
                            <x-input-label for="telefono1" :value="__('Telefono 1')" />
                            <x-text-input wire:model.blur="telefono1" id="telefono1" class="block mt-1 w-full" type="text" name="telefono1" required autofocus autocomplete="telefono1" />
                            <x-input-error :messages="$errors->get('telefono1')" class="mt-2" />
                        </div>
                        <div class="mt-2">
                            <x-input-label for="telefono2" :value="__('Telefono 2')" />
                            <x-text-input wire:model.blur="telefono2" id="telefono2" class="block mt-1 w-full" type="text" name="telefono2" required autofocus autocomplete="telefono2" />
                            <x-input-error :messages="$errors->get('telefono2')" class="mt-2" />
                        </div>
                        @if ($credito > 0)
                            <div class="mt-2">
                                <x-input-label for="credito" :value="__('Monto del credito')" />
                                <x-text-input wire:model.blur="credito" id="credito" class="block mt-1 w-full" type="number" min="1" name="credito" required autofocus autocomplete="credito" />
                                <x-input-error :messages="$errors->get('credito')" class="mt-2" />
                            </div>
                        @endif
                </div>
            </x-slot>
            <x-slot name="footer">
                <x-secondary-button wire:click="modalAction('close')">
                    Close
                </x-secondary-button>
                <x-primary-button wire:click="guardar">
                    {{ ($id) ? 'Editar' : 'Guardar' }}
                </x-primary-button>
            </x-slot>
        </x-modal>
    @endif
</div>
@push('js')
<script>
    tailwindButton = Swal.mixin({
    customClass: {
        confirmButton: "px-4 py-2 mr-2 bg-orange-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-orange-600 focus:bg-orange-600 active:bg-orange-600 focus:outline-none focus:ring-orange-500 transition ease-in-out duration-150",
        cancelButton: "px-4 py-2 bg-gray-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-600 focus:bg-gray-600 active:bg-gray-600 focus:outline-none focus:ring-gray-500 transition ease-in-out duration-150",
    },
    buttonsStyling: false
    });

    window.addEventListener('alerta', event => {
        Swal.fire(event.detail[0]);
    });

    window.addEventListener('eliminar-cliente', event => {
        tailwindButton.fire(event.detail[0]).then((result) => {
            if (result.isConfirmed) {
                @this.dispatch('eliminar');
            }
        });
    });
</script>
@endpush