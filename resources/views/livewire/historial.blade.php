<div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
    <x-table>
        <x-slot name="title">
            <p class="mt-1 text-sm font-normal text-gray-500">{{ $titulo }} en el sistema.</p>
        </x-slot>
        <x-slot name="head">
            @foreach ($columnas as $item)
                <th scope="col" class="px-6 py-3">
                   {{ $item}}
                </th>
            @endforeach
        </x-slot>
        <x-slot name="body">
            @foreach ($historial as $item)
                <tr class="bg-white border-b hover:bg-orange-100">
                    @if ($tipo == 'producto')
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                            {{ moneda($item->precio_venta) }}
                        </th>
                        <td class="px-6 py-4">
                            {{ $item->fecha }}
                        </td>
                    @endif
                    @if ($tipo == 'cliente')
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                            {{ $item->monto }}
                        </th>
                        <td class="px-6 py-4">
                            {{ $item->fecha }}
                        </td>
                    @endif
                </tr> 
            @endforeach
        </x-slot>
    </x-table>
    <div class="mt-2">
        {{ $historial->links() }}
    </div>
</div>