<div class="p-6 border-t-4 border-orange-400 shadow-md sm:rounded-lg">
    <x-table>
        <x-slot name="title">
            <button wire:click="modalAction('open')" type="button" class="float-right text-white bg-orange-400 hover:bg-orange-500 focus:ring-4 focus:outline-none focus:ring-orange-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center me-2">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 21v-8.25M15.75 21v-8.25M8.25 21v-8.25M3 9l9-6 9 6m-1.5 12V10.332A48.36 48.36 0 0 0 12 9.75c-2.551 0-5.056.2-7.5.582V21M3 21h18M12 6.75h.008v.008H12V6.75Z" />
                </svg>
                Nuevo tipo de pago
            </button>   
            Tipo de pagos
            <p class="mt-1 text-sm font-normal text-gray-500">Explora la lista de tipo de pagos en el sistema.</p>
        </x-slot>
        <x-slot name="head">
            <th scope="col" class="px-6 py-3">
                Nombre
            </th>
            <th scope="col" class="px-6 py-3">
                <span class="">Acciones</span>
            </th>
        </x-slot>
        <x-slot name="body">
            @forelse ($pagos as $item)
                <tr class="bg-white border-b hover:bg-orange-100">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ $item->nombre }}
                    </th>
                    <td class="px-6 py-4 text-right cursor-pointer text-orange-600 inline-block">
                        <svg wire:click="editar('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L10.582 16.07a4.5 4.5 0 0 1-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 0 1 1.13-1.897l8.932-8.931Zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0 1 15.75 21H5.25A2.25 2.25 0 0 1 3 18.75V8.25A2.25 2.25 0 0 1 5.25 6H10" />
                        </svg>
                        <svg wire:click.prevent="alerta('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                        </svg>
                    </td>
                </tr> 
            @empty
                <tr class="bg-white text-center">
                    <td colspan="2" class="px-6 py-4">
                        No hay tipo de pagos registrados
                    </td>
                </tr> 
            @endforelse
        </x-slot>
    </x-table>

    <div class="mt-2">
        {{ $pagos->links() }}
    </div>


    <x-modal :show="$modal">
        <x-slot name="title">
            <h3 class="text-lg font-semibold text-gray-900">
                {{ ($id) ? 'Editar tipo de pago' : 'Nuevo tipo de pago' }}
            </h3>
        </x-slot>
        <x-slot name="body">
            <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                <div class="mt-1">
                    <x-input-label for="nombre" :value="__('Nombre')" />
                    <x-text-input wire:model.blur="nombre" id="nombre" class="block mt-1 w-full" type="text" name="nombre" required autofocus autocomplete="nombre" />
                    <x-input-error :messages="$errors->get('nombre')" class="mt-2" />
                </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-secondary-button wire:click="modalAction('close')">
                Close
            </x-secondary-button>
            <x-primary-button wire:click="guardar">
                {{ ($id) ? 'Editar' : 'Guardar' }}
            </x-primary-button>
        </x-slot>
    </x-modal>
</div>
@push('js')
<script>
    tailwindButton = Swal.mixin({
    customClass: {
        confirmButton: "px-4 py-2 mr-2 bg-orange-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-orange-600 focus:bg-orange-600 active:bg-orange-600 focus:outline-none focus:ring-orange-500 transition ease-in-out duration-150",
        cancelButton: "px-4 py-2 bg-gray-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-600 focus:bg-gray-600 active:bg-gray-600 focus:outline-none focus:ring-gray-500 transition ease-in-out duration-150",
    },
    buttonsStyling: false
    });

    window.addEventListener('alerta', event => {
        Swal.fire(event.detail[0]);
    });

    window.addEventListener('eliminar-pago', event => {
        tailwindButton.fire(event.detail[0]).then((result) => {
            if (result.isConfirmed) {
                @this.dispatch('eliminar');
            }
        });
    });
</script>
@endpush