<div class="p-6 border-t-4 border-orange-400 shadow-md sm:rounded-lg">
    <x-table>
        <x-slot name="title">
            Ventas
            <button wire:click="modalAction('open')" type="button" class="text-white bg-orange-400 hover:bg-orange-500 focus:ring-4 focus:outline-none focus:ring-orange-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center me-2">
                <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 0 0-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 0 0-16.536-1.84M7.5 14.25 5.106 5.272M6 20.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0Zm12.75 0a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0Z" />
                </svg>
                Nueva venta
            </button>   
            <p class="mt-1 text-sm font-normal text-gray-500">Explora la lista de ventas en el sistema.</p>
            <div class="grid grid-cols-3 gap-4 mb-4 px-1 mt-2">
                <div>
                    <x-input-label for="cliente_idF" :value="__('Clientes')" />
                    <select wire:model.live="cliente_idF" id="cliente_idF" class="block w-full text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="cliente_idF" required autofocus autocomplete="cliente_idF" >
                        <option value="0">Todos los clientes</option>
                        @foreach ($clientes as $item)
                            <option value="{{  $item->id }}" {{ ($cliente_idF ==  $item->id) ? 'selected' : '' }}>{{ $item->rut. ' '. $item->razon_social }}</option>
                        @endforeach
                    </select>
                    <x-input-error :messages="$errors->get('cliente_idF')" class="mt-2" />
                </div>
            </div>
        </x-slot>
        <x-slot name="head">
            <th scope="col" class="px-6 py-3">
                Fecha
            </th>
            <th scope="col" class="px-6 py-3">
                Tipo documento
            </th>
            <th scope="col" class="px-6 py-3">
                Nro documento
            </th>
            <th scope="col" class="px-6 py-3">
                Cliente
            </th>
            <th scope="col" class="px-6 py-3">
                Sucursal
            </th>
            <th scope="col" class="px-6 py-3">
                Neto
            </th>
            <th scope="col" class="px-6 py-3">
                Descuento
            </th>
            <th scope="col" class="px-6 py-3">
                Total
            </th>
            {{-- <th scope="col" class="px-6 py-3">
                Cuadre
            </th>
            <th scope="col" class="px-6 py-3">
                Guia
            </th>
            <th scope="col" class="px-6 py-3">
                N&deg; factura
            </th> --}}
            <th scope="col" class="px-6 py-3">
                Pagada
            </th>
            <th scope="col" class="px-6 py-3">
                Forma de pago
            </th>
            <th scope="col" class="px-6 py-3">
                Fecha de pago
            </th>
            <th scope="col" class="px-6 py-3">
                Anulada
            </th>
            <th scope="col" class="px-6 py-3">
                <span class="">Acciones</span>
            </th>
        </x-slot>
        <x-slot name="body">
            @php
                $totalVenta = 0;
            @endphp
            @forelse ($ventas as $item)
                <tr class="bg-white border-b  {{ $item->estado ? 'bg-red-200' : 'hover:bg-orange-100' }}">
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ \Carbon\Carbon::parse($item->fecha)->format('d/m/Y') }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ $item->documento->nombre ?? 'Sin documento' }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ $item->nro_doc }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ $item->cliente->rut.' '.$item->cliente->razon_social }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ $item->sucursal->nombre ?? null }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{moneda($item->total($item->id)) }}
                    </td>
                    <td scope="row" wire:click="alerta('{{ $item->id }}', 'descuento')"  class="px-6 py-4 text-gray-900 hover:bg-orange-300 whitespace-nowrap">
                        {{ moneda($item->descuento ?? 0) }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ moneda($item->total($item->id)-$item->descuento) }}
                    </td>
                   {{--  <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ moneda($item->cuadreguia) }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ moneda(($item->total($item->id)-$item->descuento)+$item->cuadreguia) }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{$item->factura ?? 'S/F' }}
                    </td> --}}
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ $item->pagada ? 'Si' : 'No' }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ $item->pago->nombre ?? 'Credito' }}
                    </td>
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ \Carbon\Carbon::parse($item->fecha_pago)->format('d/m/Y') }}
                    </td> 
                    <td scope="row" class="px-6 py-4 text-gray-900 whitespace-nowrap">
                        {{ $item->estado ? 'Si' : 'No' }}
                    </td>           
                    <td class="px-2 py-2 text-right cursor-pointer text-orange-600 inline-block">
                       
                        <a href="{{ route('ventas.detalles', ['id' => $item->id]) }}" wire:navigate>
                            <svg   xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                                <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                            </svg>
                        </a>
                        <svg wire:click.prevent="editar('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L10.582 16.07a4.5 4.5 0 0 1-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 0 1 1.13-1.897l8.932-8.931Zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0 1 15.75 21H5.25A2.25 2.25 0 0 1 3 18.75V8.25A2.25 2.25 0 0 1 5.25 6H10" />
                        </svg>
                        <svg wire:click.prevent="alerta('{{ $item->id }}', 'anular')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M18.364 18.364A9 9 0 0 0 5.636 5.636m12.728 12.728A9 9 0 0 1 5.636 5.636m12.728 12.728L5.636 5.636" />
                        </svg>
                        <svg wire:click.prevent="alerta('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                        </svg>
                    </td>
                </tr>
                @php
                    $totalVenta+=($item->total($item->id)-$item->descuento);
                @endphp
            @empty
                <tr class="bg-white text-center">
                    <td colspan="15" class="px-6 py-4">
                        No hay ventas registradas
                    </td>
                </tr>
            @endforelse
            <tr>
                <th colspan="7" class="py-1 px-1 text-sm">
                    Total 
                </th>
                <th  colspan="6" class="py-1 px-1 text-sm">
                    {{ moneda($totalVenta) }}
                </th>
            </tr>
        </x-slot>
    </x-table>

    <div class="mt-2">
        {{ $ventas->links() }}
    </div>

    @if ($vista == null)
        <x-modal :show="$modal">
            <x-slot name="title">
                <h3 class="text-lg font-semibold text-gray-900">
                Nueva venta
                </h3>
            </x-slot>
            <x-slot name="body">
                <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                    @if (Auth::user()->type == 'admin')
                        <div class="mt-1">
                            <x-input-label for="sucursal_id" :value="__('Sucursal')" />
                            <select wire:model.live="sucursal_id" id="sucursal_id" class="block w-full text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="sucursal_id" required autofocus autocomplete="sucursal_id" >
                                <option>--Seleccione--</option>
                                @foreach ($sucursales as $item)
                                    <option value="{{  $item->id }}" {{ ($sucursal_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                                @endforeach
                            </select>
                            <x-input-error :messages="$errors->get('sucursal_id')" class="mt-2" />
                        </div>
                    @endif
                    <div class="mt-1">
                        <x-input-label for="fecha" :value="__('Fecha')" />
                        <x-text-input wire:model.live="fecha" id="fecha" class="block mt-1 w-full" type="date" name="fecha" required autofocus autocomplete="fecha" />
                        <x-input-error :messages="$errors->get('fecha')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="documento_id" :value="__('Tipo de documento')" />
                        <select wire:model.live="documento_id" id="documento_id" class="block w-full mt-1 text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="documento_id" required autofocus autocomplete="documento_id" >
                            <option>--Seleccione--</option>
                            @foreach ($documentos as $item)
                                <option value="{{  $item->id }}" {{ ($documento_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('documento_id')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="numero_documento" :value="__('Numero documento')" />
                        <x-text-input wire:model.live="numero_documento"  id="numero_documento" class="block mt-1 w-full" type="text" name="numero_documento" required autofocus autocomplete="numero_documento" />
                        <x-input-error :messages="$errors->get('numero_documento')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="cliente_id" :value="__('Clientes')" />
                        <select wire:model.live="cliente_id" id="cliente_id" class="block w-full mt-1 text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="cliente_id" required autofocus autocomplete="cliente_id" >
                            <option>--Seleccione--</option>
                            @foreach ($clientes as $item)
                                <option class="{{ isset($item->credito->monto) ? 'bg-green-500 text-white' : 'bg-red-500 text-white'}}" value="{{  $item->id }}" {{ ($cliente_id ==  $item->id) ? 'selected' : '' }}>{{  $item->rut.' - '.$item->razon_social .' Credito Disp.:'.moneda($item->credito->monto ?? 0) }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('cliente_id')" class="mt-2" />
                    </div>
                    
                    <div class="mt-1">
                        <x-input-label for="pago_id" :value="__('Tipo de pago')" />
                        <select wire:model.live="pago_id" id="pago_id" class="block w-full mt-1 text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="pago_id" required autofocus autocomplete="pago_id" >
                            <option>--Seleccione--</option>
                            @foreach ($pagos as $item)
                                <option value="{{  $item->id }}" {{ ($pago_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('pago_id')" class="mt-2" />
                    </div>
                </div>
            </x-slot>
            <x-slot name="footer">
                <x-secondary-button wire:click="modalAction('close')">
                    Close
                </x-secondary-button>
                    <x-primary-button wire:click="guardar">
                        Guardar
                    </x-primary-button>
            </x-slot>
        </x-modal>
    @endif

    @if ($vista == 'editar')
        <x-modal :show="$modal">
            <x-slot name="title">
                <h3 class="text-lg font-semibold text-gray-900">
                    Editar venta
                    <x-input-label for="pagada" :value="__($pagada == 'si' ? 'Pagada' : 'Pendiente')" 
                    class="
                    float-right
                    {{ $pagada == 'si' 
                    ? 'bg-green-100 text-green-800 text-xs font-medium me-2 px-2.5 py-0.5 rounded' 
                    : 'bg-red-100 text-red-800 text-xs font-medium me-2 px-2.5 py-0.5 rounded' 
                    }}
                    "/>
                </h3>
            </x-slot>
            <x-slot name="body">
                <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                    @if (Auth::user()->type == 'admin')
                        <div class="mt-1">
                            <x-input-label for="sucursal_id" :value="__('Sucursal')" />
                            <select wire:model.live="sucursal_id" id="sucursal_id" class="block w-full text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="sucursal_id" required autofocus autocomplete="sucursal_id" >
                                <option>--Seleccione--</option>
                                @foreach ($sucursales as $item)
                                    <option value="{{  $item->id }}" {{ ($sucursal_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                                @endforeach
                            </select>
                            <x-input-error :messages="$errors->get('sucursal_id')" class="mt-2" />
                        </div>
                    @endif
                    <div class="mt-1">
                        <x-input-label for="fecha" :value="__('Fecha')" />
                        <x-text-input wire:model.live="fecha" id="fecha" class="block mt-1 w-full" type="date" name="fecha" required autofocus autocomplete="fecha" />
                        <x-input-error :messages="$errors->get('fecha')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="documento_id" :value="__('Tipo de documento')" />
                        <select wire:model.live="documento_id" id="documento_id" class="block w-full mt-1 text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="documento_id" required autofocus autocomplete="documento_id" >
                            <option>--Seleccione--</option>
                            @foreach ($documentos as $item)
                                <option value="{{  $item->id }}" {{ ($documento_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('documento_id')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="numero_documento" :value="__('Numero documento')" />
                        <x-text-input wire:model.live="numero_documento"  id="numero_documento" class="block mt-1 w-full" type="text" name="numero_documento" required autofocus autocomplete="numero_documento" />
                        <x-input-error :messages="$errors->get('numero_documento')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="cliente_id" :value="__('Clientes')" />
                        <select wire:model.live="cliente_id" id="cliente_id" class="block w-full mt-1 text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="cliente_id" required autofocus autocomplete="cliente_id" >
                            <option>--Seleccione--</option>
                            @foreach ($clientes as $item)
                                <option class="{{ isset($item->credito->monto) ? 'bg-green-500 text-white' : 'bg-red-500 text-white'}}" value="{{  $item->id }}" {{ ($cliente_id ==  $item->id) ? 'selected' : '' }}>{{  $item->rut.' - '.$item->razon_social .' Credito Disp.:'.moneda($item->credito->monto ?? 0) }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('cliente_id')" class="mt-2" />
                    </div>
                    
                    <div class="mt-1">
                        <x-input-label for="pago_id" :value="__('Tipo de pago')" />
                        <select wire:model.live="pago_id" id="pago_id" class="block w-full mt-1 text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="pago_id" required autofocus autocomplete="pago_id" >
                            <option>--Seleccione--</option>
                            @foreach ($pagos as $item)
                                <option value="{{  $item->id }}" {{ ($pago_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('pago_id')" class="mt-2" />
                    </div>
                    <div class="flex gap-8 mt-1">
                        <div class="mt-1">
                            <x-input-label for="pagada" :value="__('Pagada')" />
                            <input id="inline-radio-pagada" type="radio" wire:model.live='pagada' value="si" name="inline-radio-pagada" class="w-4 h-4  text-orange-500 bg-gray-100 border-gray-300 focus:ring-orange-500">
                            <label for="inline-radio-pagada" class="text-sm font-medium text-gray-800">Si</label>
                            <input id="inline-2-radio-pagada" type="radio" wire:model.live='pagada' value="no" name="inline-radio-pagada" class="ml-4 w-4 h-4 text-orange-500 bg-gray-100 border-gray-300 focus:ring-orange-500">
                            <label for="inline-2-radio-pagada" class="text-sm font-medium text-gray-800">no</label>
                            <x-input-error :messages="$errors->get('pagada')" class="mt-2" />
                        </div>
                    </div>
                    <div class="mt-1">
                        <x-input-label for="fecha_pago" :value="__('Fecha pago')" />
                        <x-text-input wire:model.live="fecha_pago" id="fecha_pago" class="block mt-1 w-full" type="date" name="fecha_pago" required autofocus autocomplete="fecha_pago" />
                        <x-input-error :messages="$errors->get('fecha_pago')" class="mt-2" />
                    </div>
                </div>
            </x-slot>
            <x-slot name="footer">
                <x-secondary-button wire:click="modalAction('close')">
                    Close
                </x-secondary-button>
                    <x-primary-button wire:click="guardar">
                        Guardar
                    </x-primary-button>
            </x-slot>
        </x-modal>
    @endif
</div>
@push('js')
<script>
    tailwindButton = Swal.mixin({
    customClass: {
        confirmButton: "px-4 py-2 mr-2 bg-orange-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-orange-600 focus:bg-orange-600 active:bg-orange-600 focus:outline-none focus:ring-orange-500 transition ease-in-out duration-150",
        cancelButton: "px-4 py-2 bg-gray-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-600 focus:bg-gray-600 active:bg-gray-600 focus:outline-none focus:ring-gray-500 transition ease-in-out duration-150",
    },
    buttonsStyling: false
    });

    window.addEventListener('alerta', event => {
        Swal.fire(event.detail[0]);
    });

    window.addEventListener('venta-guardada', event => {
        Swal.fire(event.detail[0]);
        setTimeout(() => {
            @this.dispatch('venta');
        }, 3500);
    });

    window.addEventListener('eliminar-venta', event => {
        tailwindButton.fire(event.detail[0]).then((result) => {
            if (result.isConfirmed) {
                @this.dispatch('eliminar');
            }
        });
    });

    window.addEventListener('anular-venta', event => {
        tailwindButton.fire(event.detail[0]).then((result) => {
            if (result.isConfirmed) {
                @this.dispatch('anular');
            }
        });
    });

    window.addEventListener('descuento-venta', event => {
        tailwindButton.fire(event.detail[0]).then((result) => {
            if (result.isConfirmed) {
                @this.set('monto', result.value);
                @this.dispatch('saldo');
            }
        });
    });

</script>
@endpush