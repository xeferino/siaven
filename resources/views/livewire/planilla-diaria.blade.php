<div class="p-6 border-t-4 border-orange-400 shadow-md sm:rounded-lg">
    <div class="mb-2">
        <h1 class="mb-2 text-lg font-semibold text-left rtl:text-right text-gray-900 bg-white">
             Planilla Sucursal {{ $sucursal->nombre ?? '' }}
             @if ($sucursal) 
                <span class="text-white capitalize {{ ($estado == 'Sin rendir') ? 'bg-red-500' : 'bg-green-500' }} font-medium rounded-lg text-base px-1 py-1"> {{ $estado }} </span>
             @endif
             <p class="mt-1 text-base font-normal text-gray-500">Se muestran los saldos diarios para la fecha {{ \Carbon\Carbon::parse($fecha)->format('d/m/Y') }}</p>
        </h1>
    </div>
    @if (Auth::user()->type != 'sucursal')   
        <div class="grid grid-cols-3 gap-4 mb-4">
            <div>
                <x-input-label for="sucursal_id" :value="__('Sucursal')" />
                <select wire:model.live="sucursal_id" id="sucursal_id" class="block w-full text-base border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="sucursal_id" required autofocus autocomplete="sucursal_id" >
                    <option>--Seleccione--</option>
                    @foreach ($sucursales as $item)
                        <option value="{{  $item->id }}" {{ ($sucursal_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                    @endforeach
                </select>
                <x-input-error :messages="$errors->get('sucursal_id')" class="mt-2" />
            </div>
            <div>
                <x-input-label for="fecha" :value="__('Fecha')" />
                <x-text-input wire:model.live="fecha" id="fecha" class="block w-full" type="date" name="fecha" required autofocus autocomplete="fecha" />
                <x-input-error :messages="$errors->get('fecha')" class="mt-2" />
            </div>      
            <div>
                <x-primary-button class="mt-6"  wire:click="buscar" wire:loading.attr="disabled" wire:loading.remove>
                    {{ __('Cargar planilla') }}
                </x-primary-button>
                <x-primary-button class="mt-6" wire:loading wire:click="buscar" wire:loading.attr="disabled">
                    {{ __('Cargando...') }}
                </x-primary-button>
            </div>
        </div>
    @else
        <div class="grid grid-cols-3 gap-4 mb-4">
            <div>
                <x-input-label for="sucursal_id" :value="__('Sucursal')" />
                <select wire:model.live="sucursal_id" id="sucursal_id" class="block w-full text-base border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="sucursal_id" required autofocus autocomplete="sucursal_id" >
                    <option value="{{  $sucursal->id }}" {{ ($sucursal_id ==  $sucursal->id) ? 'selected' : '' }}>{{  $sucursal->nombre }}</option>
                </select>
                <x-input-error :messages="$errors->get('sucursal_id')" class="mt-2" />
            </div>
            <div>
                <x-input-label for="fecha" :value="__('Fecha')" />
                <x-text-input wire:model.live="fecha" id="fecha" class="block w-full" type="date" name="fecha" required autofocus autocomplete="fecha" />
                <x-input-error :messages="$errors->get('fecha')" class="mt-2" />
            </div>      
            <div>
                <x-primary-button class="mt-6"  wire:click="buscar" wire:loading.attr="disabled" wire:loading.remove>
                    {{ __('Cargar planilla') }}
                </x-primary-button>
                <x-primary-button class="mt-6" wire:loading wire:click="buscar" wire:loading.attr="disabled">
                    {{ __('Cargando...') }}
                </x-primary-button>
            </div>
        </div>
    @endif
   <!-- ====== Table Section Start -->
   @if (!empty($resultados))
        <div class="grid grid-cols-1 gap-4 mb-4" id="printJS-planilla">
            <div class="overflow-x-auto shadow-md sm:rounded-lg">
                <table class="w-full table-fixed">
                    <thead>
                        <tr class="text-left bg-gray-200">
                            <th class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Producto
                            </th>
                            <th class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Saldo anterior
                            </th>
                            <th class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Recibido
                            </th>
                            <th class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Saldo teórico
                            </th>
                            <th class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Saldo registrado
                            </th>
                            <th class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Venta calculada
                            </th>
                            <th class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Valor unitario
                            </th>
                            <th class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Total
                            </th> 
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $ventaDiaria = 0;
                        @endphp
                        @foreach ($categorias as $categoria)
                            @if ($categoria->productos()->count()>0)  
                                <tr class="text-left bg-orange-200">
                                    <th colspan="8" class="py-2 px-2 border text-base font-semibold text-gray-800">
                                        {{ $categoria->nombre }}
                                    </th>
                                </tr>                    
                            @endif
                            @foreach ($resultados as $item)
                                @if ($categoria->id == $item['producto']->categoria_id)
                                   {{--  @if ($item['saldo_teorico'] > 0) --}}
                                        <tr class="text-left bg-gray-100 hover:bg-orange-100">
                                            <td class="border-b border-l py-2 px-2 text-left text-base font-medium">
                                                {{ $item['nombre'] }}
                                            </td>
                                            <td class="border-b border-l py-2 px-2 text-left text-base font-medium">
                                                {{ moneda($item['saldo_anterior']) }}
                                            </td>
                                            <td class="border-b border-l py-2 px-2 text-left text-base font-medium">
                                                {{ $item['recibido'] }}
                                            </td>
                                            <td class="border-b border-l py-2 px-2 text-left text-base font-medium">
                                                {{ moneda($item['saldo_teorico']) }}
                                            </td>
                                            @if ($estado == 'rendida' or $estado == 'cerrada')
                                                <td class="border-b border-l py-2 px-2 text-left text-base font-medium">
                                                    {{ moneda($item['saldo_registrado']) }}
                                                </td>
                                            @else
                                                <td wire:click="alerta({{ $item['producto']->id }})" class="border-b border-l py-2 px-2 text-left text-base font-medium hover:bg-orange-300">
                                                    {{ moneda($item['saldo_registrado']) }}
                                                </td>
                                            @endif
                                            <td class="border-b border-l py-2 px-2 text-left text-base font-medium">
                                                {{ moneda($item['venta_calculda']) }}
                                            </td>
                                            <td class="border-b border-l py-2 px-2 text-left text-base font-medium">
                                                {{ moneda($item['valor_unitario']) }}
                                            </td>
                                            <td class="border-b border-l py-2 px-2 text-left text-base font-medium">
                                                {{ moneda($item['total']) }}
                                            </td>
                                        </tr>
                                        @php
                                            $ventaDiaria +=$item['venta_calculda']*$item['valor_unitario'];
                                        @endphp
                                    {{-- @endif --}}
                                @endif
                            @endforeach
                        @endforeach
                        <tr class="text-left bg-gray-100">
                            <td colspan="7" class="border-b border-l py-2 px-2 text-right text-base font-medium">
                                Venta Diaria
                            </td>
                            <td class="border-b border-l py-2 px-2 text-left text-base font-medium">
                            {{ moneda($ventaDiaria) }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="grid grid-cols-3 gap-4 mb-4">
            <div class="grid grid-cols-1 gap-4">
                <table class="bg-gray-100 border-t-4 border-red-400 shadow-md sm:rounded-lg">
                    <thead>
                        <tr class="text-center">
                            <th colspan="2" class="border text-base font-semibold text-gray-800">
                                Gastos
                                @if ($estado == 'abierta' or $estado == 'Sin rendir')
                                    <svg wire:click="vista('gasto')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="mr-2 px-1 py-1 text-white bg-green-400 hover:bg-green-500 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-base text-left  inline-flex items-center me-2 w-6 h-6">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                                    </svg>
                                @endif
                            </th>
                        </tr>
                        <tr class="text-left">
                            <th class="py-1 px-1 border text-base font-semibold text-gray-800">
                                Concepto
                            </th>
                            <th colspan="2" class="py-1 px-1 border text-base font-semibold text-gray-800">
                                Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $totalGasto = 0;
                        @endphp
                        @forelse ($gastos as $item)
                            <tr class="text-left">
                                <td class="border-b border-l py-1 px-1 text-left text-base">
                                    {{ $item->tipo->nombre ?? 'Diferencia de caja' }}
                                </td>
                                @if ($estado == 'rendida' or $estado == 'cerrada')
                                    <td class="border-b border-l py-2 px-2 text-left text-base">
                                        {{ moneda($item->monto) }}
                                    </td>
                                @else
                                    <td wire:click="alerta('{{ $item->id }}', 'gasto')" class="border-b border-l py-2 px-2 text-left text-base font-medium hover:bg-orange-300">
                                        {{ moneda($item->monto) }}
                                    </td>
                                @endif
                                @if ($estado == 'abierta' or $estado == 'Sin rendir')
                                    <td class="border-b border-l py-2 px-2 text-left text-base font-medium">
                                        <svg wire:click.prevent="alerta('{{ $item->id }}', 'gasto-eliminar')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block cursor-pointer">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                                        </svg>
                                    </td>
                                @endif
                            </tr>
                            @php
                                $totalGasto+=$item->monto;
                            @endphp
                        @empty
                            <tr class="text-left">
                                <td colspan="2" class="border-b border-l py-1 px-1 text-left text-base">
                                    No hay gastos registrados
                                </td>
                            </tr>
                        @endforelse
                        <tr>
                            <td class="border-b border-l py-1 px-1 text-base">
                                Total 
                            </td>
                            <td class="border-b border-l py-1 px-1 text-left text-base">
                                {{ moneda($totalGasto) }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="grid grid-cols-1 gap-4">
                <table class="bg-gray-100 border-t-4 border-blue-400 shadow-md sm:rounded-lg">
                    <thead>
                        <tr class="text-center">
                            <th colspan="2" class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Sueldos
                                @if ($estado == 'abierta' or $estado == 'Sin rendir')
                                    <svg wire:click="vista('sueldo')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="mr-2 px-1 py-1 text-white bg-green-400 hover:bg-green-500 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-base text-left  inline-flex items-center me-2 w-6 h-6">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                                    </svg>
                                @endif
                            </th>
                        </tr>
                        <tr class="text-left">
                            <th class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Concepto
                            </th>
                            <th colspan="2" class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $totalSueldo = 0;
                        @endphp
                        @forelse ($sueldos as $item)
                            <tr class="text-left">
                                <td class="border-b border-l py-2 px-2 text-left text-base">
                                    {{ $item->empleado->razon_social ?? null }}
                                </td>
                                @if ($estado == 'rendida' or $estado == 'cerrada')
                                    <td class="border-b border-l py-2 px-2 text-left text-base">
                                        {{ moneda($item->monto) }}
                                    </td>
                                @else
                                    <td wire:click="alerta('{{ $item->id }}', 'sueldo')" class="border-b border-l py-2 px-2 text-left text-base font-medium hover:bg-orange-300">
                                        {{ moneda($item->monto) }}
                                    </td>
                                @endif
                                @if ($estado == 'abierta' or $estado == 'Sin rendir')
                                    <td class="border-b border-l py-2 px-2 text-left text-base font-medium">
                                        <svg wire:click.prevent="alerta('{{ $item->id }}', 'sueldo-eliminar')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block cursor-pointer">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                                        </svg>
                                    </td>
                                @endif
                            </tr>
                            @php
                                $totalSueldo +=$item->monto;
                            @endphp
                        @empty
                            <tr class="text-left">
                                <td colspan="2" class="border-b border-l py-2 px-2 text-left text-base">
                                    No hay sueldos registrados
                                </td>
                            </tr>
                        @endforelse
                        <tr class="text-left">
                            <td class="border-b border-l py-2 px-2 text-base">
                                Total 
                            </td>
                            <td class="border-b border-l py-2 px-2 text-left text-base">
                                {{ moneda($totalSueldo) }}
                            </td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
            <div class="grid grid-cols-1 gap-4">
                <table class="bg-gray-100 border-t-4 border-green-400 shadow-md sm:rounded-lg">
                    <thead>
                        <tr class="text-center">
                            <th colspan="2" class="py-1 px-1 border text-base font-semibold text-gray-800">
                                Ingresos
                                @if ($estado == 'abierta' or $estado == 'Sin rendir')
                                    <svg wire:click="vista('ingreso')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="mr-2 px-1 py-1 text-white bg-green-400 hover:bg-green-500 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-base text-left  inline-flex items-center me-2 w-6 h-6">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                                    </svg>
                                @endif
                            </th>
                        </tr>
                        <tr class="text-left">
                            <th class="py-1 px-1 border text-base font-semibold text-gray-800">
                                Concepto
                            </th>
                            <th colspan="2" class="py-1 px-1 border text-base font-semibold text-gray-800">
                                Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $totalIngreso = 0;
                        @endphp
                        @forelse ($ingresos as $item)
                            <tr class="text-left">
                                <td class="border-b border-l py-1 px-1 text-left text-base">
                                    {{ $item->tipo->nombre ?? null }}
                                </td>
                                @if ($estado == 'rendida' or $estado == 'cerrada')
                                    <td class="border-b border-l py-2 px-2 text-left text-base">
                                        {{ moneda($item->monto) }}
                                    </td>
                                @else
                                    <td wire:click="alerta('{{ $item->id }}', 'ingreso')" class="border-b border-l py-2 px-2 text-left text-base font-medium hover:bg-orange-300">
                                        {{ moneda($item->monto) }}
                                    </td>
                                @endif
                                @if ($estado == 'abierta' or $estado == 'Sin rendir')
                                    <td class="border-b border-l py-2 px-2 text-left text-base font-medium">
                                        <svg wire:click.prevent="alerta('{{ $item->id }}', 'ingreso-eliminar')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block cursor-pointer">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                                        </svg>
                                    </td>
                                @endif
                            </tr>
                            @php
                                $totalIngreso+=$item->monto;
                            @endphp
                        @empty
                            <tr class="text-left">
                                <td colspan="2" class="border-b border-l py-1 px-1 text-left text-base">
                                    No hay ingresos registrados
                                </td>
                            </tr>
                        @endforelse
                        <tr>
                            <td class="border-b border-l py-1 px-1 text-base">
                                Total 
                            </td>
                            <td class="border-b border-l py-1 px-1 text-left text-base">
                                {{ moneda($totalIngreso) }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="grid grid-cols-2 gap-4 mb-4">
            <div class="grid grid-cols-1 gap-4">
                <table class="bg-gray-100 border-t-4 border-orange-400 shadow-md sm:rounded-lg">
                    <thead>
                        <tr class="text-center">
                            <th colspan="4" class="py-1 px-1 border text-base font-semibold text-gray-800">
                                Perdidas
                                @if ($estado == 'abierta' or $estado == 'Sin rendir')
                                    <svg wire:click="vista('perdida')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="mr-2 px-1 py-1 text-white bg-green-400 hover:bg-green-500 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-base text-left  inline-flex items-center me-2 w-6 h-6">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                                    </svg>
                                @endif
                            </th>
                        </tr>
                        <tr class="text-left">
                            <th  class="py-1 px-1 border text-base font-semibold text-gray-800">
                                Producto
                            </th>
                            <th class="py-1 px-1 border text-base font-semibold text-gray-800">
                                Precio
                            </th>
                            <th class="py-1 px-1 border text-base font-semibold text-gray-800">
                                Tipo
                            </th>
                            <th colspan="2" class="py-1 px-1 border text-base font-semibold text-gray-800">
                                Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $totalPerdida = 0;
                        @endphp
                        @forelse ($perdidas as $item)
                            <tr class="text-left">
                                @if ($estado == 'rendida' or $estado == 'cerrada')
                                    <td class="border-b border-l py-1 px-1 text-base">
                                        {{ $item->cantidad. ' '.$item->producto->nombre }}
                                    </td>
                                @else
                                    <td wire:click="alerta('{{ $item->id }}', 'perdida')" class="border-b border-l py-2 px-2 text-left text-base font-medium hover:bg-orange-300">
                                        {{ $item->cantidad. ' '.$item->producto->nombre }}
                                    </td>
                                @endif
                                <td class="border-b border-l py-1 px-1 text-base">
                                    {{ moneda($item->monto) }}
                                </td>
                                <td class="border-b border-l py-1 px-1 text-base">
                                    {{ $item->tipo->nombre }}
                                </td>
                                <td class="border-b border-l py-1 px-1 text-base">
                                    {{ moneda($item->monto*$item->cantidad) }}
                                </td>
                                @if ($estado == 'abierta' or $estado == 'Sin rendir')
                                    <td class="border-b border-l py-2 px-2 text-left text-base font-medium">
                                        <svg wire:click.prevent="alerta('{{ $item->id }}', 'perdida-eliminar')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block cursor-pointer">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                                        </svg>
                                    </td>
                                @endif
                            </tr>
                            @php
                                $totalPerdida+=$item->monto*$item->cantidad;
                            @endphp
                        @empty
                            <tr class="text-left">
                                <td colspan="4" class="border-b border-l py-1 px-1 text-left text-base">
                                    No hay perdidas registradas
                                </td>
                            </tr>
                        @endforelse
                        <tr class="text-left">
                            <td colspan="3" class="border-b border-l py-1 px-1 text-base">
                                Total 
                            </td>
                            <td colspan="2" class="border-b border-l py-1 px-1 text-left text-base">
                                {{ moneda($totalPerdida) }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            
            </div>
            <div class="grid grid-cols-1 gap-4">
                <table class="bg-gray-100 border-t-4 border-yellow-400 shadow-md sm:rounded-lg">
                    <thead>
                        <tr class="text-center">
                            <th colspan="4" class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Ventas a Credito
                                @if ($estado == 'abierta' or $estado == 'Sin rendir')
                                    <a href="{{ route('ventas') }}" wire:navigate>
                                        <svg wire:click="vista('venta')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="mr-2 px-1 py-1 text-white bg-green-400 hover:bg-green-500 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-base text-left  inline-flex items-center me-2 w-6 h-6">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                                        </svg>
                                    </a>
                                @endif
                            </th>
                        </tr>
                        <tr class="text-left">
                            <th class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Cliente
                            </th>
                            <th class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Neto
                            </th>
                            <th class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Descuento
                            </th>
                            <th colspan="2" class="py-2 px-2 border text-base font-semibold text-gray-800">
                                Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $totalVenta = 0;
                        @endphp
                        @forelse ($ventas as $item)
                            <tr class="text-left">
                                <td class="py-2 px-2 border text-base font-semibold text-gray-800">
                                    {{ $item->cliente->rut.' - '.$item->cliente->razon_social }}
                                </td>
                                <td class="py-2 px-2 border text-base font-semibold text-gray-800">
                                    {{moneda($item->total($item->id)) }}
                                </td>
                                <td class="py-2 px-2 border text-base font-semibold text-gray-800">
                                    {{ moneda($item->descuento ?? 0) }}
                                </td>
                                <td class="py-2 px-2 border text-base font-semibold text-gray-800">
                                    {{ moneda($item->total($item->id)-$item->descuento) }}
                                </td>
                                <td class="py-2 px-2 border text-base font-semibold text-gray-800">
                                    <a href="{{ route('ventas.detalles', ['id' => $item->id]) }}" wire:navigate target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                                        </svg>
                                    </a>
                                </td>
                            </tr>
                            @php
                                $totalVenta+=$item->total($item->id);
                            @endphp
                        @empty
                            <tr class="bg-white text-center">
                                <td colspan="5" class="px-6 py-4">
                                    No hay ventas registradas
                                </td>
                            </tr>
                        @endforelse
                        <tr class="text-left">
                            <td colspan="3" class="border-b border-l py-1 px-1 text-base">
                                Total 
                            </td>
                            <td class="border-b border-l py-1 px-1 text-left text-base">
                                {{ moneda($totalVenta) }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="grid grid-cols-1 gap-4">
            <table class="bg-gray-100 border-t-4 border-indigo-400 shadow-md sm:rounded-lg">
                <thead>
                    <tr class="text-center">
                        <th colspan="2" class="py-1 px-1 border text-base font-semibold text-gray-800">
                            Rendicion de caja
                            @if (Auth::user()->type == 'admin')
                                @if ($estado == 'Sin rendir')
                                    <svg wire:click="alerta('{{ $sucursal_id }}', 'rendida')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="mr-2 px-1 py-1 text-white bg-gray-500 hover:bg-gray-600 focus:ring-4 focus:outline-none focus:ring-gray-400 font-medium rounded-lg text-base text-left  inline-flex items-center me-2 w-6 h-6">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 18.75a60.07 60.07 0 0 1 15.797 2.101c.727.198 1.453-.342 1.453-1.096V18.75M3.75 4.5v.75A.75.75 0 0 1 3 6h-.75m0 0v-.375c0-.621.504-1.125 1.125-1.125H20.25M2.25 6v9m18-10.5v.75c0 .414.336.75.75.75h.75m-1.5-1.5h.375c.621 0 1.125.504 1.125 1.125v9.75c0 .621-.504 1.125-1.125 1.125h-.375m1.5-1.5H21a.75.75 0 0 0-.75.75v.75m0 0H3.75m0 0h-.375a1.125 1.125 0 0 1-1.125-1.125V15m1.5 1.5v-.75A.75.75 0 0 0 3 15h-.75M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Zm3 0h.008v.008H18V10.5Zm-12 0h.008v.008H6V10.5Z" />
                                    </svg>
                                @endif
                                @if ($estado == 'rendida')
                                    <svg wire:click="alerta('{{ $sucursal_id }}', 'abierta')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="mr-2 px-1 py-1 text-white bg-gray-500 hover:bg-gray-600 focus:ring-4 focus:outline-none focus:ring-gray-400 font-medium rounded-lg text-base text-left  inline-flex items-center me-2 w-6 h-6">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 5.25a3 3 0 0 1 3 3m3 0a6 6 0 0 1-7.029 5.912c-.563-.097-1.159.026-1.563.43L10.5 17.25H8.25v2.25H6v2.25H2.25v-2.818c0-.597.237-1.17.659-1.591l6.499-6.499c.404-.404.527-1 .43-1.563A6 6 0 1 1 21.75 8.25Z" />
                                    </svg>
                                    <svg  wire:click="alerta('{{ $sucursal_id }}', 'cerrada')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="mr-2 px-1 py-1 text-white bg-gray-500 hover:bg-gray-600 focus:ring-4 focus:outline-none focus:ring-gray-400 font-medium rounded-lg text-base text-left  inline-flex items-center me-2 w-6 h-6">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M16.5 10.5V6.75a4.5 4.5 0 1 0-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 0 0 2.25-2.25v-6.75a2.25 2.25 0 0 0-2.25-2.25H6.75a2.25 2.25 0 0 0-2.25 2.25v6.75a2.25 2.25 0 0 0 2.25 2.25Z" />
                                    </svg> 
                                @endif
                                @if ($estado == 'abierta')
                                    <svg  wire:click="alerta('{{ $sucursal_id }}', 'rendida')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="mr-2 px-1 py-1 text-white bg-gray-500 hover:bg-gray-600 focus:ring-4 focus:outline-none focus:ring-gray-400 font-medium rounded-lg text-base text-left  inline-flex items-center me-2 w-6 h-6">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 5.25a3 3 0 0 1 3 3m3 0a6 6 0 0 1-7.029 5.912c-.563-.097-1.159.026-1.563.43L10.5 17.25H8.25v2.25H6v2.25H2.25v-2.818c0-.597.237-1.17.659-1.591l6.499-6.499c.404-.404.527-1 .43-1.563A6 6 0 1 1 21.75 8.25Z" />
                                    </svg>
                                    <svg  wire:click="alerta('{{ $sucursal_id }}', 'cerrada')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="mr-2 px-1 py-1 text-white bg-gray-500 hover:bg-gray-600 focus:ring-4 focus:outline-none focus:ring-gray-400 font-medium rounded-lg text-base text-left  inline-flex items-center me-2 w-6 h-6">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M16.5 10.5V6.75a4.5 4.5 0 1 0-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 0 0 2.25-2.25v-6.75a2.25 2.25 0 0 0-2.25-2.25H6.75a2.25 2.25 0 0 0-2.25 2.25v6.75a2.25 2.25 0 0 0 2.25 2.25Z" />
                                    </svg> 
                                @endif
                            @else
                                @if ($estado == 'Sin rendir')
                                    <svg wire:click="alerta('{{ $sucursal_id }}', 'rendida')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="mr-2 px-1 py-1 text-white bg-gray-500 hover:bg-gray-600 focus:ring-4 focus:outline-none focus:ring-gray-400 font-medium rounded-lg text-base text-left  inline-flex items-center me-2 w-6 h-6">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 18.75a60.07 60.07 0 0 1 15.797 2.101c.727.198 1.453-.342 1.453-1.096V18.75M3.75 4.5v.75A.75.75 0 0 1 3 6h-.75m0 0v-.375c0-.621.504-1.125 1.125-1.125H20.25M2.25 6v9m18-10.5v.75c0 .414.336.75.75.75h.75m-1.5-1.5h.375c.621 0 1.125.504 1.125 1.125v9.75c0 .621-.504 1.125-1.125 1.125h-.375m1.5-1.5H21a.75.75 0 0 0-.75.75v.75m0 0H3.75m0 0h-.375a1.125 1.125 0 0 1-1.125-1.125V15m1.5 1.5v-.75A.75.75 0 0 0 3 15h-.75M15 10.5a3 3 0 1 1-6 0 3 3 0 0 1 6 0Zm3 0h.008v.008H18V10.5Zm-12 0h.008v.008H6V10.5Z" />
                                    </svg>
                                @endif
                            @endif
                            <span class="text-white capitalize {{ ($estado == 'Sin rendir') ? 'bg-red-500' : 'bg-green-500' }} font-medium rounded-lg text-base px-1 py-1"> {{ $estado }} </span>
                            <button onclick="printJS({
                                        printable: 'printJS-planilla', 
                                        header: '<center><h3>{{ 'Planilla diaria sucursal '.$sucursal->nombre . ' '.\Carbon\Carbon::parse($fecha)->format('d/m/Y') }}</h3></center>',
                                        type: 'html',
                                        style: 'table {font-family: Be Vietnam, ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont; color:#383739; width:100%; border-width: 1px; border-color: #0099A8; border-collapse: collapse;} table th {font-size:10px;background-color:#0099A8; color:#fff; padding: 5px; text-align:left; border:none;}  table tr {background-color:#fff; } table td {font-size:10px; padding: 8px; border:none;}'
                                    })"
                                type="button" class="mt-4 text-white bg-orange-400 hover:bg-orange-500 focus:ring-4 focus:outline-none focus:ring-orange-300 font-medium rounded-lg text-base px-1 py-1 text-left inline-flex items-center me-2 ml-2">                
                                Imprimir planilla
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M6.72 13.829c-.24.03-.48.062-.72.096m.72-.096a42.415 42.415 0 0 1 10.56 0m-10.56 0L6.34 18m10.94-4.171c.24.03.48.062.72.096m-.72-.096L17.66 18m0 0 .229 2.523a1.125 1.125 0 0 1-1.12 1.227H7.231c-.662 0-1.18-.568-1.12-1.227L6.34 18m11.318 0h1.091A2.25 2.25 0 0 0 21 15.75V9.456c0-1.081-.768-2.015-1.837-2.175a48.055 48.055 0 0 0-1.913-.247M6.34 18H5.25A2.25 2.25 0 0 1 3 15.75V9.456c0-1.081.768-2.015 1.837-2.175a48.041 48.041 0 0 1 1.913-.247m10.5 0a48.536 48.536 0 0 0-10.5 0m10.5 0V3.375c0-.621-.504-1.125-1.125-1.125h-8.25c-.621 0-1.125.504-1.125 1.125v3.659M18 10.5h.008v.008H18V10.5Zm-3 0h.008v.008H15V10.5Z" />
                                </svg>
                                                   
                            </button>
                        </th>
                    </tr>
                    <tr class="text-left">
                        <th class="py-1 px-1 border text-base font-semibold text-gray-800">
                            Concepto
                        </th>
                        <th class="py-1 px-1 border text-base font-semibold text-gray-800">
                            Total
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="text-left">
                        <td class="border-b border-l py-1 px-1 text-base font-semibold">
                            Venta diaria
                        </td>
                        <td class="border-b border-l py-1 px-1 text-base">
                            {{ moneda($ventaDiaria) }}
                        </td>
                    </tr>
                    <tr class="text-left">
                        <td class="border-b border-l py-1 px-1 text-base font-semibold">
                            Ventas a credito
                        </td>
                        <td class="border-b border-l py-1 px-1 text-base">
                            {{ moneda(-$totalVenta) }}
                        </td>
                    </tr>
                    <tr class="text-left">
                        <td class="border-b border-l py-1 px-1 text-base font-semibold">
                            Gastos
                        </td>
                        <td class="border-b border-l py-1 px-1 text-base">
                            {{ moneda(-$totalGasto) }}
                        </td>
                    </tr>
                    <tr class="text-left">
                        <td class="border-b border-l py-1 px-1 text-base font-semibold">
                            Sueldos
                        </td>
                        <td class="border-b border-l py-1 px-1 text-base">
                            {{ moneda(-$totalSueldo) }}
                        </td>
                    </tr>
                    <tr class="text-left">
                        <td class="border-b border-l py-1 px-1 text-base font-semibold">
                            Perdidas
                        </td>
                        <td class="border-b border-l py-1 px-1 text-base">
                            {{ moneda(-$totalPerdida) }}
                        </td>
                    </tr>
                    
                    <tr class="text-left">
                        <td class="border-b border-l py-1 px-1 text-base font-semibold">
                            Total por rendir 
                        </td>
                        <td class="border-b border-l py-1 px-1 text-base">
                            @php
                                $totalRendir = $ventaDiaria - ($totalGasto+$totalPerdida+$totalSueldo+$totalVenta)
                            @endphp
                            {{ moneda($totalRendir) }}
                        </td>
                    </tr>
                    <tr class="text-left">
                        <td class="border-b border-l py-1 px-1 text-base font-semibold">
                            Total ingresos 
                        </td>
                        <td class="border-b border-l py-1 px-1 text-base">
                            {{ moneda($totalIngreso) }}
                        </td>
                    </tr>
                    <tr class="text-left">
                        <td class="border-b border-l py-1 px-1 text-base font-semibold">
                            Diferencia de caja 
                        </td>
                        <td class="border-b border-l py-1 px-1 text-base">
                            {{ moneda($totalIngreso-$totalRendir) }}
                            <input type="hidden" name="diferenciaCaja" id="diferenciaCaja" value="{{ $totalIngreso-$totalRendir; }}">
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    @endif
 <!-- ====== Table Section End -->

    <x-modal :show="$modal">
        <x-slot name="title">
            <h3 class="text-lg capitalize font-semibold text-gray-900">
                {{ $titulo }}
            </h3>
        </x-slot>
        <x-slot name="body">

            @if ($tipo == 'gasto')  
                <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                    <div class="mt-2">
                        <x-input-label for="fecha" :value="__('Fecha')" />
                        <x-text-input wire:model.blur="fecha" id="fecha" class="block mt-1 w-full" type="date" name="fecha" required autofocus autocomplete="fecha" />
                        <x-input-error :messages="$errors->get('fecha')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="gasto_tipo_id" :value="__('Tipo de gasto')" />
                        <select wire:model.live="gasto_tipo_id" id="gasto_tipo_id" class="block w-full mt-1 text-base border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="gasto_tipo_id" required autofocus autocomplete="gasto_tipo_id" >
                            <option>--Seleccione--</option>
                            @foreach ($tipoGastos as $item)
                                <option value="{{  $item->id }}" {{ ($gasto_tipo_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('gasto_tipo_id')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="monto" :value="__('Monto')" />
                        <x-text-input wire:model.live="monto" id="monto" class="block mt-1 w-full" type="number" name="monto" required autofocus autocomplete="monto"/>
                        <x-input-error :messages="$errors->get('monto')" class="mt-2" />
                    </div>
                </div>
            @endif

            @if ($tipo == 'ingreso')  
                <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                    <div class="mt-2">
                        <x-input-label for="fecha" :value="__('Fecha')" />
                        <x-text-input wire:model.blur="fecha" id="fecha" class="block mt-1 w-full" type="date" name="fecha" required autofocus autocomplete="fecha" />
                        <x-input-error :messages="$errors->get('fecha')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="ingreso_tipo_id" :value="__('Tipo de ingreso')" />
                        <select wire:model.live="ingreso_tipo_id" id="ingreso_tipo_id" class="block w-full mt-1 text-base border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="ingreso_tipo_id" required autofocus autocomplete="ingreso_tipo_id" >
                            <option>--Seleccione--</option>
                            @foreach ($tipoIngresos as $item)
                                <option value="{{  $item->id }}" {{ ($ingreso_tipo_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('ingreso_tipo_id')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="monto" :value="__('Monto')" />
                        <x-text-input wire:model.live="monto" id="monto" class="block mt-1 w-full" type="number" name="monto" required autofocus autocomplete="monto"/>
                        <x-input-error :messages="$errors->get('monto')" class="mt-2" />
                    </div>
                </div>
            @endif

            @if ($tipo == 'perdida')  
                <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                    <div class="mt-2">
                        <x-input-label for="fecha" :value="__('Fecha')" />
                        <x-text-input wire:model.blur="fecha" id="fecha" class="block mt-1 w-full" type="date" name="fecha" required autofocus autocomplete="fecha" />
                        <x-input-error :messages="$errors->get('fecha')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="perdida_id" :value="__('Tipo de perdida')" />
                        <select wire:model.live="perdida_id" id="perdida_id" class="block w-full mt-1 text-base border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="perdida_id" required autofocus autocomplete="perdida_id" >
                            <option>--Seleccione--</option>
                            @foreach ($tipoPerdidas as $item)
                                <option value="{{  $item->id }}" {{ ($perdida_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('perdida_id')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="producto_perdida_id" :value="__('Productos')" />
                        <select wire:model.live="producto_perdida_id" wi id="producto_perdida_id" wire:change="precio($event.target.value)" class="block w-full mt-1 text-base border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="producto_perdida_id" required autofocus autocomplete="producto_perdida_id" >
                            <option>--Seleccione--</option>
                            @foreach ($productos as $item)
                                <option value="{{  $item->id }}" {{ ($producto_perdida_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('producto_perdida_id')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="cantidad" :value="__('Cantidad')" />
                        <x-text-input wire:model.live="cantidad" id="cantidad" class="block mt-1 w-full" type="text" name="cantidad" required autofocus autocomplete="cantidad"/>
                        <x-input-error :messages="$errors->get('cantidad')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="monto" :value="__('Precio unitario')" />
                        <x-text-input wire:model.live="monto" id="monto" class="block mt-1 w-full" type="number" name="monto" required autofocus autocomplete="monto" readonly/>
                        <x-input-error :messages="$errors->get('monto')" class="mt-2" />
                    </div>
                </div>
            @endif

            @if ($tipo == 'sueldo')
                <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                    <div class="mt-2">
                        <x-input-label for="fecha" :value="__('Fecha')" />
                        <x-text-input wire:model.blur="fecha" id="fecha" class="block mt-1 w-full" type="date" name="fecha" required autofocus autocomplete="fecha" />
                        <x-input-error :messages="$errors->get('fecha')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="empleado_id" :value="__('Empleados')" />
                        <select wire:model.live="empleado_id" id="empleado_id" class="block w-full mt-1 text-base border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="empleado_id" required autofocus autocomplete="empleado_id" >
                            <option>--Seleccione--</option>
                            @foreach ($empleados as $item)
                                <option value="{{  $item->id }}" {{ ($empleado_id ==  $item->id) ? 'selected' : '' }}>{{  $item->razon_social }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('empleado_id')" class="mt-2" />
                    </div>
                    <div class="mt-1">
                        <x-input-label for="monto" :value="__('Monto')" />
                        <x-text-input wire:model.live="monto" id="monto" class="block mt-1 w-full" type="number" name="monto" required autofocus autocomplete="monto"/>
                        <x-input-error :messages="$errors->get('monto')" class="mt-2" />
                    </div>
                </div>
            @endif

        </x-slot>
        <x-slot name="footer">
            <x-secondary-button wire:click="modalAction('close')">
                Close
            </x-secondary-button>
            <x-primary-button wire:click="guardar('{{ $tipo }}')">
            Guardar
            </x-primary-button>
        </x-slot>
    </x-modal>
</div>

@push('js')
<script>
    document.addEventListener('livewire:navigated', () => {
        tailwindButton = Swal.mixin({
        customClass: {
            confirmButton: "px-4 py-2 mr-2 bg-orange-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-orange-600 focus:bg-orange-600 active:bg-orange-600 focus:outline-none focus:ring-orange-500 transition ease-in-out duration-150",
            cancelButton: "px-4 py-2 bg-gray-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-600 focus:bg-gray-600 active:bg-gray-600 focus:outline-none focus:ring-gray-500 transition ease-in-out duration-150",
        },
        buttonsStyling: false
        });

        window.addEventListener('alerta', event => {
            Swal.fire(event.detail[0]);
        });

        window.addEventListener('saldo-planilla', event => {
            tailwindButton.fire(event.detail[0]).then((result) => {
                if (result.isConfirmed) {
                    @this.set('monto', result.value);
                    @this.dispatch('saldo');
                }
            });
        });

        window.addEventListener('ingreso-eliminar', event => {
            tailwindButton.fire(event.detail[0]).then((result) => {
                if (result.isConfirmed) {
                    @this.dispatch('eliminar');
                }
            });
        });

        window.addEventListener('perdida-eliminar', event => {
            tailwindButton.fire(event.detail[0]).then((result) => {
                if (result.isConfirmed) {
                    @this.dispatch('eliminar');
                }
            });
        });

        window.addEventListener('gasto-eliminar', event => {
            tailwindButton.fire(event.detail[0]).then((result) => {
                if (result.isConfirmed) {
                    @this.dispatch('eliminar');
                }
            });
        });

        window.addEventListener('sueldo-eliminar', event => {
            tailwindButton.fire(event.detail[0]).then((result) => {
                if (result.isConfirmed) {
                    @this.dispatch('eliminar');
                }
            });
        });

        window.addEventListener('planilla-sucursal', event => {
            tailwindButton.fire(event.detail[0]).then((result) => {
                if (result.isConfirmed) {
                    @this.set('diferenciaCaja',  $('#diferenciaCaja').val());
                    @this.dispatch('planilla');
                }
            });
        });
    });
</script>
@endpush