<div class="p-6 border-t-4 border-orange-400 shadow-md sm:rounded-lg">
    <x-table>
        <x-slot name="title">
            
            <button wire:click="modalAction('open')" type="button" class="float-right text-white bg-orange-400 hover:bg-orange-500 focus:ring-4 focus:outline-none focus:ring-orange-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center me-2">
                <svg  class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 21v-7.5a.75.75 0 0 1 .75-.75h3a.75.75 0 0 1 .75.75V21m-4.5 0H2.36m11.14 0H18m0 0h3.64m-1.39 0V9.349M3.75 21V9.349m0 0a3.001 3.001 0 0 0 3.75-.615A2.993 2.993 0 0 0 9.75 9.75c.896 0 1.7-.393 2.25-1.016a2.993 2.993 0 0 0 2.25 1.016c.896 0 1.7-.393 2.25-1.015a3.001 3.001 0 0 0 3.75.614m-16.5 0a3.004 3.004 0 0 1-.621-4.72l1.189-1.19A1.5 1.5 0 0 1 5.378 3h13.243a1.5 1.5 0 0 1 1.06.44l1.19 1.189a3 3 0 0 1-.621 4.72M6.75 18h3.75a.75.75 0 0 0 .75-.75V13.5a.75.75 0 0 0-.75-.75H6.75a.75.75 0 0 0-.75.75v3.75c0 .414.336.75.75.75Z" />
                </svg>
                Nuevo producto
            </button>
            <a href="{{ route('traspasos') }}" wire:navigate>
                <button type="button" class="text-white bg-orange-400 hover:bg-orange-500 focus:ring-4 focus:outline-none focus:ring-orange-300 font-medium rounded-lg text-xs px-2 py-2 text-center inline-flex items-center me-2">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M9 15 3 9m0 0 6-6M3 9h12a6 6 0 0 1 0 12h-3" />
                      </svg>                      
                </button> 
            </a>  
            {{ $traspaso ? 'Traspaso '. $traspaso->fecha. ' Sucursal '.$traspaso->sucursal->nombre : 'Traspaso sucursal' }}
            <p class="mt-1 text-sm font-normal text-gray-500">Explora la lista de traspasos a detalle en el sistema.</p>
        </x-slot>
        <x-slot name="head">
            <th scope="col" class="px-6 py-3">
                Producto
            </th>
            <th scope="col" class="px-6 py-3">
                Cantidad
            </th>
            <th scope="col" class="px-6 py-3">
                Valor Unitario
            </th>
            <th scope="col" class="px-6 py-3">
                Total Item
            </th>
            <th scope="col" class="px-6 py-3">
                Costo Unitario
            </th>
            <th scope="col" class="px-6 py-3">
                Costo Total
            </th>
            <th scope="col" class="px-6 py-3">
                <span class="">Acciones</span>
            </th>
        </x-slot>
        <x-slot name="body">
            @php
                $sumatotal = 0;
                $costototal = 0;
            @endphp
            @foreach ($traspasos as $item)
                <tr class="bg-white border-b hover:bg-orange-100">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ $item->producto->nombre ?? null }} ({{ $item->producto->codigo ?? null }})
                    </th>
                    @if ((Auth::user()->type != 'despachador'))
                        <th wire:click="alerta('{{ $item->id }}', 'cantidad')" scope="row" class="px-6 py-4 font-medium text-gray-900 hover:bg-orange-300 whitespace-nowrap">
                            {{ $item->cantidad }}
                        </th>
                    @else
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                            {{ $item->cantidad }}
                        </th>
                    @endif
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ moneda($item->precio_unitario ?? 0) }}
                    </th>
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ moneda($item->cantidad*$item->precio_unitario) }}
                    </th>
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ moneda($item->producto->precio_compra ?? 0) }}
                    </th>
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ moneda($item->cantidad*$item->producto->precio_compra) }}
                    </th>
                    <td class="px-6 py-4 text-right cursor-pointer text-orange-600 inline-block">
                        @if (Auth::user()->type != 'despachador')
                            <svg wire:click.prevent="alerta('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                                <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                            </svg>
                        @endif
                    </td>
                </tr>
                @php
                    $sumatotal  +=$item->cantidad*$item->precio_unitario;
                    $costototal +=$item->cantidad*$item->producto->precio_compra;
                @endphp
            @endforeach
            <th scope="col" colspan="3" class="px-6 py-3 text-right">
                Totales
            </th>
            <th scope="col" class="px-6 py-3">
                {{ moneda($sumatotal) }}
            </th>
            <th scope="col" colspan="3" class="px-6 py-3 text-center">
                {{ moneda($costototal) }}
            </th>
        </x-slot>
    </x-table>

    <div class="mt-2">
        {{ $traspasos->links() }}
    </div>


    <x-modal :show="$modal">
        <x-slot name="title">
            <h3 class="text-lg font-semibold text-gray-900">
               Nuevo producto
            </h3>
        </x-slot>
        <x-slot name="body">
            <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                @if ($sucursal_id>0 && count($productos)>0)
                    <div class="mt-1">
                        <x-input-label for="producto_id" :value="__('Producto')" />
                        <select wire:model.live="producto_id" id="producto_id" wire:change="agregar($event.target.value)" class="block w-full mt-1 text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="producto_id" required autofocus autocomplete="producto_id" >
                            <option>--Seleccione--</option>
                            @foreach ($productos as $item)
                                <option value="{{  $item->id }}">{{  $item->nombre }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('producto_id')" class="mt-2" />
                    </div>

                    @if ((isset($producto_id) && $producto_id>0))
                        <div class="mt-1">
                            <x-input-label for="cantidad" :value="__('Cantidad')" />
                            <x-text-input wire:model.live="cantidad" wire:change="total($event.target.value)" id="cantidad" class="block mt-1 w-full" type="text" name="cantidad" required autofocus autocomplete="cantidad" />
                            <x-input-error :messages="$errors->get('cantidad')" class="mt-2" />
                        </div>

                        <div class="mt-1">
                            <x-input-label for="precio_unitario_compra" :value="__('Valor unitario de venta')" />
                            <x-text-input wire:model.live="precio_unitario_compra" id="precio_unitario_compra" class="block mt-1 w-full" type="text" name="precio_unitario_compra" required autofocus autocomplete="precio_unitario_compra" readonly=""/>
                            <x-input-error :messages="$errors->get('precio_unitario_compra')" class="mt-2" />
                        </div>

                        <div class="mt-1">
                            <x-input-label for="total_venta" :value="__('Valor total por productos')" />
                            <x-text-input wire:model.live="total_venta" id="total_venta" class="block mt-1 w-full" type="text" name="total_venta" required autofocus autocomplete="total_venta" readonly=""/>
                            <x-input-error :messages="$errors->get('total_venta')" class="mt-2" />
                        </div>
                    @endif

                @endif
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-secondary-button wire:click="modalAction('close')">
                Close
            </x-secondary-button>
            @if ($sucursal_id>0 && count($productos)>0 && $producto_id>0)
                <x-primary-button wire:click="guardar">
                    Guardar
                </x-primary-button>
            @endif
        </x-slot>
    </x-modal>
</div>
@push('js')
<script>
    tailwindButton = Swal.mixin({
    customClass: {
        confirmButton: "px-4 py-2 mr-2 bg-orange-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-orange-600 focus:bg-orange-600 active:bg-orange-600 focus:outline-none focus:ring-orange-500 transition ease-in-out duration-150",
        cancelButton: "px-4 py-2 bg-gray-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-600 focus:bg-gray-600 active:bg-gray-600 focus:outline-none focus:ring-gray-500 transition ease-in-out duration-150",
    },
    buttonsStyling: false
    });

    window.addEventListener('alerta', event => {
        Swal.fire(event.detail[0]);
    });

    window.addEventListener('eliminar-detalle-traspaso', event => {
        tailwindButton.fire(event.detail[0]).then((result) => {
            if (result.isConfirmed) {
                @this.dispatch('eliminar');
            }
        });
    });

    window.addEventListener('cantidad-detalle-traspaso', event => {
        tailwindButton.fire(event.detail[0]).then((result) => {
            if (result.isConfirmed) {
                @this.set('cantidad', result.value);
                @this.dispatch('producto');
            }
        });
    });
</script>
@endpush