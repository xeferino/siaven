<div class="p-6 border-t-4 border-orange-400 shadow-md sm:rounded-lg">
    <x-table>
        <x-slot name="title">
            
            <button wire:click="modalAction('open')" type="button" class="float-right text-white bg-orange-400 hover:bg-orange-500 focus:ring-4 focus:outline-none focus:ring-orange-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center me-2">
                <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 0 0-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 0 0-16.536-1.84M7.5 14.25 5.106 5.272M6 20.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0Zm12.75 0a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0Z" />
                </svg>
                Nuevo producto
            </button>
            <a href="{{ route('ventas') }}" wire:navigate>
                <button type="button" class="text-white bg-orange-400 hover:bg-orange-500 focus:ring-4 focus:outline-none focus:ring-orange-300 font-medium rounded-lg text-xs px-2 py-2 text-center inline-flex items-center me-2">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M9 15 3 9m0 0 6-6M3 9h12a6 6 0 0 1 0 12h-3" />
                      </svg>                      
                </button> 
            </a>  
            {{ 'Venta #'.str_pad($id, 6, "0", STR_PAD_LEFT). ' - '.$venta->cliente->razon_social }} 
            <p class="mt-1 text-sm font-normal text-gray-500">Explora la lista de ventas a detalle en el sistema.</p>
        </x-slot>
        <x-slot name="head">
            <th scope="col" class="px-6 py-3">
                Producto
            </th>
            <th scope="col" class="px-6 py-3">
                Cantidad
            </th>
            <th scope="col" class="px-6 py-3">
                Valor Unitario
            </th>
            <th scope="col" class="px-6 py-3">
                Total Item
            </th>
            <th scope="col" class="px-6 py-3">
               Anulado
            </th>
            <th scope="col" class="px-6 py-3">
                <span class="">Acciones</span>
            </th>
        </x-slot>
        <x-slot name="body">
            @php
                $totalguia = 0;
                $costototal = 0;
            @endphp
            @foreach ($ventas as $item)
                <tr class="bg-white border-b  {{ $item->estado ? 'bg-red-200' : 'hover:bg-orange-100' }}">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        ({{ $item->producto->codigo ?? null }}) {{ $item->producto->nombre ?? null }}
                    </th>
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ $item->cantidad }}
                    </th>
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ moneda($item->precio_unitario) }}
                    </th>
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ moneda($item->cantidad*$item->precio_unitario) }}
                    </th>
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ $item->estado ? 'Si' :'No' }}
                    </th>
                    <td class="px-6 py-4 text-right cursor-pointer text-orange-600 inline-block">
                        <svg wire:click.prevent="alerta('{{ $item->id }}', 'anular')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M18.364 18.364A9 9 0 0 0 5.636 5.636m12.728 12.728A9 9 0 0 1 5.636 5.636m12.728 12.728L5.636 5.636" />
                        </svg>
                        <svg wire:click.prevent="alerta('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                        </svg>
                    </td>
                </tr>
                @php
                    $costototal +=$item->cantidad*$item->precio_unitario;
                @endphp
            @endforeach
            <th scope="col" colspan="3" class="px-6 py-3 text-right">
                Totales
            </th>
            <th scope="col" colspan="3" class="px-6 py-3 text-left">
                {{ moneda($costototal) }}
            </th>
        </x-slot>
    </x-table>

    <div class="mt-2">
        {{ $ventas->links() }}
    </div>

    <x-modal :show="$modal">
        <x-slot name="title">
            <h3 class="text-lg font-semibold text-gray-900">
               Nuevo producto
            </h3>
        </x-slot>
        <x-slot name="body">
            <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                @if (count($productos)>0)
                    <div class="mt-1">
                        <x-input-label for="producto_id" :value="__('Producto')" />
                        <select wire:model.live="producto_id" id="producto_id" wire:change="agregar($event.target.value)" class="block w-full mt-1 text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="producto_id" required autofocus autocomplete="producto_id" >
                            <option>--Seleccione--</option>
                            @foreach ($productos as $item)
                                <option value="{{  $item->id }}">{{  $item->nombre }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('producto_id')" class="mt-2" />
                    </div>

                    @if ((isset($producto_id) && $producto_id>0))
                        <div class="mt-1">
                            <x-input-label for="cantidad" :value="__('Cantidad')" />
                            <x-text-input wire:model.live="cantidad" id="cantidad" wire:change="total($event.target.value)" class="block mt-1 w-full" type="text" name="cantidad" required autofocus autocomplete="cantidad" />
                            <x-input-error :messages="$errors->get('cantidad')" class="mt-2" />
                        </div>
                        <div class="mt-1">
                            <x-input-label for="precio_unitario_venta" :value="__('Valor unitario de venta')" />
                            <x-text-input wire:model.live="precio_unitario_venta" id="precio_unitario_venta" class="block mt-1 w-full" type="text" name="precio_unitario_venta" required autofocus autocomplete="precio_unitario_venta" />
                            <x-input-error :messages="$errors->get('precio_unitario_venta')" class="mt-2" />
                        </div>
                        <div class="mt-1">
                            <x-input-label for="total_venta" :value="__('Valor total por productos')" />
                            <x-text-input wire:model.live="total_venta" id="total_venta" class="block mt-1 w-full" type="text" name="total_venta" required autofocus autocomplete="total_venta" readonly=""/>
                            <x-input-error :messages="$errors->get('total_venta')" class="mt-2" />
                        </div>
                    @endif

                @endif
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-secondary-button wire:click="modalAction('close')">
                Close
            </x-secondary-button>
                <x-primary-button wire:click="guardar">
                    Guardar
                </x-primary-button>
        </x-slot>
    </x-modal>
</div>
@push('js')
<script>
    tailwindButton = Swal.mixin({
    customClass: {
        confirmButton: "px-4 py-2 mr-2 bg-orange-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-orange-600 focus:bg-orange-600 active:bg-orange-600 focus:outline-none focus:ring-orange-500 transition ease-in-out duration-150",
        cancelButton: "px-4 py-2 bg-gray-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-600 focus:bg-gray-600 active:bg-gray-600 focus:outline-none focus:ring-gray-500 transition ease-in-out duration-150",
    },
    buttonsStyling: false
    });

    window.addEventListener('alerta', event => {
        Swal.fire(event.detail[0]);
    });

    window.addEventListener('eliminar-detalle-venta', event => {
        tailwindButton.fire(event.detail[0]).then((result) => {
            if (result.isConfirmed) {
                @this.dispatch('eliminar');
            }
        });
    });

    window.addEventListener('anular-detalle-venta', event => {
        tailwindButton.fire(event.detail[0]).then((result) => {
            if (result.isConfirmed) {
                @this.dispatch('anular');
            }
        });
    });
</script>
@endpush