<div class="p-6 border-t-4 border-orange-400 shadow-md sm:rounded-lg">
    <x-table>
        <x-slot name="title">
            <button wire:click="modalAction('open')" type="button" class="float-right text-white bg-orange-400 hover:bg-orange-500 focus:ring-4 focus:outline-none focus:ring-orange-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center me-2">
                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                    <path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"></path>
                </svg>
                Nuevo empleado
            </button>   
            Empleados
            <p class="mt-1 text-sm font-normal text-gray-500">Explora la lista de empleados en el sistema.</p>
        </x-slot>
        <x-slot name="head">
            <th scope="col" class="px-6 py-3">
                RUT
            </th>
            <th scope="col" class="px-6 py-3">
                Razon Social
            </th>
            <th scope="col" class="px-6 py-3">
                Ciudad
            </th>
            <th scope="col" class="px-6 py-3">
                Direccion
            </th>
            <th scope="col" class="px-6 py-3">
                Giro
            </th>
            <th scope="col" class="px-6 py-3">
                Contacto
            </th>
            <th scope="col" class="px-6 py-3">
                Email
            </th>
            <th scope="col" class="px-6 py-3">
                Sucursal
            </th>
            <th scope="col" class="px-6 py-3">
                TLF 1
            </th>
            <th scope="col" class="px-6 py-3">
                TLF 2
            </th>
            <th scope="col" class="px-6 py-3">
                <span class="">Acciones</span>
            </th>
        </x-slot>
        <x-slot name="body">
            @forelse ($empleados as $item)
                <tr class="bg-white border-b hover:bg-orange-100">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                        {{ $item->rut }}
                    </th>
                    <td class="px-6 py-4">
                        {{ $item->razon_social }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->ciudad }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->direccion }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->giro }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->contacto }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->email }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->sucursal->nombre ?? null }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->telefono1 }}
                    </td>
                    <td class="px-6 py-4">
                        {{ $item->telefono2 }}
                    </td>
                    <td class="px-6 py-4 text-right cursor-pointer text-orange-600 inline-block">
                        <svg wire:click="editar('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L10.582 16.07a4.5 4.5 0 0 1-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 0 1 1.13-1.897l8.932-8.931Zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0 1 15.75 21H5.25A2.25 2.25 0 0 1 3 18.75V8.25A2.25 2.25 0 0 1 5.25 6H10" />
                        </svg>
                        <svg wire:click.prevent="alerta('{{ $item->id }}')" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 inline-block">
                            <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                        </svg>
                    </td>
                </tr> 
            @empty
                <tr class="bg-white text-center">
                    <td colspan="11" class="px-6 py-4">
                        No hay empleados registrados
                    </td>
                </tr> 
            @endforelse
        </x-slot>
    </x-table>

    <div class="mt-2">
        {{ $empleados->links() }}
    </div>

    <x-modal :show="$modal">
        <x-slot name="title">
            <h3 class="text-lg font-semibold text-gray-900">
                {{ ($id) ? 'Editar Empleado' : 'Nuevo Empleado' }}
            </h3>
        </x-slot>
        <x-slot name="body">
            <div class="bg-white px-4 pb-4 sm:p-4 sm:pb-4">
                    <div class="mt-1">
                        <x-input-label for="sucursal_id" :value="__('Sucursal')" />
                        <select wire:model.blur="sucursal_id" id="sucursal_id" class="block w-full mt-1 text-sm border-gray-400 focus:border-gray-100 focus:ring-gray-500 rounded-md shadow-sm" name="sucursal_id" required autofocus autocomplete="sucursal_id" >
                            <option>--Seleccione--</option>
                            @foreach ($sucursales as $item)
                                <option value="{{  $item->id }}" {{ ($sucursal_id ==  $item->id) ? 'selected' : '' }}>{{  $item->nombre }}</option>
                            @endforeach
                        </select>
                        <x-input-error :messages="$errors->get('sucursal_id')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="rut" :value="__('RUT')" />
                        <x-text-input wire:model.blur="rut" id="rut" class="block mt-1 w-full" type="text" name="rut" required autofocus autocomplete="rut" />
                        <x-input-error :messages="$errors->get('rut')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="razon_social" :value="__('Razon Social')" />
                        <x-text-input wire:model.blur="razon_social" id="razon_social" class="block mt-1 w-full" type="text" name="razon_social" required autofocus autocomplete="razon_social" />
                        <x-input-error :messages="$errors->get('razon_social')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="ciudad" :value="__('Ciudad')" />
                        <x-text-input wire:model.blur="ciudad" id="ciudad" class="block mt-1 w-full" type="text" name="ciudad" required autofocus autocomplete="ciudad" />
                        <x-input-error :messages="$errors->get('ciudad')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="giro" :value="__('Giro')" />
                        <x-text-input wire:model.blur="giro" id="giro" class="block mt-1 w-full" type="text" name="giro" required autofocus autocomplete="giro" />
                        <x-input-error :messages="$errors->get('giro')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="contacto" :value="__('Contacto')" />
                        <x-text-input wire:model.blur="contacto" id="contacto" class="block mt-1 w-full" type="text" name="contacto" required autofocus autocomplete="contacto" />
                        <x-input-error :messages="$errors->get('contacto')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="email" :value="__('Email')" />
                        <x-text-input wire:model.blur="email" id="email" class="block mt-1 w-full" type="text" name="email" required autofocus autocomplete="email" />
                        <x-input-error :messages="$errors->get('email')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="direccion" :value="__('Direccion')" />
                        <x-text-input wire:model.blur="direccion" id="direccion" class="block mt-1 w-full" type="text" name="direccion" required autofocus autocomplete="direccion" />
                        <x-input-error :messages="$errors->get('direccion')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="telefono1" :value="__('Telefono 1')" />
                        <x-text-input wire:model.blur="telefono1" id="telefono1" class="block mt-1 w-full" type="text" name="telefono1" required autofocus autocomplete="telefono1" />
                        <x-input-error :messages="$errors->get('telefono1')" class="mt-2" />
                    </div>
                    <div class="mt-2">
                        <x-input-label for="telefono2" :value="__('Telefono 2')" />
                        <x-text-input wire:model.blur="telefono2" id="telefono2" class="block mt-1 w-full" type="text" name="telefono2" required autofocus autocomplete="telefono2" />
                        <x-input-error :messages="$errors->get('telefono2')" class="mt-2" />
                    </div>
            </div>
        </x-slot>
        <x-slot name="footer">
            <x-secondary-button wire:click="modalAction('close')">
                Close
            </x-secondary-button>
            <x-primary-button wire:click="guardar">
                {{ ($id) ? 'Editar' : 'Guardar' }}
            </x-primary-button>
        </x-slot>
    </x-modal>
</div>
@push('js')
<script>
    tailwindButton = Swal.mixin({
    customClass: {
        confirmButton: "px-4 py-2 mr-2 bg-orange-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-orange-600 focus:bg-orange-600 active:bg-orange-600 focus:outline-none focus:ring-orange-500 transition ease-in-out duration-150",
        cancelButton: "px-4 py-2 bg-gray-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-600 focus:bg-gray-600 active:bg-gray-600 focus:outline-none focus:ring-gray-500 transition ease-in-out duration-150",
    },
    buttonsStyling: false
    });

    window.addEventListener('alerta', event => {
        Swal.fire(event.detail[0]);
    });

    window.addEventListener('eliminar-empleado', event => {
        tailwindButton.fire(event.detail[0]).then((result) => {
            if (result.isConfirmed) {
                @this.dispatch('eliminar');
            }
        });
    });
</script>
@endpush