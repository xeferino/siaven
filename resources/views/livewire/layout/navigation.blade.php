<?php

use App\Livewire\Actions\Logout;
use Livewire\Volt\Component;

new class extends Component
{
    /**
     * Log the current user out of the application.
     */
    public function logout(Logout $logout): void
    {
        $logout();

        $this->redirect('/login', navigate: true);
    }
}; ?>

<nav x-data="{ open: false }" class="bg-white border-b border-gray-100 fixed top-0 left-0 right-0">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">
                <!-- Logo -->
                <div class="shrink-0 flex items-center">
                    <a href="{{ route('tablas') }}" wire:navigate>
                        <x-application-logo class="block h-9 w-auto fill-current text-gray-800" />
                    </a>
                </div>

                <!-- Navigation Links -->
                {{-- <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
                    <x-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')" wire:navigate>
                        {{ __('Dashboard') }}
                    </x-nav-link>
                </div> --}}
                @if (Auth::user()->type == 'admin')
                    <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
                        <x-nav-link :href="route('tablas')" 
                        :active="request()->routeIs('tablas') or request()->routeIs('sucursales') 
                        or request()->routeIs('clientes') or request()->routeIs('clientes.historial')
                        or request()->routeIs('proveedores')
                        or request()->routeIs('productos')" 
                        wire:navigate>
                            {{ __('Tablas') }}
                        </x-nav-link>
                    </div>
                    <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
                        <x-nav-link :href="route('movimientos')" :active="request()->routeIs('movimientos')
                            or request()->routeIs('traspasos')
                            or request()->routeIs('traspasos.detalles')
                            or request()->routeIs('solicitudes')
                            or request()->routeIs('compras')
                            or request()->routeIs('compras.detalles')
                            or request()->routeIs('ventas')
                            or request()->routeIs('ventas.detalles')"  wire:navigate>
                            {{ __('Movimientos') }}
                        </x-nav-link>
                    </div>
                    <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
                        <x-nav-link :href="route('informes')" :active="request()->routeIs('informes')
                            or request()->routeIs('planillas')
                            or request()->routeIs('informes.inventario')
                            or request()->routeIs('informes.productos')
                            or request()->routeIs('informes.clientes')
                            or request()->routeIs('informes.resumen')
                            or request()->routeIs('informes.rentabilidad')
                            or request()->routeIs('informes.libros')
                            or request()->routeIs('informes.despacho')
                            or request()->routeIs('informes.solicitud')
                            or request()->routeIs('informes.cxc')
                            "  wire:navigate>
                            {{ __('Informes') }}
                        </x-nav-link>
                    </div>
                
                    <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
                        <x-nav-link :href="route('configuraciones')" :active="request()->routeIs('configuraciones') 
                        or request()->routeIs('usuarios') or request()->routeIs('gastos')
                        or request()->routeIs('pagos')
                        or request()->routeIs('documentos')
                        or request()->routeIs('empleados')
                        or request()->routeIs('perdidas')
                        or request()->routeIs('categorias')
                        or request()->routeIs('ingresos')"  wire:navigate>
                            {{ __('Configuraciones') }}
                        </x-nav-link>
                    </div>
                @else
                    @if (Auth::user()->type == 'sucursal')
                        <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
                            <x-nav-link :href="route('movimientos')" :active="request()->routeIs('movimientos')
                                or request()->routeIs('ventas')
                                or request()->routeIs('solicitudes')
                                or request()->routeIs('ventas.detalles')"  wire:navigate>
                                {{ __('Movimientos') }}
                            </x-nav-link>
                        </div>
                        <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
                            <x-nav-link :href="route('informes')" :active="request()->routeIs('informes')
                                or request()->routeIs('planillas')"  wire:navigate>
                                {{ __('Informes') }}
                            </x-nav-link>
                        </div>
                    @endif

                    @if (Auth::user()->type == 'despachador')
                        <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
                            <x-nav-link :href="route('movimientos')" :active="request()->routeIs('movimientos')
                                or request()->routeIs('traspasos')
                                or request()->routeIs('traspasos.detalles')"  wire:navigate>
                                {{ __('Movimientos') }}
                            </x-nav-link>
                        </div>
                        <div class="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
                            <x-nav-link :href="route('tablas')" 
                            :active="request()->routeIs('tablas') or request()->routeIs('productos')" 
                            wire:navigate>
                                {{ __('Tablas') }}
                            </x-nav-link>
                        </div>
                    @endif
                @endif
            </div>

            <!-- Settings Dropdown -->
            <div class="hidden sm:flex sm:items-center sm:ms-6">
                <x-dropdown align="right" width="48">
                    <x-slot name="trigger">
                        <button class="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md text-gray-500 bg-white hover:text-gray-700 focus:outline-none transition ease-in-out duration-150">
                            <div x-data="{{ json_encode(['name' => auth()->user()->name]) }}" x-text="name" x-on:profile-updated.window="name = $event.detail.name"></div>

                            <div class="ms-1">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </div>
                        </button>
                    </x-slot>

                    <x-slot name="content">
                       {{--  <x-dropdown-link :href="route('profile')" wire:navigate>
                            {{ __('Perfil de usuario') }}
                        </x-dropdown-link> --}}

                        <!-- Authentication -->
                        <button wire:click="logout" class="w-full text-start">
                            <x-dropdown-link>
                                {{ __('Salir del sistema') }}
                            </x-dropdown-link>
                        </button>
                    </x-slot>
                </x-dropdown>
            </div>

            <!-- Hamburger -->
            <div class="-me-2 flex items-center sm:hidden">
                <button @click="open = ! open" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
        <div class="pt-2 pb-3 space-y-1">
            {{-- <x-responsive-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')" wire:navigate>
                {{ __('Dashboard') }}
            </x-responsive-nav-link> --}}
            @if (Auth::user()->type == 'admin')
                <x-responsive-nav-link :href="route('tablas')" 
                :active="request()->routeIs('tablas') or request()->routeIs('sucursales') 
                or request()->routeIs('clientes') or request()->routeIs('clientes.historial') or request()->routeIs('proveedores')
                or request()->routeIs('productos')" 
                wire:navigate>
                    {{ __('Tablas') }}
                </x-responsive-nav-link>
                <x-responsive-nav-link :href="route('movimientos')" :active="request()->routeIs('movimientos')
                    or request()->routeIs('traspasos')
                    or request()->routeIs('traspasos.detalles')
                    or request()->routeIs('solicitudes')
                    or request()->routeIs('compras')
                    or request()->routeIs('compras.detalles')
                    or request()->routeIs('ventas')
                    or request()->routeIs('ventas.detalles')" wire:navigate>
                    {{ __('Movimientos') }}
                </x-responsive-nav-link>
                <x-responsive-nav-link :href="route('informes')" :active="request()->routeIs('informes')
                    or request()->routeIs('planillas')
                    or request()->routeIs('informes.inventario')
                    or request()->routeIs('informes.productos')
                    or request()->routeIs('informes.clientes')
                    or request()->routeIs('informes.resumen')
                    or request()->routeIs('informes.rentabilidad')
                    or request()->routeIs('informes.libros')
                    or request()->routeIs('informes.despacho')
                    or request()->routeIs('informes.solicitud')
                    or request()->routeIs('informes.cxc')
                    " wire:navigate>
                    {{ __('Informes') }}
                </x-responsive-nav-link>
                <x-responsive-nav-link :href="route('configuraciones')" :active="request()->routeIs('configuraciones') 
                or request()->routeIs('usuarios') or request()->routeIs('gastos') 
                or request()->routeIs('pagos')
                or request()->routeIs('documentos')
                or request()->routeIs('empleados')
                or request()->routeIs('perdidas')
                or request()->routeIs('categorias')
                or request()->routeIs('ingresos')" wire:navigate>
                    {{ __('Configuraciones') }}
                </x-responsive-nav-link>
            @else
                @if (Auth::user()->type == 'sucursal')
                    <x-responsive-nav-link :href="route('movimientos')" :active="request()->routeIs('movimientos')
                        or request()->routeIs('ventas')
                        or request()->routeIs('solicitudes')
                        or request()->routeIs('ventas.detalles')" wire:navigate>
                        {{ __('Movimientos') }}
                    </x-responsive-nav-link>
                    <x-responsive-nav-link :href="route('informes')" :active="request()->routeIs('informes')
                        or request()->routeIs('planillas')" wire:navigate>
                        {{ __('Informes') }}
                    </x-responsive-nav-link>
                @endif
                @if (Auth::user()->type == 'despachador')
                    <x-responsive-nav-link :href="route('tablas')" 
                    :active="request()->routeIs('tablas') or request()->routeIs('productos')" 
                    wire:navigate>
                        {{ __('Tablas') }}
                    </x-responsive-nav-link>
                    <x-responsive-nav-link :href="route('movimientos')" :active="request()->routeIs('movimientos')
                        or request()->routeIs('traspasos')
                        or request()->routeIs('traspasos.detalles')" wire:navigate>
                        {{ __('Movimientos') }}
                    </x-responsive-nav-link>
                @endif
            @endif
        </div>

        <!-- Responsive Settings Options -->
        <div class="pt-4 pb-1 border-t border-gray-200">
            <div class="px-4">
                <div class="font-medium text-base text-gray-800" x-data="{{ json_encode(['name' => auth()->user()->name]) }}" x-text="name" x-on:profile-updated.window="name = $event.detail.name"></div>
                <div class="font-medium text-sm text-gray-500">{{ auth()->user()->email }}</div>
            </div>

            <div class="mt-3 space-y-1">
                {{-- <x-responsive-nav-link :href="route('profile')" wire:navigate>
                    {{ __('Perfil de usuario') }}
                </x-responsive-nav-link> --}}

                <!-- Authentication -->
                <button wire:click="logout" class="w-full text-start">
                    <x-responsive-nav-link>
                        {{ __('Salir del sistema') }}
                    </x-responsive-nav-link>
                </button>
            </div>
        </div>
    </div>
</nav>
