@props([
    'count',
    'title',
    'icon'
])
<div class="flex items-center p-4 border border-gray-200 shadow-md  bg-white rounded-lg shadow-xs">
    <div class="p-3 mr-4 text-orange-800 bg-orange-200 rounded-full">
      {{ $icon ?? $slot }}
    </div>
    <div>
      <p class="mb-2 text-sm font-medium">
        {{ $title ?? $slot}}
      </p>
      <p class="text-lg font-semibold">
        {{ $count ?? $slot}}
      </p>
    </div>
</div>