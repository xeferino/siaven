<div class="overflow-x-auto">
    <table class="w-full text-sm text-left rtl:text-right text-gray-500">
        <caption class="mb-2 text-lg font-semibold text-left rtl:text-right text-gray-900 bg-white">
           {{ $title }}
        </caption>
        <thead class="text-xs text-gray-700 uppercase bg-gray-50">
            <tr>
                {{ $head }}
            </tr>
        </thead>
        <tbody>
            {{ $body }}
        </tbody>
    </table>
</div>
