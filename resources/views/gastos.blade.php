<x-app-layout>
    <div class="py-12 mt-6">
        <div class="max-w-7x mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                @livewire('gasto')
            </div>
        </div>
    </div>
</x-app-layout>