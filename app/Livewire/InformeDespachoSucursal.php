<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\DetalleTraspaso;
use App\Models\SolicitudProducto;
use App\Models\Traspaso;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\Validate;

class InformeDespachoSucursal extends Component
{
    //#[Validate('required', message: 'La Sucursal es requerida.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $desde;
    #[Validate('after_or_equal:desde', message: 'La Fecha no puede ser menor.')]
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $hasta;
    public $sucursal;

    public $resultados = [];
    public $fechas = [];

    public function render()
    {
        $sucursales = Sucursal::whereNull('deleted_at')
                        ->where('id','<>', 1)
                        ->orderBy('nombre', 'ASC')
                        ->get();
        return view('livewire.informe-despacho-sucursal',compact(
            'sucursales'
        ));
    }

    public function buscar()
    {
       $this->validate([
            'desde'       => 'required',
            'hasta'       => 'required|after_or_equal:desde'
        ]);

        $sucursales = Sucursal::whereNull('deleted_at')
                        ->where('id','<>', 1)
                        ->orderBy('nombre', 'ASC')
                        ->get();

        $this->resultados = [];

        $traspasos = Traspaso::whereBetween('fecha', [$this->desde, $this->hasta])
                        ->whereNull('deleted_at')
                        ->pluck('id');

        $productos = DetalleTraspaso::selectRaw('detalle_traspasos.producto_id')
                        ->leftJoin('productos', 'productos.id', '=', 'detalle_traspasos.producto_id')
                        ->whereIn('detalle_traspasos.traspaso_id', $traspasos)
                        ->whereNull('detalle_traspasos.deleted_at')
                        ->groupBy('detalle_traspasos.producto_id')
                        ->orderBy('productos.nombre', 'ASC')
                        ->get();

        foreach ($productos as $key => $item) {
            $producto           =  $item->producto->nombre;
            $data = [];
            $total = 0;
            
            foreach ($sucursales as $key => $sucursal) {
                $cantidad = DetalleTraspaso::selectRaw('detalle_traspasos.cantidad, traspasos.fecha')
                ->leftJoin('traspasos', 'detalle_traspasos.traspaso_id', '=', 'traspasos.id')
                ->whereBetween('traspasos.fecha', [$this->desde, $this->hasta])
                ->where('detalle_traspasos.producto_id', $item->producto_id)
                ->where('traspasos.sucursal_id', $sucursal->id)
                ->whereNull('traspasos.deleted_at')
                ->whereNull('detalle_traspasos.deleted_at')
                ->sum('detalle_traspasos.cantidad');

                array_push($data, [
                    'cantidad'     => $cantidad ?? 0,
                ]);
                $total += $cantidad;
            }

            array_push($this->resultados, [
                'producto'          => $producto,
                'sucursales'        => $data,
                'total'             => $total
            ]);
        }
        if (count($this->resultados)>0) {
            $this->dispatch('alerta', [
                'title'             => 'Informe de despacho sucursales cargado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        } else {
            $this->dispatch('alerta', [
                'title'             => 'No hay despacho a sucursales, para el periodo seleccionado.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        }
    }

}
