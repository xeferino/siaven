<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Pago as TipoPago;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;

class Pago extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar'];

    public $modal = 'hidden';
    public $id;
    #[Validate('required', message: 'El nombre es requerido.')]
    #[Validate('min:3', message: 'El nombre debe contener 3 letras como minimo.')]
    public $nombre;

   
    public $telefono;

    public function render()
    {
        $pagos = TipoPago::paginate(10);
        return view('livewire.pago', compact('pagos'));
    }

    public function guardar() {

        $this->validate([
            'nombre'     => 'required|string|min:3',
        ]);

        $pago = ($this->id) ? TipoPago::updateOrCreate(
            [
                'id' => $this->id,
            ],
            [
                'nombre'        => $this->nombre,
                'created_at'    => now(),
                'updated_at'    => now()
            ])
        : TipoPago::create(
            [
                'nombre'        => $this->nombre,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

        if ($pago) {
            $this->dispatch('alerta', [
                'title'             => ($this->id) ? 'Tipo de pago actualizado exitosamente.' : 'Tipo de pago registrado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);

            $this->modalAction('close');
            $this->reset(['nombre']);
            $this->resetPage();
        }
    }
    public function editar($id) {
        $this->id = $id;
        $pago = TipoPago::find($this->id);
        $this->nombre       = $pago->nombre;
        $this->modalAction('open');
    }

    public function alerta($id) {
        $this->id = $id;
        $pago = TipoPago::find($id);
        $this->dispatch('eliminar-pago',[
            'title'                 => $pago->nombre,
            'text'                  => '¿Desea Eliminar el tipo de pago?',
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Eliminar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $pago = TipoPago::find($this->id);
        if ($pago->compras()->count()>0 or $pago->ventas()->count()>0) {
            $this->dispatch('alerta', [
                'title'             => 'El tipo de pago, no puede ser eliminado ya esta asociado a una venta o compra',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            if ($pago->delete()) {
                $this->id = null;
                $this->dispatch('alerta', [
                    'title'             => 'Tipo de pago eliminado exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            } else {
                $this->dispatch('alerta', [
                    'title'             => 'Ha ocurrido un error, intente nuevamente.',
                    'timer'             => 3000,
                    'icon'              => 'error',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            }
        }
    }
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->reset(['nombre']);
        }
        $this->resetValidation();
    }
}
