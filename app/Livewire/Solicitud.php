<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Solicitud as SolicitudProd;
use App\Models\SolicitudProducto;
use App\Models\CierrePlanillaDiaria as Planilla;
use App\Models\StockSucursal;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\PrecioProducto;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;
use Illuminate\Support\Facades\Auth;

class Solicitud extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar', 'solicitud'];

    public $modal = 'hidden';
    public $id;
    public $solicitud_id;

    #[Validate('required', message: 'La Fecha es requerida.')]
    public $fecha;
    public $fechaF;
    //#[Validate('required', message: 'La Sucursal es requerida.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_idF;
    public $sucursal_id;
    #[Validate('integer', message: 'El Producto debe ser entera.')]
    #[Validate('required', message: 'El Producto es requerido.')]
    public $producto_id;
    #[Validate('required', message: 'La Cantidad es requerida.')]
    #[Validate('numeric', message: 'La Cantidad debe ser numerica.')]
    #[Validate('min:1', message: 'La Cantidad debe ser mayor a cero.')]
    public $cantidad = 0;
    #[Validate('required', message: 'El Precio Unitario es requerida.')]
    #[Validate('numeric', message: 'El Precio Unitario debe ser numerico.')]
    public $precio_unitario_compra = 0;
    #[Validate('required', message: 'El Total es requerido.')]
    #[Validate('numeric', message: 'El Total debe ser numerico.')]
    public $total_venta = 0;

    public function mount()
    {
        if (Auth::user()->type != 'admin') {
            $this->fecha = date('Y-m-d');
            $this->sucursal_id = Auth::user()->sucursal_id;
        }        
    }


    public function render()
    {
        if ($this->sucursal_idF > 0 && (isset($this->fechaF) && $this->fechaF)) {
            $solicitudes  = SolicitudProducto::where('sucursal_id', $this->sucursal_idF)
            ->where('fecha', $this->fechaF)
            ->orderByDesc('fecha')
            ->paginate(10);
        } else {
            if (Auth::user()->type != 'admin') {
                if (isset($this->fechaF) && $this->fechaF) {
                    $solicitudes  = SolicitudProducto::where('sucursal_id', $this->sucursal_id)
                    ->where('fecha', $this->fechaF)
                    ->orderByDesc('fecha')->paginate(10);
                } else {
                    $solicitudes  = SolicitudProducto::where('sucursal_id', $this->sucursal_id)->orderByDesc('fecha')->paginate(10);
                }
            } else {
                $solicitudes  = SolicitudProducto::orderByDesc('fecha')->paginate(10);
            }
        }
        $this->resetPage();

        $sucursales = Sucursal::whereNull('deleted_at')->get(); 
        $productos = Producto::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get(); 
        return view('livewire.solicitud', compact('solicitudes', 'sucursales', 'productos'));
    }

    public function agregar($value) {
        if ($value>0) {
            $producto = Producto::find($value);
            if ($producto){
                $this->producto_id = $value;
                $this->precio_unitario_compra = $producto->precios->precio_venta ?? 0;
            }
        }
    }

    public function total($value) {
        if ($value>0) {
            $this->total_venta = round($this->precio_unitario_compra*$value,2);
        }
    }

    public function guardar() {

        $this->validate([
            //'sucursal_id'               => 'required|integer',
            'producto_id'               => 'required|integer',
            'fecha'                     => 'required',
            'cantidad'                  => 'required|numeric|min:0',
        ]);
        
        $solicitud = ($this->id) ? SolicitudProducto::updateOrCreate(
            [
                'id' => $this->id,
            ],
            [
                'producto_id'   => $this->producto_id,
                'sucursal_id'   => $this->sucursal_id,
                'fecha'         => $this->fecha,
                'cantidad'      => $this->cantidad,
                'created_at'    => now(),
                'updated_at'    => now()
            ])
        : SolicitudProducto::create(
            [
                'user_id'       => Auth::user()->id,
                'producto_id'   => $this->producto_id,
                'sucursal_id'   => $this->sucursal_id,
                'fecha'         => $this->fecha,
                'cantidad'      => $this->cantidad,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

        if ($solicitud) {
            
            
            $this->dispatch('alerta', [
                'title'             => 'Solicitud registrada exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);

            $this->modalAction('close');
            $this->resetPage();
        }
    }

    public function editar($id) {
        $this->id = $id;
        $solicitud = SolicitudProducto::find($this->id);
        $this->cantidad      = $solicitud->cantidad;
        $this->fecha         = $solicitud->fecha;
        $this->producto_id   = $solicitud->producto_id;
        $this->sucursal_id   = $solicitud->sucursal_id;
        $this->modalAction('open');
    }


    public function alerta($id) {
        $this->id = $id;
        $this->dispatch('eliminar-solicitud',[
            'title'                 => '',
            'text'                  => '¿Desea Eliminar el solicitud?',
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Eliminar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $solicitud = SolicitudProducto::find($this->id);
        if ($solicitud->delete()) {
            $this->id = null;
            $this->dispatch('alerta', [
                'title'             => 'Solicitud eliminado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }
    
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            if (Auth::user()->type == 'admin') {
                $this->reset([
                    'producto_id', 
                    'sucursal_id', 
                    'cantidad',
                    'fecha'
                ]);
            } else {
                $this->reset([
                    'producto_id', 
                    'cantidad',
                ]);
            }
        }

        $this->resetValidation();
    }
}
