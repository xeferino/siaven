<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Sucursal as Tienda;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;

class Sucursal extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar'];

    public $modal = 'hidden';
    public $id;
    #[Validate('required', message: 'El Nombre es requerido.')]
    #[Validate('min:3', message: 'El Nombre debe contener 3 letras como minimo.')]
    public $nombre;
    #[Validate('required', message: 'La Direccion es requerida.')]
    #[Validate('min:10', message: 'La Direccion debe contener 10 letras como minimo.')]
    public $direccion;
    #[Validate('required', message: 'El Telefono es requerido.')]
    #[Validate('min:9', message: 'El Telefono debe contener 9 caracteres como minimo.')]
    #[Validate('integer', message: 'El Telefono debe ser numerico.')]
    public $telefono;

    public function render()
    {
        $sucursales = Tienda::paginate(10);
        return view('livewire.sucursal', compact('sucursales'));
    }

    public function guardar() {

        $this->validate([
            'nombre'     => 'required|string|min:3',
            'direccion'  => 'required|string|min:10',
            'telefono'   => 'required|integer|min:9',
        ]);

        $sucursal = ($this->id) ? Tienda::updateOrCreate(
            [
                'id' => $this->id,
            ],
            [
                'nombre'        => $this->nombre,
                'direccion'     => $this->direccion,
                'telefono'      => $this->telefono,
                'created_at'    => now(),
                'updated_at'    => now()
            ])
        : Tienda::create(
            [
                'nombre'        => $this->nombre,
                'direccion'     => $this->direccion,
                'telefono'      => $this->telefono,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

        if ($sucursal) {
            $this->dispatch('alerta', [
                'title'             => ($this->id) ? 'Sucursal actualizada exitosamente.' : 'Sucursal registrada exitosamente.',
                'timer'             =>3000,
                'icon'              =>'success',
                'toast'             =>true,
                'showConfirmButton' => false,
                'position'          =>'top-right'
            ]);

            $this->modalAction('close');
            $this->reset([
                'nombre', 
                'direccion', 
                'telefono',
            ]);
            $this->resetPage();
        }
    }

    public function editar($idSurcursal) {
        $this->id = $idSurcursal;
        $sucursal = Tienda::find($this->id);
        $this->nombre       = $sucursal->nombre;
        $this->direccion    = $sucursal->direccion;
        $this->telefono     = $sucursal->telefono;
        $this->modalAction('open');
    }

    public function alerta($id) {
        $this->id = $id;
        $sucursal = Tienda::find($id);
        $this->dispatch('eliminar-sucursal',[
            'title'                 => $sucursal->nombre,
            'text'                  => '¿Desea Eliminar la sucursal?',
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Eliminar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $sucursal = Tienda::find($this->id);
        if ($sucursal->delete()) {
            $this->id = null;
            $this->dispatch('alerta', [
                'title'             => 'Sucursal eliminada exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }
    
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->reset([
                'nombre', 
                'direccion', 
                'telefono',
            ]);
        }
        $this->resetValidation();
    }
}
