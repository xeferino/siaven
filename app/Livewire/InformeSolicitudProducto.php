<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\DetalleTraspaso;
use App\Models\SolicitudProducto;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\Validate;

class InformeSolicitudProducto extends Component
{
    //#[Validate('required', message: 'La Sucursal es requerida.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $desde;
    #[Validate('after_or_equal:desde', message: 'La Fecha no puede ser menor.')]
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $hasta;
    public $sucursal;

    public $resultados = [];
    public $fechas = [];

    public function render()
    {
        $sucursales = Sucursal::whereNull('deleted_at')
                        ->where('id','<>', 1)
                        ->orderBy('nombre', 'ASC')
                        ->get();
        return view('livewire.informe-producto',compact(
            'sucursales'
        ));
    }

    public function buscar()
    {
       $this->validate([
            'desde'       => 'required',
            'hasta'       => 'required|after_or_equal:desde'
        ]);

        $sucursales = Sucursal::whereNull('deleted_at')
                        ->where('id','<>', 1)
                        ->orderBy('nombre', 'ASC')
                        ->get();

        $productos = SolicitudProducto::selectRaw('sum(cantidad) as cantidad, producto_id')
                        ->whereBetween('fecha', [$this->desde, $this->hasta])
                        ->whereNull('deleted_at')
                        ->orderBy('cantidad', 'DESC')
                        ->groupBy('producto_id')
                        ->get();
        

        $this->resultados = [];

        foreach ($productos as $key => $item) {
            $producto           =  $item->producto->nombre;
            $data = [];
            $total = 0;
            
            foreach ($sucursales as $key => $sucursal) {
                $cantidad = SolicitudProducto::where('sucursal_id', $sucursal->id)
                ->where('producto_id', $item->producto_id)
                ->whereBetween('fecha', [$this->desde, $this->hasta])
                ->sum('cantidad');

                array_push($data, [
                    'cantidad'     => $cantidad ?? 0,
                ]);
                $total += $cantidad;
            }

            array_push($this->resultados, [
                'producto'          => $producto,
                'sucursales'        => $data,
                'total'             => $total
            ]);
        }

        if (count($this->resultados)>0) {
            $this->dispatch('alerta', [
                'title'             => 'Informe de solicitudes de productos cargado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        } else {
            $this->dispatch('alerta', [
                'title'             => 'No hay solicitudes de productos, para el periodo seleccionado.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        }
    }

}
