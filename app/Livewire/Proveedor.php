<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Proveedor as Persona;
use App\Models\Sucursal;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;
use Illuminate\Support\Facades\Auth;

class Proveedor extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar'];

    public $modal = 'hidden';
    public $id;
    #[Validate('required', message: 'El RUT es requerido.')]
    #[Validate('unique:proveedores,rut', message: 'El RUT es ya esta en uso.')]
    public $rut;
    #[Validate('required', message: 'La Razon social es requerida.')]
    public $razon_social;
    #[Validate('required', message: 'La Direccion es requerida.')]
    #[Validate('min:10', message: 'La Direccion debe contener 10 letras como minimo.')]
    public $direccion;
    #[Validate('required', message: 'La Ciudad es requerida.')]
    public $ciudad;
    #[Validate('required', message: 'El Giro es requerida.')]
    public $giro;
    #[Validate('required', message: 'El Contacto es requerida.')]
    public $contacto;
    #[Validate('required', message: 'El Email es requerida.')]
    #[Validate('required', message: 'El Email es invalido.')]
    public $email;
    #[Validate('required', message: 'El Telefono 1 es requerido.')]
    #[Validate('min:9', message: 'El Telefono 1 debe contener 9 caracteres como minimo.')]
    #[Validate('integer', message: 'El Telefono 1 debe ser numerico.')]
    public $telefono1;
    #[Validate('required', message: 'El Telefono 2 es requerido.')]
    #[Validate('min:9', message: 'El Telefono 2 debe contener 9 caracteres como minimo.')]
    #[Validate('integer', message: 'El Telefono 2 debe ser numerico.')]
    public $telefono2;
    //#[Validate('required', message: 'La Sucursal es requerida.')]
    public $sucursal_id;
    
    public function render()
    {
        $proveedores    = Persona::paginate(10);
        $sucursales     = Sucursal::all();
        return view('livewire.proveedor', compact('proveedores','sucursales'));
    }

    public function guardar() {

        $this->validate([
            'rut'           => isset($this->id) ? 'required|unique:proveedores,rut,'.$this->id : 'required|unique:proveedores,rut',
            'razon_social'  => 'required',
            'ciudad'        => 'required',
            'giro'          => 'required',
            'contacto'      => 'required',
            'email'         => 'required|email',
            //'sucursal_id'   => 'required',
            'direccion'     => 'required|string|min:10',
            'telefono1'     => 'required|integer|min:9',
            'telefono2'     => 'required|integer|min:9',
        ]);

        $proveedor = ($this->id) ? Persona::updateOrCreate(
            [
                'id' => $this->id,
            ],
            [
                'rut'           => $this->rut,
                'razon_social'  => $this->razon_social,
                'ciudad'        => $this->ciudad,
                'giro'          => $this->giro,
                'contacto'      => $this->contacto,
                'email'         => $this->email,
                'sucursal_id'   => $this->sucursal_id,
                'direccion'     => $this->direccion,
                'telefono1'     => $this->telefono1,
                'telefono2'     => $this->telefono2,
                'created_at'    => now(),
                'updated_at'    => now()
            ])
        : Persona::create(
            [
                'rut'           => $this->rut,
                'razon_social'  => $this->razon_social,
                'ciudad'        => $this->ciudad,
                'giro'          => $this->giro,
                'contacto'      => $this->contacto,
                'email'         => $this->email,
                'sucursal_id'   => $this->sucursal_id,
                'direccion'     => $this->direccion,
                'telefono1'     => $this->telefono1,
                'telefono2'     => $this->telefono2,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

        if ($proveedor) {
            $this->dispatch('alerta', [
                'title'             => ($this->id) ? 'Proveedor actualizado exitosamente.' : 'Proveedor registrado exitosamente.',
                'timer'             =>3000,
                'icon'              =>'success',
                'toast'             =>true,
                'showConfirmButton' => false,
                'position'          =>'top-right'
            ]);

            $this->modalAction('close');
            $this->reset([
                'rut',          
                'razon_social',
                'ciudad',
                'giro',
                'contacto',
                'email',
                'sucursal_id',
                'direccion',
                'telefono1',
                'telefono2'
            ]);
            $this->resetPage();
        }
    }

    public function editar($idProveedor) {
        $this->id            = $idProveedor;
        $proveedor           = Persona::find($this->id);
        $this->rut           = $proveedor->rut;
        $this->razon_social  = $proveedor->razon_social;
        $this->ciudad        = $proveedor->ciudad;
        $this->giro          = $proveedor->giro;
        $this->contacto      = $proveedor->contacto;
        $this->email         = $proveedor->email;
        $this->sucursal_id   = $proveedor->sucursal_id;
        $this->direccion     = $proveedor->direccion;
        $this->telefono1     = $proveedor->telefono1;
        $this->telefono2     = $proveedor->telefono2;
        $this->modalAction('open');
    }
    
    public function alerta($id) {
        $this->id = $id;
        $proveedor = Persona::find($id);
        $this->dispatch('eliminar-proveedor',[
            'title'                 => $proveedor->razon_social,
            'text'                  => '¿Desea Eliminar el proveedor?',
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Eliminar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $proveedor = Persona::find($this->id);
        if ($proveedor->compras()->count()>0) {
            $this->dispatch('alerta', [
                'title'             => 'El proveedor, no puede ser eliminado ya esta asociado a una compra',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            if ($proveedor->delete()) {
                $this->id = null;
                $this->dispatch('alerta', [
                    'title'             => 'Proveedor eliminado exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            } else {
                $this->dispatch('alerta', [
                    'title'             => 'Ha ocurrido un error, intente nuevamente.',
                    'timer'             => 3000,
                    'icon'              => 'error',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            }
        }
    }
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->reset([
                'rut',          
                'razon_social',
                'ciudad',
                'giro',
                'contacto',
                'email',
                //'sucursal_id',
                'direccion',
                'telefono1',
                'telefono2'
            ]);
        }
        $this->resetValidation();
    }
}
