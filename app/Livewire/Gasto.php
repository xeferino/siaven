<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Gasto as TipoGasto;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;

class Gasto extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar'];

    public $modal = 'hidden';
    public $id;
    #[Validate('required', message: 'El nombre es requerido.')]
    #[Validate('min:3', message: 'El nombre debe contener 3 letras como minimo.')]
    public $nombre;

   
    public $telefono;

    public function render()
    {
        $gastos = TipoGasto::paginate(10);
        return view('livewire.gasto', compact('gastos'));
    }

    public function guardar() {

        $this->validate([
            'nombre'     => 'required|string|min:3',
        ]);

        $gasto = ($this->id) ? TipoGasto::updateOrCreate(
            [
                'id' => $this->id,
            ],
            [
                'nombre'        => $this->nombre,
                'created_at'    => now(),
                'updated_at'    => now()
            ])
        : TipoGasto::create(
            [
                'nombre'        => $this->nombre,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

        if ($gasto) {
            $this->dispatch('alerta', [
                'title'             => ($this->id) ? 'Tipo de gasto actualizado exitosamente.' : 'Tipo de gasto registrado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);

            $this->modalAction('close');
            $this->reset(['nombre']);
            $this->resetPage();
        }
    }
    public function editar($id) {
        $this->id = $id;
        $gasto = TipoGasto::find($this->id);
        $this->nombre       = $gasto->nombre;
        $this->modalAction('open');
    }

    public function alerta($id) {
        $this->id = $id;
        $gasto = TipoGasto::find($id);
        $this->dispatch('eliminar-gasto',[
            'title'                 => $gasto->nombre,
            'text'                  => '¿Desea Eliminar el tipo gasto?',
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Eliminar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $gasto = TipoGasto::find($this->id);
        if ($gasto->sucursales()->count()>0) {
            $this->dispatch('alerta', [
                'title'             => 'El tipo de gasto, no puede ser eliminado ya esta asociado a una sucursal',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            if ($gasto->delete()) {
                $this->id = null;
                $this->dispatch('alerta', [
                    'title'             => 'Tipo de gasto eliminado exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            } else {
                $this->dispatch('alerta', [
                    'title'             => 'Ha ocurrido un error, intente nuevamente.',
                    'timer'             => 3000,
                    'icon'              => 'error',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            }
        }

    }
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->reset(['nombre']);
        }
        $this->resetValidation();
    }
}
