<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Compra as Purchase;
use App\Models\DetalleCompra as DetallePurchase;
use App\Models\Sucursal;
use App\Models\Documento;
use App\Models\Pago;
use App\Models\Proveedor;
use App\Models\Producto;
use App\Models\PrecioProducto;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;
use Illuminate\Support\Facades\Auth;

class Compra extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar', 'anular', 'compra'];

    public $modal = 'hidden';
    public $id;
    public $compra_id;
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $fecha;
    //#[Validate('required', message: 'La Sucursal es requerida.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'El tipo de documento es requerido.')]
    #[Validate('integer', message: 'El tipo de documento debe ser entero.')]
    public $documento_id;
    #[Validate('required', message: 'El proveedor es requerido.')]
    #[Validate('integer', message: 'El proveedor debe ser entero.')]
    public $proveedor_id;
    #[Validate('required', message: 'El tipo de pago es requerido.')]
    #[Validate('integer', message: 'El tipo de pago debe ser entero.')]
    public $pago_id;
    #[Validate('required', message: 'El numero de documento es requerido.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $numero_documento;
    #[Validate('required', message: 'El numero de cheque documento es requerido.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $numero_cheque;
    #[Validate('required', message: 'El numero de cheque del banco es requerido.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $numero_banco_cheque;
    #[Validate('required', message: 'La compra, si o no esta paga es requerida.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $pagada = 'no';
    #[Validate('integer', message: 'El Producto debe ser entera.')]
    #[Validate('required', message: 'El Producto es requerido.')]
    public $producto_id;
    #[Validate('required', message: 'La Cantidad es requerida.')]
    #[Validate('numeric', message: 'La Cantidad debe ser numerica.')]
    #[Validate('min:1', message: 'La Cantidad debe ser mayor a cero.')]
    public $cantidad = 0;
    #[Validate('required', message: 'El Precio Unitario es requerida.')]
    #[Validate('numeric', message: 'El Precio Unitario debe ser numerico.')]
    public $precio_unitario_compra = 0;
    #[Validate('required', message: 'El Total es requerido.')]
    #[Validate('numeric', message: 'El Total debe ser numerico.')]
    public $total_venta = 0;
    #[Validate('required', message: 'El stock inicial es requerido.')]
    public $stock = 1;
    public $vista = null;
    #[Validate('required', message: 'La Fecha pago es requerida.')]
    public $fecha_pago;

    public function render()
    {
        //$compras        = Purchase::paginate(10);
        $compras        = Purchase::whereNull('deleted_at')->orderBy('fecha', 'DESC')->paginate(10);
        $sucursales     = Sucursal::whereNull('deleted_at')->get();
        $productos      = Producto::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        $proveedores    = Proveedor::whereNull('deleted_at')->get();
        $documentos     = Documento::whereNull('deleted_at')->get();
        $pagos          = Pago::whereNull('deleted_at')->get();
        
        return view('livewire.compra', compact(
            'compras',
            'sucursales',
            'productos',
            'proveedores',
            'documentos',
            'pagos'
        ));
    }

    public function agregar($value) {
        if ($value>0) {
            $producto = Producto::find($value);
            if ($producto){
                $this->producto_id = $value;
                $this->precio_unitario_compra = $producto->precios->precio_venta ?? 0;
            }
        }
    }

    public function total($value) {
        if ($value>0) {
            $this->total_venta = round($this->precio_unitario_compra*$value, 2);
        }
    }

    public function guardar() {

        $this->validate([
            'documento_id'              => 'required|integer',
            'pago_id'                   => $this->pagada == 'si' ? 'required|integer' : 'nullable|integer',
            'proveedor_id'              => 'required|integer',
            'numero_documento'          => 'required',
            'numero_cheque'             => $this->pagada == 'si' ? 'required' : 'nullable',
            'numero_banco_cheque'       => $this->pagada == 'si' ? 'required' : 'nullable',
            'fecha'                     => 'required',
            'pagada'                    => 'required',
            'stock'                     => 'required',
            'fecha_pago'                => ($this->id && $this->pagada == 'si') ? 'required' : 'nullable',
        ]);

        if ($this->id) {
            $compra  = Purchase::find($this->id);
            $compra->fecha = $this->fecha;
            $compra->documento_id = $this->documento_id;
            $compra->proveedor_id = $this->proveedor_id;
            $compra->pago_id = $this->pagada == 'si' ? $this->pago_id: null;
            $compra->sucursal_id = $this->sucursal_id;
            $compra->nro_documento  = $this->numero_documento;
            $compra->nro_cheque  = $this->pagada == 'si' ? $this->numero_cheque: null;
            $compra->nro_cheque_banco  = $this->pagada == 'si' ? $this->numero_banco_cheque : null;
            $compra->pagada = $this->pagada == 'si' ? 1 : 0;
            $compra->fecha_pago = $this->pagada == 'si' ? $this->fecha_pago : null;
            $compra->nota = ($this->stock) ? 'stock inicial' : null;
            
            if ($compra->save()) {
                $this->id = null;
                $this->dispatch('alerta', [
                    'title'             => 'Compra actualizada exitosamente',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          =>'top-right'
                ]);
                $this->modalAction('close');
                $this->resetPage();
            }
        } else {
            $compra = Purchase::where('proveedor_id', $this->proveedor_id)
                    ->where('documento_id', $this->documento_id)
                    ->where('nro_documento', $this->numero_documento)
                    ->whereNull('deleted_at')
                    ->first();
            if (!$compra) {
                $compra = Purchase::create([
                    'fecha'             => $this->fecha,
                    'proveedor_id'      => $this->proveedor_id,
                    //'monto'             => $this->monto,
                    'estado'            => 0,
                    'pagada'            => ($this->pagada == 'si') ? 1 : 0,
                    'fecha_pago'        => ($this->pagada == 'si') ? $this->fecha : null,
                    'pago_id'           => ($this->pagada == 'si') ? $this->pago_id : null,
                    'documento_id'      => $this->documento_id,
                    'nro_cheque'        => ($this->pagada == 'si') ? $this->numero_cheque : null,
                    'nro_cheque_banco'  => ($this->pagada == 'si') ? $this->numero_banco_cheque : null,
                    'nro_documento'     => $this->numero_documento,
                    'nota'              => ($this->stock) ? 'stock inicial' : null,
                    'sucursal_id'       => $this->sucursal_id,
                    'created_at'        => now(),
                    'updated_at'        => now()
                ]);   
            }
    
            if ($compra) {
                $this->compra_id = $compra->id;
                $this->dispatch('compra-guardada', [
                    'title'             => 'Compra registrada exitosamente, redireccionando...',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          =>'top-right'
                ]);
                $this->modalAction('close');
                $this->resetPage();
            }
        }
    }

    public function editar($id) {
        $compra  = Purchase::find($id);
        $this->id = $id;
        $this->fecha = $compra->fecha;
        $this->documento_id = $compra->documento_id;
        $this->proveedor_id = $compra->proveedor_id;
        $this->pago_id = $compra->pago_id;
        $this->sucursal_id = $compra->sucursal_id;
        $this->numero_documento = $compra->nro_documento;
        $this->numero_cheque = $compra->nro_cheque;
        $this->numero_banco_cheque = $compra->nro_cheque_banco;
        $this->pagada = $compra->pagada ? 'si' : 'no';
        $this->fecha_pago = $compra->fecha_pago;
        $this->stock = $compra->nota == 'stock inicial' ? 1 : 0;
        $this->vista = 'editar';
        $this->modalAction('open');   
    }

    public function alerta($id, $tipo = null) {
        $this->id = $id;
        $compra = Purchase::find($id);
        $titulo = '¿Desea Eliminar la compra?';
        $boton = 'Eliminar';
        $evento = 'eliminar-compra';
        if ($tipo == 'anular') {
            $titulo = $compra->estado ? '¿Desea reactivar la compra?' : '¿Desea anular la compra?';
            $evento = 'anular-compra';
            $boton  = $compra->estado ? 'Aceptar' : 'Anular';
        }
        $this->dispatch($evento, [
            'title'                 => 'Compra #'.str_pad($compra->id, 6, "0", STR_PAD_LEFT),
            'text'                  => $titulo,
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => $boton,
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $compra = Purchase::find($this->id);
        if ($compra->delete()) {
            $this->id = null;
            $this->dispatch('alerta', [
                'title'             => 'Compra eliminada exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }
    
    public function anular() {
        $compra = Purchase::find($this->id);
        $compra->estado = $compra->estado ? 0 : 1;
        if ($compra->save()) {
            $this->id = null;
            $this->dispatch('alerta', [
                'title'             => $compra->estado ? 'Compra anulada exitosamente.' :  'Compra reativada exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->vista = null;
            $this->reset([
                'documento_id',
                'pago_id',
                'proveedor_id',
                'numero_documento',
                'numero_cheque',
                'numero_banco_cheque',
                'fecha',
                'pagada',
                'stock'                     
            ]);
        }
        $this->resetValidation();
    }

    public function compra() {
        return redirect()->route('compras.detalles', ['id' => $this->compra_id]);
    }
}
