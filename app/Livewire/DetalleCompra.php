<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Compra as Purchase;
use App\Models\DetalleCompra as DetallePurchase;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\PrecioProducto;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;
use Illuminate\Support\Facades\Auth;

class DetalleCompra extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar', 'anular'];

    public $modal = 'hidden';
    public $id;
    public $detalle_compra_id;
    #[Validate('required', message: 'La Sucursal es requerida.')]
    #[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'La Compra es requerido.')]
    #[Validate('integer', message: 'La Compra debe ser entero.')]
    public $compra_id;
    #[Validate('integer', message: 'El Producto debe ser entera.')]
    #[Validate('required', message: 'El Producto es requerido.')]
    public $producto_id;
    #[Validate('required', message: 'La Cantidad es requerida.')]
    #[Validate('numeric', message: 'La Cantidad debe ser numerica.')]
    #[Validate('min:0', message: 'La Cantidad debe ser mayor a cero.')]
    public $cantidad = 0;
    #[Validate('required', message: 'El Precio Unitario de compra es requerida.')]
    #[Validate('numeric', message: 'El Precio Unitario de compra debe ser numerico.')]
    #[Validate('min:0', message: 'El Precio Unitario de compra debe ser mayor a cero.')]
    public $precio_unitario_compra = 0;
    #[Validate('required', message: 'El Precio Unitario de guia es requerida.')]
    #[Validate('numeric', message: 'El Precio Unitario de guia debe ser numerico.')]
    #[Validate('min:0', message: 'El Precio Unitario de guia debe ser mayor a cero.')]
    public $precio_unitario_guia = 0;
    #[Validate('required', message: 'El Total de producto es requerido.')]
    #[Validate('numeric', message: 'El Total de producto debe ser numerico.')]
    #[Validate('min:0', message: 'El Total de producto debe ser mayor a cero.')]
    public $total_compra = 0;
    #[Validate('required', message: 'El Total de guia es requerido.')]
    #[Validate('numeric', message: 'El Total de guia debe ser numerico.')]
    #[Validate('min:0', message: 'El Total de guia debe ser mayor a cero.')]
    public $total_guia = 0;

    public function render()
    {
        $compra = Purchase::find($this->id);
        if ($compra) {
            $this->sucursal_id = $compra->sucursal_id;
            $this->compra_id = $compra->id;
            $compras = DetallePurchase::where('compra_id', $compra->id)->whereNull('deleted_at')->paginate(10);
        }

        $sucursales = Sucursal::whereNull('deleted_at')->get();
        $productos  = Producto::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        return view('livewire.detalle-compra', compact('compras', 'sucursales', 'productos'));
    }


    public function agregar($value) {
        if ($value>0) {
            $producto = Producto::find($value);
            if ($producto){
                $compra = Purchase::find($this->id);
                $this->producto_id = $value;
                $this->precio_unitario_compra = $producto::precio($value, $compra->fecha) ?? 0;
            }
        }
    }

    public function total($value) {
        if ($value>0) {
            $this->total_compra = round($value*$this->cantidad, 2);
        }
    }

    public function guia($value) {
        if ($value>0) {
            $this->total_guia = round($value*$this->cantidad, 2);
        }
    }

    public function guardar() {

        $this->validate([
            'compra_id'                 => 'required|integer',
            'producto_id'               => 'required|integer',
            'cantidad'                  => 'required|numeric|min:0',
            'precio_unitario_compra'    => 'required|numeric|min:0',
            'total_compra'              => 'required|numeric|min:0',
            'precio_unitario_guia'      => 'required|numeric|min:0',
            'total_guia'                => 'required|numeric|min:0',
        ]);

        $compra = DetallePurchase::create([
            'compra_id'         => $this->compra_id,
            'producto_id'       => $this->producto_id,
            'cantidad'          => $this->cantidad,
            'precio_unitario'   => $this->precio_unitario_compra,
            'precio_guia'       => $this->precio_unitario_guia,
            'estado'            => 0,
            'created_at'        => now(),
            'updated_at'        => now()
        ]);

        if ($compra) {
            $detalle = DetallePurchase::selectRaw('detalle_compras.precio_unitario, compras.fecha')
                        ->leftJoin('compras', 'detalle_compras.compra_id', '=', 'compras.id')
                        ->whereNull('compras.deleted_at')
                        ->whereNull('detalle_compras.deleted_at')
                        ->orderByDesc('compras.fecha')
                        ->limit(1)
                        ->get();
            
            Producto::find($this->producto_id)->update(['precio_compra' => $detalle[0]->precio_unitario]);
            $this->dispatch('alerta', [
                'title'             => 'Prodcuto registrado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          =>'top-right'
            ]);
            $this->modalAction('close');
            $this->resetPage(); 
        }
    }

    public function alerta($id, $tipo = null) {
        $this->detalle_compra_id = $id;
        $compra = DetallePurchase::find($id);
        $titulo = '¿Desea Eliminar el producto?';
        $boton = 'Eliminar';
        $evento = 'eliminar-detalle-compra';
        if ($tipo == 'anular') {
            $titulo = $compra->estado ? '¿Desea reactivar el producto?' : '¿Desea anular el producto?';
            $evento = 'anular-detalle-compra';
            $boton  = $compra->estado ? 'Aceptar' : 'Anular';
        }
        $this->dispatch($evento, [
            'title'                 => $compra->producto->nombre,
            'text'                  => $titulo,
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => $boton,
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $compra = DetallePurchase::find($this->detalle_compra_id);
        if ($compra->delete()) {
            $this->detalle_compra_id = null;
            $this->dispatch('alerta', [
                'title'             => 'Producto eliminado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }

    public function anular() {
        $compra = DetallePurchase::find($this->detalle_compra_id);
        $compra->estado = $compra->estado ? 0 : 1;
        if ($compra->save()) {
            $this->detalle_compra_id = null;
            $this->dispatch('alerta', [
                'title'             => $compra->estado ? 'Producto anulado exitosamente.' :  'Producto reativado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }
    
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->detalle_compra_id = null;
            $this->reset([
                'compra_id', 
                'producto_id', 
                'precio_unitario_compra',
                'total_compra',
                'cantidad',
                'precio_unitario_guia',
                'total_guia',
                'cantidad'
            ]);
        }
        $this->resetValidation();
    }
}
