<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Perdida as TipoPerdida;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;

class Perdida extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar'];

    public $modal = 'hidden';
    public $id;
    #[Validate('required', message: 'El nombre es requerido.')]
    #[Validate('min:3', message: 'El nombre debe contener 3 letras como minimo.')]
    public $nombre;

   
    public $telefono;

    public function render()
    {
        $perdidas = TipoPerdida::paginate(10);
        return view('livewire.perdida', compact('perdidas'));
    }

    public function guardar() {

        $this->validate([
            'nombre'     => 'required|string|min:3',
        ]);

        $perdida = ($this->id) ? TipoPerdida::updateOrCreate(
            [
                'id' => $this->id,
            ],
            [
                'nombre'        => $this->nombre,
                'created_at'    => now(),
                'updated_at'    => now()
            ])
        : TipoPerdida::create(
            [
                'nombre'        => $this->nombre,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

        if ($perdida) {
            $this->dispatch('alerta', [
                'title'             => ($this->id) ? 'Tipo de perdida actualizada exitosamente.' : 'Tipo de perdida registrada exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);

            $this->modalAction('close');
            $this->reset(['nombre']);
            $this->resetPage();
        }
    }
    public function editar($id) {
        $this->id = $id;
        $perdida = TipoPerdida::find($this->id);
        $this->nombre       = $perdida->nombre;
        $this->modalAction('open');
    }

    public function alerta($id) {
        $this->id = $id;
        $perdida = TipoPerdida::find($id);
        $this->dispatch('eliminar-perdida',[
            'title'                 => $perdida->nombre,
            'text'                  => '¿Desea Eliminar el tipo perdida?',
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Eliminar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $perdida = TipoPerdida::find($this->id);
        if ($perdida->productos()->count()>0) {
            $this->dispatch('alerta', [
                'title'             => 'El tipo de perdida, no puede ser eliminada ya esta asociada a un producto',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            if ($perdida->delete()) {
                $this->id = null;
                $this->dispatch('alerta', [
                    'title'             => 'Tipo de perdida eliminada exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            } else {
                $this->dispatch('alerta', [
                    'title'             => 'Ha ocurrido un error, intente nuevamente.',
                    'timer'             => 3000,
                    'icon'              => 'error',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            }
        }
    }
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->reset(['nombre']);
        }
        $this->resetValidation();
    }
}
