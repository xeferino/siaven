<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\DetalleTraspaso;
use App\Models\SolicitudProducto;
use App\Models\Traspaso;
use App\Models\Venta;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\Validate;

class InformeVentaProducto extends Component
{
    //#[Validate('required', message: 'La Sucursal es requerida.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $desde;
    #[Validate('after_or_equal:desde', message: 'La Fecha no puede ser menor.')]
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $hasta;
    public $sucursal;

    public $resultados = [];
    public $fechas = [];

    public function render()
    {
        $sucursales = Sucursal::whereNull('deleted_at')
                        ->where('id','<>', 1)
                        ->orderBy('nombre', 'ASC')
                        ->get();
        return view('livewire.venta-producto',compact(
            'sucursales'
        ));
    }

    public function buscar()
    {
       $this->validate([
            'desde'       => 'required',
            'hasta'       => 'required|after_or_equal:desde'
        ]);

        $sucursales = Sucursal::whereNull('deleted_at')
                        ->where('id','<>', 1)
                        ->orderBy('nombre', 'ASC')
                        ->get();

        $productos  = Producto::whereNull('deleted_at')
                        ->orderBy('nombre', 'ASC')
                        ->get();

        $this->resultados = [];

        $this->resultados = Venta::whereBetween('fecha', [$this->desde, $this->hasta])
        ->whereNull('deleted_at')
        ->where('nota', null)
        ->get();

        if (count($this->resultados)>0) {
            $this->dispatch('alerta', [
                'title'             => 'Informe de ventas de productos cargado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        } else {
            $this->dispatch('alerta', [
                'title'             => 'No hay ventas de productos, para el periodo seleccionado.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        }
    }

}
