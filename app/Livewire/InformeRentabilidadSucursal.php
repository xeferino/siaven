<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\DetalleTraspaso;
use App\Models\DetalleVenta;
use App\Models\SolicitudProducto;
use App\Models\Traspaso;
use App\Models\Venta;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\Validate;

class InformeRentabilidadSucursal extends Component
{
    //#[Validate('required', message: 'La Sucursal es requerida.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $desde;
    #[Validate('after_or_equal:desde', message: 'La Fecha no puede ser menor.')]
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $hasta;
    public $sucursal;

    public $resultados = [];
    public $fechas = [];

    public function render()
    {
        $sucursales = Sucursal::whereNull('deleted_at')
                        ->where('id','<>', 1)
                        ->orderBy('nombre', 'ASC')
                        ->get();
        return view('livewire.informe-rentabilidad-sucursal',compact(
            'sucursales'
        ));
    }

    public function buscar()
    {
       $this->validate([
            'desde'       => 'required',
            'hasta'       => 'required|after_or_equal:desde'
        ]);

        $sucursales = Sucursal::whereNull('deleted_at')
                        ->where('id','<>', 1)
                        ->orderBy('nombre', 'ASC')
                        ->get();

        $this->resultados = [];

        $ventas = Venta::whereBetween('fecha', [$this->desde, $this->hasta])
                        ->whereNull('deleted_at')
                        ->whereNull('nota')
                        ->pluck('id');
        $productos = DetalleVenta::selectRaw('detalle_ventas.producto_id')
                        ->leftJoin('productos', 'productos.id', '=', 'detalle_ventas.producto_id')
                        ->whereIn('detalle_ventas.venta_id', $ventas)
                        ->whereNull('detalle_ventas.deleted_at')
                        ->groupBy('detalle_ventas.producto_id')
                        ->orderBy('productos.nombre', 'ASC')
                        ->get();

        foreach ($productos as $key => $item) {
            $producto           =  $item->producto->nombre;
            $data = [];
            $total = 0;
            
            foreach ($sucursales as $key => $sucursal) {
                $venta = DetalleVenta::selectRaw('
                    sum(detalle_ventas.cantidad*detalle_ventas.precio_unitario) as monto,
                    detalle_ventas.producto_id,
                    ventas.sucursal_id,
                    sum(detalle_ventas.cantidad) as cantidad
                ')
                ->leftJoin('ventas', 'detalle_ventas.venta_id', '=', 'ventas.id')
                ->whereBetween('ventas.fecha', [$this->desde, $this->hasta])
                ->where('detalle_ventas.producto_id', $item->producto_id)
                ->whereIn('detalle_ventas.venta_id', $ventas)
                ->where('ventas.sucursal_id', $sucursal->id)
                ->whereNull('ventas.deleted_at')
                ->whereNull('detalle_ventas.deleted_at')
                ->groupBy('detalle_ventas.producto_id')
                ->groupBy('ventas.sucursal_id')
                ->get();

                $cantidad = round($venta[0]->cantidad ?? 0, 2);
                $venta_total = round($venta[0]->monto ?? 0, 2);
                $costo_actual_unitario = round($item->producto->precio_compra ?? 0, 2);
                $costo_total = round($item->producto->precio_compra ?? 0*$venta[0]->cantidad ?? 0 , 2);
                $margen = round($venta[0]->monto ?? 0 - $costo_total ?? 0, 2);

                array_push($data, [
                    'cantidad'              => $cantidad ?? 0,
                    'venta_total'           => $venta_total ?? 0,
                    'costo_actual_unitario' => $costo_actual_unitario ?? 0,
                    'costo_total'           => $costo_total ?? 0,
                    'margen'                => $margen ?? 0,
                ]);
            }

            array_push($this->resultados, [
                'producto'          => $producto,
                'sucursales'        => $data,
            ]);
        }

        if (count($this->resultados)>0) {
            $this->dispatch('alerta', [
                'title'             => 'Informe de despacho sucursales cargado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        } else {
            $this->dispatch('alerta', [
                'title'             => 'No hay despacho a sucursales, para el periodo seleccionado.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        }
    }

}
