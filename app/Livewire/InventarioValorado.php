<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\DetalleTraspaso;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\Validate;

class InventarioValorado extends Component
{
    //#[Validate('required', message: 'La Sucursal es requerida.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $fecha;
    public $sucursal;

    public $resultados = [];

    public function render()
    {
        $sucursales = Sucursal::whereNull('deleted_at')
                        ->where('id','<>', 1)
                        ->orderBy('nombre', 'ASC')
                        ->get();

        return view('livewire.inventario-valorado',compact(
            'sucursales'
        ));
    }

    public function buscar()
    {
        $this->validate([
            //'sucursal_id' => 'required|integer',
            'fecha'       => 'required'
        ]);

        $sucursales = Sucursal::whereNull('deleted_at')
                        ->where('id','<>', 1)
                        ->orderBy('nombre', 'ASC')
                        ->get();
        $productos = Producto::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        $this->resultados = [];

        foreach ($productos as $key => $item) {
            $producto           = Producto::find($item->id);
            $nombre             = $producto->nombre;
            $stock              = round($producto->stock($item->id, $this->fecha), 2);
            $data = [];
            $total = 0;
            $valorizacion = 0;
            
            foreach ($sucursales as $key => $sucursal) {
                $cantidad           = round(DetalleTraspaso::cantidad($producto->id, $sucursal->id, $this->fecha)[0]->cantidad ?? 0, 2);
                $saldo              = round(Sucursal::stockAyer($producto->id, $sucursal->id, $this->fecha)[0]->saldo ?? 0, 2);
                $stockSucursal      = round($cantidad+$saldo, 2);
                $total              += round($stockSucursal, 2);
                
                array_push($data, [ 
                'stockSucursal'     => $stockSucursal,
                ]);
            }
            if ($stock>0) {
                $valorizacion       += round($producto->precio_compra*$total,2);
                array_push($this->resultados, [
                    'producto'          => $producto,
                    'nombre'            => $nombre,
                    'stock'             => $stock,
                    'sucursales'        => $data,
                    'total'             => $total,
                    'valorizacion'      => $valorizacion
                ]);
            }
        }

        if (count($this->resultados)>0) {
            $this->dispatch('alerta', [
                'title'             => 'Inventario valorado cargado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        }
    }

}
