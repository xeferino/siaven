<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Traspaso as Despacho;
use App\Models\DetalleTraspaso as DetalleDespacho;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\PrecioProducto;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;
use Illuminate\Support\Facades\Auth;

class DetalleTraspaso extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar', 'producto'];

    public $modal = 'hidden';
    public $id;
    public $detalle_traspaso_id;
    #[Validate('required', message: 'La Sucursal es requerida.')]
    #[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'El Traspaso es requerido.')]
    #[Validate('integer', message: 'El Traspaso debe ser entero.')]
    public $traspaso_id;
    #[Validate('integer', message: 'El Producto debe ser entera.')]
    #[Validate('required', message: 'El Producto es requerido.')]
    public $producto_id;
    #[Validate('required', message: 'La Cantidad es requerida.')]
    #[Validate('numeric', message: 'La Cantidad debe ser numerica.')]
    #[Validate('min:0', message: 'La Cantidad debe ser mayor a cero.')]
    public $cantidad = 0;
    #[Validate('required', message: 'El Precio Unitario es requerida.')]
    #[Validate('numeric', message: 'El Precio Unitario debe ser numerico.')]
    public $precio_unitario_compra = 0;
    #[Validate('required', message: 'El Total es requerido.')]
    #[Validate('numeric', message: 'El Total debe ser numerico.')]
    public $total_venta = 0;
    public $tipo = 0;

    public function render()
    {
        $traspaso = Despacho::find($this->id);
        if ($traspaso) {
            $this->sucursal_id = $traspaso->sucursal_id;
            $this->traspaso_id = $traspaso->id;
            $traspasos = DetalleDespacho::selectRaw('detalle_traspasos.*')
            ->leftJoin('productos', 'detalle_traspasos.producto_id', '=', 'productos.id')
            ->where('detalle_traspasos.traspaso_id', $traspaso->id)
            ->whereNull('detalle_traspasos.deleted_at')
            ->whereNull('productos.deleted_at')
            ->orderBy('productos.nombre', 'ASC')
            ->paginate(10);
        }
        $sucursales = Sucursal::whereNull('deleted_at')->get();
        $productos = Producto::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        return view('livewire.detalle-traspaso', compact('traspasos', 'sucursales', 'productos', 'traspaso'));
    }


    public function agregar($value) {
        if ($value>0) {
            $producto = Producto::find($value);
            if ($producto){
                $traspaso = Despacho::find($this->id);
                $this->producto_id = $value;
                $this->precio_unitario_compra = $producto::precio($value, $traspaso->fecha) ?? 0;
            }
        }
    }

    public function total($value) {
        if ($value>0) {
            $this->total_venta = round($this->precio_unitario_compra*$value,2);
        }
    }

    public function guardar() {

        $this->validate([
            'traspaso_id'               => 'required|integer',
            'sucursal_id'               => 'required|integer',
            'producto_id'               => 'required|integer',
            'cantidad'                  => 'required|numeric|min:0',
            'precio_unitario_compra'    => 'required|numeric',
            'total_venta'               => 'required|numeric',
        ]);

        $detalleTraspaso = DetalleDespacho::where('producto_id', $this->producto_id)
                ->where('traspaso_id', $this->traspaso_id)
                ->whereNull('deleted_at')
                ->first();

        $traspaso = ($detalleTraspaso) ? DetalleDespacho::updateOrCreate(
            [
                'id' => $detalleTraspaso->id,
            ],
            [
                'traspaso_id'       => $this->traspaso_id,
                'producto_id'       => $this->producto_id,
                'cantidad'          => $this->cantidad,
                'precio_unitario'   => $this->precio_unitario_compra,
                'created_at'        => now(),
                'updated_at'        => now()
            ])
        : DetalleDespacho::create([
                'traspaso_id'       => $this->traspaso_id,
                'producto_id'       => $this->producto_id,
                'cantidad'          => $this->cantidad,
                'precio_unitario'   => $this->precio_unitario_compra,
                'created_at'        => now(),
                'updated_at'        => now()
            ]);

        if ($traspaso) {
            $this->dispatch('alerta', [
                'title'             => ($this->detalle_traspaso_id) ? 'Traspaso actualizado exitosamente.' : 'Traspaso registrado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          =>'top-right'
            ]);

            $this->modalAction('close');
            $this->reset([
                'traspaso_id', 
                'sucursal_id', 
                'producto_id', 
                'precio_unitario_compra',
                'total_venta',
                'cantidad'
            ]);
            $this->resetPage(); 
        }
    }

    public function alerta($id, $tipo = null) {
        $this->detalle_traspaso_id = $id;
        $detalle  = DetalleDespacho::find($id);
        $titulo = '¿Desea Eliminar el producto?';
        $boton  = 'Eliminar';
        $evento = 'eliminar-detalle-traspaso';
        $value = 0;
        $input = null;
        $this->tipo = $tipo;

        if ($tipo == 'cantidad') {
            $titulo     = 'Nueva cantidad';
            $evento     = 'cantidad-detalle-traspaso';
            $input      = 'text';
            $boton      = 'Aceptar';
            $value      =  $detalle ? $detalle->cantidad : 0;
        }

        $this->dispatch($evento,[
            'title'                 => '',
            'input'                 => $input,
            'text'                  => $titulo,
            'inputValue'            => $value,
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => $boton,
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function producto () 
    {
        if ($this->cantidad == 0 or $this->cantidad == null or !is_numeric($this->cantidad)) {
            $this->dispatch('alerta', [
                'title'             => 'Cantidad incorrecta.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            if (isset($this->tipo) && $this->tipo == 'cantidad') {
                $detalle = DetalleDespacho::where('id', $this->detalle_traspaso_id)
                        ->update(['cantidad' => $this->cantidad]);
                if ($detalle) {
                    $this->detalle_traspaso_id = null;
                    $this->cantidad = 0;
                    $this->dispatch('alerta', [
                        'title'             => 'Cantidad actualizada exitosamente.',
                        'timer'             => 3000,
                        'icon'              => 'success',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          => 'top-right'
                    ]);
                } 
            }
        }
    }

    public function eliminar() {
        $traspaso = DetalleDespacho::find($this->detalle_traspaso_id);
        if ($traspaso->delete()) {
            $this->detalle_traspaso_id = null;
            $this->dispatch('alerta', [
                'title'             => 'Producto eliminado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }
    
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->detalle_traspaso_id = null;
            $this->reset([
                'traspaso_id', 
                'sucursal_id', 
                'producto_id', 
                'precio_unitario_compra',
                'total_venta',
                'cantidad'
            ]);
        }
        $this->resetValidation();
    }
}
