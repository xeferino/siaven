<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Documento as TipoDocumento;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;

class Documento extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar'];

    public $modal = 'hidden';
    public $id;
    #[Validate('required', message: 'El nombre es requerido.')]
    #[Validate('min:3', message: 'El nombre debe contener 3 letras como minimo.')]
    public $nombre;

   
    public $telefono;

    public function render()
    {
        $documentos = TipoDocumento::paginate(10);
        return view('livewire.documento', compact('documentos'));
    }

    public function guardar() {

        $this->validate([
            'nombre'     => 'required|string|min:3',
        ]);

        $documento = ($this->id) ? TipoDocumento::updateOrCreate(
            [
                'id' => $this->id,
            ],
            [
                'nombre'        => $this->nombre,
                'created_at'    => now(),
                'updated_at'    => now()
            ])
        : TipoDocumento::create(
            [
                'nombre'        => $this->nombre,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

        if ($documento) {
            $this->dispatch('alerta', [
                'title'             => ($this->id) ? 'Tipo de documento actualizado exitosamente.' : 'Tipo de documento registrado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);

            $this->modalAction('close');
            $this->reset(['nombre']);
            $this->resetPage();
        }
    }
    public function editar($id) {
        $this->id = $id;
        $documento = TipoDocumento::find($this->id);
        $this->nombre       = $documento->nombre;
        $this->modalAction('open');
    }

    public function alerta($id) {
        $this->id = $id;
        $documento = TipoDocumento::find($id);
        $this->dispatch('eliminar-documento',[
            'title'                 => $documento->nombre,
            'text'                  => '¿Desea Eliminar el tipo documento?',
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Eliminar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $documento = TipoDocumento::find($this->id);
        if ($documento->compras()->count()>0 or $documento->ventas()->count()>0) {
            $this->dispatch('alerta', [
                'title'             => 'El tipo de documento, no puede ser eliminado ya esta asociado a una venta o compra',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            if ($documento->delete()) {
                $this->id = null;
                $this->dispatch('alerta', [
                    'title'             => 'Tipo de documento eliminado exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            } else {
                $this->dispatch('alerta', [
                    'title'             => 'Ha ocurrido un error, intente nuevamente.',
                    'timer'             => 3000,
                    'icon'              => 'error',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            }
        }
    }
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->reset(['nombre']);
        }
        $this->resetValidation();
    }
}
