<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Producto;
use App\Models\Cliente;
use Livewire\WithPagination;

class Historial extends Component
{
    use WithPagination;

    public $id;
    public $tipo = null;

    public function render()
    {   
        if ($this->tipo == 'producto') {
            $producto = Producto::find($this->id);
            $historial = $producto->historial()->orderByDesc('fecha')->paginate(5);
            $titulo = 'Historial precio de productos';
            $columnas = ['Precio', 'Fecha'];
        }

        if ($this->tipo == 'cliente') {
            $cliente = Cliente::find($this->id);
            $historial = $cliente->historial()->orderByDesc('id')->paginate(5);
            $titulo = 'Historial creditos';
            $columnas = ['Monto', 'Fecha'];
        }

        return view('livewire.historial', [
            'historial' => $historial,
            'titulo'    => $titulo,
            'columnas'  => $columnas,
            'tipo'      => $this->tipo,
        ]);
    }
}
