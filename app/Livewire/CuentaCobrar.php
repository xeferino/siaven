<?php

namespace App\Livewire;

use App\Models\Cliente;
use Livewire\Component;
use App\Models\Compra as Purchase;
use App\Models\DetalleCompra as DetallePurchase;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\PrecioProducto;
use App\Models\Venta;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;
use Illuminate\Support\Facades\Auth;

class CuentaCobrar extends Component
{
    use WithPagination;
    protected $listeners = ['pagada'];

    public $modal = 'hidden';
    public $id;
    public $compra_id;
    public $monto;
    
    public function render()
    {
        $ventas = Venta::where('estado', 0)
                        ->where('pagada', 0)
                        ->orderByDesc('fecha')
                        ->paginate(10);
        $clientes       = Cliente::whereNull('deleted_at')->orderBy('razon_social', 'ASC')->get();

        return view('livewire.cxc', compact('ventas', 'clientes'));
    }

    public function alerta($id, $monto = null) {
        $this->compra_id = $id;
        $this->monto = $monto;
        $compra = DetallePurchase::find($id);
        $titulo = '¿Desea pagar la venta?';

        $this->dispatch('compra-alerta', [
            'text'                  => $titulo,
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Aceptar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function pagada() {
        $compra = Venta::find($this->compra_id);
        $compra->pagada = 1;
        if ($compra->save()) {
            $credito = $compra->cliente->credito->monto ?? 0;
            $compra->cliente->credito->update(['monto' => $credito+$this->monto]);
            $this->compra_id = null;
            $this->monto = null;
            $this->dispatch('alerta', [
                'title'             => 'Cuenta pagada exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }

    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->compra_id = null;
            $this->reset([
                'compra_id', 
                'producto_id', 
                'precio_unitario_compra',
                'total_compra',
                'cantidad',
                'precio_unitario_guia',
                'total_guia',
                'cantidad'
            ]);
        }
        $this->resetValidation();
    }
}
