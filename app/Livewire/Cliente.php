<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Cliente as Persona;
use App\Models\CreditoCliente;
use App\Models\Sucursal;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;
use Illuminate\Support\Facades\Auth;

class Cliente extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar'];

    public $modal = 'hidden';
    public $id;
    #[Validate('required', message: 'El RUT es requerido.')]
    #[Validate('unique:clientes,rut', message: 'El RUT es ya esta en uso.')]
    public $rut;
    #[Validate('required', message: 'La Razon social es requerida.')]
    public $razon_social;
    #[Validate('required', message: 'La Direccion es requerida.')]
    #[Validate('min:3', message: 'La Direccion debe contener 3 letras como minimo.')]
    public $direccion;
    #[Validate('required', message: 'La Ciudad es requerida.')]
    public $ciudad;
    #[Validate('required', message: 'El Giro es requerida.')]
    public $giro;
    #[Validate('required', message: 'El Contacto es requerida.')]
    public $contacto;
    #[Validate('required', message: 'El Email es requerida.')]
    #[Validate('required', message: 'El Email es invalido.')]
    public $email;
    #[Validate('required', message: 'El Telefono 1 es requerido.')]
    #[Validate('min:9', message: 'El Telefono 1 debe contener 9 caracteres como minimo.')]
    #[Validate('integer', message: 'El Telefono 1 debe ser numerico.')]
    public $telefono1;
    //#[Validate('required', message: 'El Telefono 2 es requerido.')]
    #[Validate('min:9', message: 'El Telefono 2 debe contener 9 caracteres como minimo.')]
    #[Validate('integer', message: 'El Telefono 2 debe ser numerico.')]
    public $telefono2;
    //#[Validate('required', message: 'La Sucursal es requerida.')]
    public $sucursal_id;
    public $vista = null;
    #[Validate('required', message: 'El monto del credito 1 es requerido.')]
    #[Validate('integer', message: 'El monto del credito 1 debe ser numerico.')]
    public $credito = null;
    public $cliente_id;
    
    public function render()
    {
        if ($this->cliente_id > 0) {
            $clientes  = Persona::where('id', $this->cliente_id)
                        ->whereNull('deleted_at')
                        ->orderBy('razon_social', 'ASC')
                        ->paginate(10);
        } else {
            $this->cliente_id = 0;
            $clientes  = Persona::whereNull('deleted_at')
            ->orderBy('razon_social', 'ASC')
            ->paginate(10);
        }
        $this->resetPage();
        
        $sucursales = Sucursal::all();
        $clientesf = Persona::whereNull('deleted_at')->orderBy('razon_social', 'ASC')->get();
        return view('livewire.cliente', compact('clientes','sucursales', 'clientesf'));
    }

    public function guardar() {

        $this->validate([
            'rut'              => isset($this->id) ? 'required|unique:clientes,rut,'.$this->id : 'required|unique:clientes,rut',
            //'rut'           => 'required',
            'razon_social'  => 'required',
            'ciudad'        => 'required',
            'giro'          => 'required',
            'contacto'      => 'required',
            'email'         => 'required|email',
            //'sucursal_id'   => 'required',
            'direccion'     => 'required|string|min:3',
            'telefono1'     => 'required|integer|min:9',
            'telefono2'     => 'nullable|integer|min:9',
        ]);

        $cliente = ($this->id) ? Persona::updateOrCreate(
            [
                'id' => $this->id,
            ],
            [
                'rut'           => $this->rut,
                'razon_social'  => $this->razon_social,
                'ciudad'        => $this->ciudad,
                'giro'          => $this->giro,
                'contacto'      => $this->contacto,
                'email'         => $this->email,
                'sucursal_id'   => $this->sucursal_id,
                'direccion'     => $this->direccion,
                'telefono1'     => $this->telefono1,
                'telefono2'     => $this->telefono2,
                'created_at'    => now(),
                'updated_at'    => now()
            ])
        : Persona::create(
            [
                'rut'           => $this->rut,
                'razon_social'  => $this->razon_social,
                'ciudad'        => $this->ciudad,
                'giro'          => $this->giro,
                'contacto'      => $this->contacto,
                'email'         => $this->email,
                'sucursal_id'   => $this->sucursal_id,
                'direccion'     => $this->direccion,
                'telefono1'     => $this->telefono1,
                'telefono2'     => $this->telefono2,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

        if ($cliente) {
            if ($this->id) {
                $cliente = Persona::find($this->id);
                $monto = $cliente->credito->monto ?? 0;
                if ($monto > 0 && isset($this->credito)) {
                    $cliente->credito->update(['monto' => $this->credito]);
                }
            }
            $this->dispatch('alerta', [
                'title'             => ($this->id) ? 'Cliente actualizado exitosamente.' : 'Cliente registrado exitosamente.',
                'timer'             =>3000,
                'icon'              =>'success',
                'toast'             =>true,
                'showConfirmButton' => false,
                'position'          =>'top-right'
            ]);

            $this->modalAction('close');
            $this->reset([
                'rut',          
                'razon_social',
                'ciudad',
                'giro',
                'contacto',
                'email',
                'sucursal_id',
                'direccion',
                'telefono1',
                'telefono2'
            ]);
            $this->resetPage();
        }
    }

    public function editar($idCliente) {
        $this->id = $idCliente;
        $cliente = Persona::find($this->id);
        $this->rut           = $cliente->rut;
        $this->razon_social  = $cliente->razon_social;
        $this->ciudad        = $cliente->ciudad;
        $this->giro          = $cliente->giro;
        $this->contacto      = $cliente->contacto;
        $this->email         = $cliente->email;
        $this->sucursal_id   = $cliente->sucursal_id;
        $this->direccion     = $cliente->direccion;
        $this->telefono1     = $cliente->telefono1;
        $this->telefono2     = $cliente->telefono2;
        $this->credito       = $cliente->credito->monto ?? 0;
        $this->modalAction('open');
    }

    public function alerta($id) {
        $this->id = $id;
        $cliente = Persona::find($id);
        $this->dispatch('eliminar-cliente',[
            'title'                 => $cliente->razon_social,
            'text'                  => '¿Desea Eliminar el cliente?',
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Eliminar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $cliente = Persona::find($this->id);
        if ($cliente->ventas()->count()>0) {
            $this->dispatch('alerta', [
                'title'             => 'El cliente, no puede ser eliminado ya esta asociado a una venta',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            if ($cliente->delete()) {
                $this->id = null;
                $this->dispatch('alerta', [
                    'title'             => 'Cliente eliminado exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            } else {
                $this->dispatch('alerta', [
                    'title'             => 'Ha ocurrido un error, intente nuevamente.',
                    'timer'             => 3000,
                    'icon'              => 'error',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            }
        }
    }

    public function historialCredito($idCliente) {
        $this->id = $idCliente;
        if ($this->id) {
            $this->vista = 'historial';
            $this->modalAction('open');   
        }
    }

    public function agregarCredito($idCliente) {
        $this->id = $idCliente;
        if ($this->id) {
            /* $cliente = Persona::find($this->id);
            $monto = $cliente->credito->monto ?? 0;
            if ($monto > 0) {
                $this->dispatch('alerta', [
                    'title'             => 'El cliente tiene credito positivo, para agregar debe estar en cero',
                    'timer'             => 3000,
                    'icon'              => 'error',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          =>'top-right'
                ]);
                return;
            }    */         
            $this->vista = 'credito';
            $this->modalAction('open');   
        }
    }

    public function guadarCredito() {
        if ($this->id) {
            $credito = CreditoCliente::create([
                'fecha'         => date('Y-m-d'),
                'monto'         => $this->credito,
                'cliente_id'    => $this->id,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);
            if ($credito) {
                $this->dispatch('alerta', [
                    'title'             => 'Credito agredado exitosamente',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          =>'top-right'
                ]);
            }
            $this->modalAction('close');   
        }
    }
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->vista = null;
            $this->reset([
                'rut',          
                'razon_social',
                'ciudad',
                'giro',
                'contacto',
                'email',
                'sucursal_id',
                'direccion',
                'telefono1',
                'telefono2',
                'credito'
            ]);
            $this->resetPage();
        }
        $this->resetValidation();
    }
}
