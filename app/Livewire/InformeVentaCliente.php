<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\DetalleTraspaso;
use App\Models\SolicitudProducto;
use App\Models\Traspaso;
use App\Models\Venta;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\Validate;

class InformeVentaCliente extends Component
{
    //#[Validate('required', message: 'La Sucursal es requerida.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $desde;
    #[Validate('after_or_equal:desde', message: 'La Fecha no puede ser menor.')]
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $hasta;
    public $sucursal;

    public $resultados = [];
    public $fechas = [];

    public function render()
    {
        $sucursales = Sucursal::whereNull('deleted_at')
                        ->where('id','<>', 1)
                        ->orderBy('nombre', 'ASC')
                        ->get();
        return view('livewire.venta-cliente',compact(
            'sucursales'
        ));
    }

    public function buscar()
    {
       $this->validate([
            'desde'       => 'required',
            'hasta'       => 'required|after_or_equal:desde'
        ]);

        $this->resultados = [];

        $ventas = Venta::whereBetween('fecha', [$this->desde, $this->hasta])
        ->whereNull('deleted_at')
        ->where('nota', null)
        ->where('estado', '<>', 1)
        ->get();

        $contado = 0;
        $credito = 0;
        foreach ($ventas as $key => $value) {
            # code...
            if ($value->pagada) {
                $contado += $value->historial->sum('precio_unitario');
            } else {
                $credito += $value->historial->sum('precio_unitario');
            }
            array_push($this->resultados, [
                'id'        => $value->id,
                'fecha'     => $value->fecha,
                'cliente'   => $value->cliente->rut. ' '.$value->cliente->razon_social,
                'descuento' => $value->descuento ?? 0,
                'contado'   => $contado,
                'credito'   => $credito,
            ]);
        }

        if (count($this->resultados)>0) {
            $this->dispatch('alerta', [
                'title'             => 'Informe de ventas de clientes cargado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        } else {
            $this->dispatch('alerta', [
                'title'             => 'No hay ventas de clientes, para el periodo seleccionado.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        }
    }

}
