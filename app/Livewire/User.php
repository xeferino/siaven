<?php

namespace App\Livewire;

use App\Models\Empleado;
use Livewire\Component;
use App\Models\User as Usuario;
use App\Models\Sucursal;
use Illuminate\Support\Facades\Auth;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;
use Illuminate\Support\Facades\Hash;

class User extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar', 'activar'];

    public $modal = 'hidden';
    public $id;
    public $name;
    public $email;
    public $password;
    public $password_confirm;
    public $type;
    public $usuario;
    public $sucursal_id;
    public $empleado_id;
   
    
    public function render()
    {
        $usuarios   = Usuario::paginate(10);
        $sucursales = Sucursal::all();
        $empleados  = Empleado::where('sucursal_id', $this->sucursal_id)->get();
        $roles = ['admin', 'sucursal', 'despachador'];
        return view('livewire.usuario', compact('usuarios','sucursales', 'empleados', 'roles'));
    }

    public function guardar() {         
        $this->validate([
            'email'             => isset($this->id) ? 'nullable|email|unique:users,email,'.$this->id : 'nullable|email|unique:users,email',
            'name'              => isset($this->id) ? 'required|min:4|unique:users,name,'.$this->id : 'required|unique:users,name|min:4',
            'password'          => isset($this->id) ? 'nullable|min:8|max:16' : 'required|min:8|max:16',
            'password_confirm'  => isset($this->id) ? 'nullable|min:8|max:16|required_with:password|same:password' : 'required|min:8|max:16|required_with:password|same:password',
            'sucursal_id'       => 'required',
            'empleado_id'       => 'required',
            'type'              => 'required'
        ]);

        $usuario = ($this->id) ? Usuario::updateOrCreate(
            [
                'id' => $this->id,
            ],
            [
                'email'             => $this->email,
                'name'              => $this->name,
                'password'          => $this->password ? Hash::make($this->password) : $this->usuario->password,
                'sucursal_id'       => $this->type == 1 ? 1 :$this->sucursal_id,
                'empleado_id'       => $this->empleado_id,
                'type'              => $this->type,
                'email_verified_at' => now(),
                'created_at'        => now(),
                'updated_at'        => now()
            ])
        : Usuario::create(
            [
                'email'             => $this->email,
                'name'              => $this->name,
                'password'          => Hash::make($this->password),
                'sucursal_id'       => $this->type == 1 ? 1 :$this->sucursal_id,
                'empleado_id'       => $this->empleado_id,
                'type'              => $this->type,
                'email_verified_at' => now(),
                'created_at'        => now(),
                'updated_at'        => now()
            ]);

        if ($usuario) {
            $this->dispatch('alerta', [
                'title'             => ($this->id) ? 'Usuario actualizado exitosamente.' : 'Usuario registrado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);

            $this->modalAction('close');
            $this->reset([
                'email',          
                'name',
                'password',
                'password_confirm',
                'sucursal_id',
                'empleado_id',
                'type'
            ]);
            $this->resetPage();
        }
    }

    public function editar($idUsuario) {
        $this->id = $idUsuario;
        $usuario = Usuario::find($this->id);
        $this->usuario      = $usuario;
        $this->email        = $usuario->email;
        $this->name         = $usuario->name;
        $this->sucursal_id  = $usuario->sucursal_id;
        $this->empleado_id  = $usuario->empleado_id;
        $this->type         = $usuario->type;
        $this->modalAction('open');
    }

    public function activar() {
        $usuario = Usuario::find($this->id);
        $usuario->activo = $usuario->activo ? 0 : 1;
        if ($usuario->save()) {
            $this->id = null;
            $this->dispatch('alerta', [
                'title'             => 'Usuario actualizado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }
    

    public function alerta($id, $tipo = null) {
        $this->id = $id;
        $usuario = Usuario::find($id);
        if ($tipo == 'activar') {
            $this->dispatch('activar-usuario',[
                'title'                 => $usuario->name,
                'text'                  => $usuario->activo ? '¿Desea bloquear el usuario?' : '¿Desea desbloquear el usuario?',
                'icon'                  => 'question',
                'toast'                 => false,
                'showConfirmButton'     => true,
                'confirmButtonText'     => 'Aceptar',
                'showCancelButton'      => true,
                'cancelButtonText'      => 'Cancelar',
            ]);
        } else {
            $this->dispatch('eliminar-usuario',[
                'title'                 => $usuario->nombre,
                'text'                  => '¿Desea Eliminar el usuario?',
                'icon'                  => 'question',
                'toast'                 => false,
                'showConfirmButton'     => true,
                'confirmButtonText'     => 'Eliminar',
                'showCancelButton'      => true,
                'cancelButtonText'      => 'Cancelar',
            ]);
        }
    }

    public function eliminar() {
        $usuario = Usuario::find($this->id);
        if ($usuario->delete()) {
            $this->id = null;
            $this->dispatch('alerta', [
                'title'             => 'Usuario eliminado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }

    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->reset([
                'email',          
                'name',
                'password',
                'password_confirm',
                'sucursal_id',
                'empleado_id',
                'type'
            ]);
        }
        $this->resetValidation();
    }
}
