<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Traspaso as Despacho;
use App\Models\DetalleTraspaso as DetalleDespacho;
use App\Models\CierrePlanillaDiaria as Planilla;
use App\Models\StockSucursal;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\PrecioProducto;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;
use Illuminate\Support\Facades\Auth;

class Traspaso extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar', 'traspaso'];

    public $modal = 'hidden';
    public $id;
    public $traspaso_id;

    #[Validate('required', message: 'La Fecha es requerida.')]
    public $fecha;
    public $fechaF;
    //#[Validate('required', message: 'La Sucursal es requerida.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_idF;
    public $sucursal_id;
    #[Validate('integer', message: 'El Producto debe ser entera.')]
    #[Validate('required', message: 'El Producto es requerido.')]
    public $producto_id;
    #[Validate('required', message: 'La Cantidad es requerida.')]
    #[Validate('numeric', message: 'La Cantidad debe ser numerica.')]
    #[Validate('min:1', message: 'La Cantidad debe ser mayor a cero.')]
    public $cantidad = 0;
    #[Validate('required', message: 'El Precio Unitario es requerida.')]
    #[Validate('numeric', message: 'El Precio Unitario debe ser numerico.')]
    public $precio_unitario_compra = 0;
    #[Validate('required', message: 'El Total es requerido.')]
    #[Validate('numeric', message: 'El Total debe ser numerico.')]
    public $total_venta = 0;

    public function render()
    {
        if ($this->sucursal_idF > 0) {
            $traspasos  = Despacho::where('sucursal_id', $this->sucursal_idF)
            ->orderByDesc('fecha')
            ->paginate(10);
        } else {
            $traspasos  = Despacho::orderByDesc('fecha')->paginate(10);
        }
        $this->resetPage();

        $sucursales = Sucursal::whereNull('deleted_at')->get(); 
        $productos = Producto::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get(); 
        return view('livewire.traspaso', compact('traspasos', 'sucursales', 'productos'));
    }

    public function agregar($value) {
        if ($value>0) {
            $producto = Producto::find($value);
            if ($producto){
                $this->producto_id = $value;
                $this->precio_unitario_compra = $producto->precios->precio_venta ?? 0;
            }
        }
    }

    public function total($value) {
        if ($value>0) {
            $this->total_venta = round($this->precio_unitario_compra*$value,2);
        }
    }

    public function guardar() {

        $this->validate([
            //'sucursal_id'               => 'required|integer',
            'producto_id'               => 'required|integer',
            'fecha'                     => 'required',
            'cantidad'                  => 'required|numeric|min:1',
            'precio_unitario_compra'    => 'required|numeric',
            'total_venta'               => 'required|numeric',
        ]);
        $valid = Despacho::where('fecha', $this->fecha)->where('sucursal_id', $this->sucursal_id)->whereNull('deleted_at')->first();
        if ($valid) {
            $sucursal = Sucursal::find($this->sucursal_id);
            $this->dispatch('alerta', [
                'title'             => 'La sucursal '.$sucursal->nombre.' ya tiene un traspaso para la fecha '.date('Y-m-d'),
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          =>'top-right'
            ]);
            $this->modalAction('close');
        } else {

            $traspaso = ($this->id) ? Despacho::updateOrCreate(
                [
                    'id' => $this->id,
                ],
                [
                    'sucursal_id'   => $this->sucursal_id,
                    'fecha'         => $this->fecha,
                    'created_at'    => now(),
                    'updated_at'    => now()
                ])
            : Despacho::create(
                [
                    'sucursal_id'   => $this->sucursal_id,
                    'fecha'         => $this->fecha,
                    'created_at'    => now(),
                    'updated_at'    => now()
                ]);
    
            if ($traspaso) {
                $detalle = DetalleDespacho::create([
                        'traspaso_id'       => $traspaso->id,
                        'producto_id'       => $this->producto_id,
                        'cantidad'          => $this->cantidad,
                        'precio_unitario'   => $this->precio_unitario_compra,
                        'created_at'        => now(),
                        'updated_at'        => now()
                    ]);
                $this->traspaso_id = $traspaso->id;
                
                $this->dispatch('traspaso-guardado', [
                    'title'             => 'Traspaso exitosamente, redireccionando...',
                    'timer'             =>3000,
                    'icon'              =>'success',
                    'toast'             =>true,
                    'showConfirmButton' => false,
                    'position'          =>'top-right'
                ]);
    
                $this->modalAction('close');
                $this->reset([
                    'sucursal_id', 
                    'producto_id', 
                    'precio_unitario_compra',
                    'total_venta',
                    'cantidad',
                    'fecha'
                ]);
                $this->resetPage();
            }
        }
    }

    public function alerta($id) {
        $this->id = $id;
        $this->dispatch('eliminar-traspaso',[
            'title'                 => '',
            'text'                  => '¿Desea Eliminar el traspaso?',
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Eliminar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $traspaso = Despacho::find($this->id);
        $planilla = Planilla::where('sucursal_id', $traspaso->sucursal_id)->where('fecha', $traspaso->fecha)->first();
        if ($planilla) {
            $this->dispatch('alerta', [
                'title'             => 'El traspaso no se puede eliminar, ya existe un movimiento de planilla diaria',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            if ($traspaso->delete()) {
                $this->id = null;
                StockSucursal::where('sucursal_id', $traspaso->sucursal_id)->where('fecha', $traspaso->fecha)->delete();
                $this->dispatch('alerta', [
                    'title'             => 'Traspaso eliminado exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            } else {
                $this->dispatch('alerta', [
                    'title'             => 'Ha ocurrido un error, intente nuevamente.',
                    'timer'             => 3000,
                    'icon'              => 'error',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            }
        }
    }
    
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->reset([
                'sucursal_id', 
                'producto_id', 
                'precio_unitario_compra',
                'total_venta',
                'cantidad',
                'fecha'
            ]);
        }
        $this->resetValidation();
    }

    public function traspaso() {
        return redirect()->route('traspasos.detalles', ['id' => $this->traspaso_id]);
    }
}
