<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Producto as Product;
use App\Models\PrecioProducto;
use App\Models\Proveedor;
use App\Models\Categoria;
use App\Models\Sucursal;
use App\Models\Venta;
use App\Models\DetalleVenta;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;
use Illuminate\Support\Facades\Auth;

class Producto extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar', 'stock'];

    public $modal = 'hidden';
    public $id;
    #[Validate('required', message: 'El codigo es requerido.')]
    #[Validate('min:3', message: 'El codigo debe contener 3 letras como minimo.')]
    #[Validate('unique:productos,codigo', message: 'El codigo es ya esta en uso.')]
    public $codigo;
    #[Validate('required', message: 'El nombre es requerido.')]
    #[Validate('min:4', message: 'El nombre debe contener 3 letras como minimo.')]
    #[Validate('unique:productos,nombre', message: 'El nombre es ya esta en uso.')]
    public $nombre;
    //#[Validate('required', message: 'La Sucursal es requerida.')]
    public $sucursal_id;
    #[Validate('required', message: 'La categoria es requerida.')]
    public $categoria_id;
    //#[Validate('required', message: 'El Proveedor es requerido.')]
    public $proveedor_id;
    #[Validate('required', message: 'La Descripcion es requerida.')]
    #[Validate('min:2', message: 'La Descripcion debe contener 2 letras como minimo.')]
    public $descripcion;
    //#[Validate('required', message: 'El precio de compra es requerido.')]
    //#[Validate('integer', message: 'El precio de compra debe ser numerico.')]
    public $precio_compra;
    #[Validate('required', message: 'El precio de venta es requerido.')]
    #[Validate('integer', message: 'El precio de venta debe ser numerico.')]
    public $precio_venta;
    #[Validate('required', message: 'la fecha es requerida.')]
    public $fecha;
    public $cantidad;
    public $producto_id;

    public $vista = null;
    
    public function render()
    {
        if ($this->producto_id > 0) {
            $productos  = Product::where('id', $this->producto_id)
                        ->whereNull('deleted_at')
                        ->orderBy('nombre', 'ASC')
                        ->paginate(10);
        } else {
            $this->producto_id = 0;
            $productos  = Product::whereNull('deleted_at')
                        ->orderBy('nombre', 'ASC')
                        ->paginate(10);
        }

        $this->resetPage();
        $productosf      = Product::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        $categorias     = Categoria::whereNull('deleted_at')->get();
        $sucursales     = Sucursal::whereNull('deleted_at')->get();
        $proveedores    = Proveedor::whereNull('deleted_at')->get();
        return view('livewire.producto', compact('productos','sucursales', 'proveedores', 'categorias', 'productosf'));
    }

    public function guardar() {

        $this->validate([
            'codigo'           => isset($this->id) ? 'required|min:3|unique:productos,codigo,'.$this->id : 'required|min:3|unique:productos,codigo',
            'nombre'           => isset($this->id) ? 'required|min:4|unique:productos,nombre,'.$this->id : 'required|min:4|unique:productos,nombre',
            'descripcion'      => 'required|min:2',
            //'sucursal_id'      => 'required',
            //'proveedor_id'     => 'required',
            'categoria_id'     => 'required', 
            'precio_compra'    => 'required|integer',
            'precio_venta'     => 'required|integer',
        ]);

        $producto = ($this->id) ? Product::updateOrCreate(
            [
                'id' => $this->id,
            ],
            [
                'codigo'        => $this->codigo,
                'nombre'        => $this->nombre,
                'descripcion'   => $this->descripcion,
                'sucursal_id'   => $this->sucursal_id,
                'categoria_id'  => $this->categoria_id,
                'proveedor_id'  => $this->proveedor_id,
                'precio_compra' => $this->precio_compra,
                'created_at'    => now(),
                'updated_at'    => now()
            ])
        : Product::create(
            [
                'codigo'        => $this->codigo,
                'nombre'        => $this->nombre,
                'descripcion'   => $this->descripcion,
                'sucursal_id'   => $this->sucursal_id,
                'categoria_id'  => $this->categoria_id,
                'proveedor_id'  => $this->proveedor_id,
                'precio_compra' => $this->precio_compra,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

        if ($producto) {
            $precio = PrecioProducto::where('producto_id', $producto->id)->where('fecha', date('Y-m-d'))->whereNull('deleted_at')->first();

            if ($precio) {
                $precio = PrecioProducto::where('producto_id', $producto->id)
                        ->where('fecha', date('Y-m-d'))
                        ->whereNull('deleted_at')
                        ->update(['precio_venta' => $this->precio_venta]);
            } else {
                PrecioProducto::create([
                    'fecha'         => date('Y-m-d'),
                    'precio_venta'  => $this->precio_venta,
                    'producto_id'   => $producto->id,
                    'created_at'    => now(),
                    'updated_at'    => now()
                ]);
            }


            $this->dispatch('alerta', [
                'title'             => ($this->id) ? 'Producto actualizado exitosamente.' : 'Producto registrado exitosamente.',
                'timer'             =>3000,
                'icon'              =>'success',
                'toast'             =>true,
                'showConfirmButton' => false,
                'position'          =>'top-right'
            ]);

            $this->modalAction('close');
            $this->reset([
                'codigo',          
                'nombre',
                'descripcion',
                'sucursal_id',
                'categoria_id',
                'proveedor_id',
                'precio_compra',
                'precio_venta'
            ]);
            $this->resetPage();
        }
    }

    public function editar($idProducto) {
        $this->id = $idProducto;
        $producto = Product::find($this->id);
        $this->codigo           = $producto->codigo;
        $this->nombre           = $producto->nombre;
        $this->descripcion      = $producto->descripcion;
        $this->categoria_id     = $producto->categoria_id;
        $this->sucursal_id      = $producto->sucursal_id;
        $this->proveedor_id     = $producto->proveedor_id;
        $this->precio_compra    = $producto->precio_compra;
        $this->precio_venta     = $producto->precios->precio_venta;
        $this->modalAction('open');
    }

    public function alerta($id, $tipo = null) {
        $this->id = $id;
        $producto = Product::find($id);
        $titulo = '¿Desea Eliminar el producto?';
        $boton = 'Eliminar';
        $evento = 'eliminar-producto';
        $value = 0;
        $input = false;
        if ($tipo == 'stock') {
            $titulo = '¿Desea actualizar el stock?';
            $evento = 'stock-producto';
            $boton  = 'Actualizar';
            $input  = true;
            $value  = round(Product::stock($id), 2);
        }

        $this->dispatch($evento, [
            'title'                 => $producto->nombre,
            'text'                  => $titulo,
            'input'                 => $input ? "text" : $input,
            'icon'                  => 'question',
            'inputValue'            => $value,
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => $boton,
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function stock() 
    {
        $producto = Product::find($this->id);
        $stockactual    = Product::stock($this->id);
        $stocknuevo     = $this->cantidad;

        $stock         = $stockactual-$stocknuevo;
        if ($stock<>0) {
            $venta = Venta::create([
                'fecha'         => date('Y-m-d'),
                'fecha_pago'    => date('Y-m-d'),
                'nota'          => 'correccion de inventario',
                'sucursal_id'   => 1,
                'pagada'        => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

            if ($venta) {
                DetalleVenta::create([
                    'cantidad'      => $stock,
                    'venta_id'      => $venta->id,
                    'producto_id'   => $producto->id,
                    'created_at'    => now(),
                    'updated_at'    => now()
                ]);
                $this->cantidad = 0;
                $this->id = null;
                $productos = Product::paginate(10);
                $this->resetPage();
                $this->dispatch('alerta', [
                    'title'             => 'El stock del producto se actualizo, exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            }

        }
    }

    public function eliminar() {
        $producto = Product::find($this->id);
        if ($producto->compras()->count()>0 or $producto->ventas()->count()>0 or $producto->stockSucursal()->count()>0) {
            $this->dispatch('alerta', [
                'title'             => 'El producto no puede ser eliminado, ya esta asociado a una venta o compra',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            if ($producto->delete()) {
                $this->id = null;
                $this->dispatch('alerta', [
                    'title'             => 'Producto eliminado exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            } else {
                $this->dispatch('alerta', [
                    'title'             => 'Ha ocurrido un error, intente nuevamente.',
                    'timer'             => 3000,
                    'icon'              => 'error',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            }
        }
    }

    public function historialPrecios($idProducto) {
        $this->id = $idProducto;
        if ($this->id) {
            $this->vista = 'historial';
            $this->modalAction('open');   
        }
    }

    public function precio($idProducto) {
        $this->id = $idProducto;
        if ($this->id) {
            $this->vista = 'precio';
            $this->modalAction('open');   
        }
    }

    public function nuevoPrecio() {
        $this->validate([
            'fecha'         => 'required',
            'precio_venta'  => 'required|integer',
        ]);

        $precio = PrecioProducto::where('producto_id', $this->id)->where('fecha', $this->fecha)->whereNull('deleted_at')->first();

        if ($precio) {
            $precio = PrecioProducto::where('producto_id', $this->id)
                    ->where('fecha', $this->fecha)
                    ->whereNull('deleted_at')
                    ->update(['precio_venta' => $this->precio_venta]);
        } else {
            PrecioProducto::create([
                'fecha'         => $this->fecha,
                'precio_venta'  => $this->precio_venta,
                'producto_id'   => $this->id,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);
        }

        $this->modalAction('close');
        $this->dispatch('alerta', [
            'title'             => 'Precio de Producto actualizado exitosamente',
            'timer'             => 3000,
            'icon'              => 'success',
            'toast'             => true,
            'showConfirmButton' => false,
            'position'          => 'top-right'
        ]);
    }


    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->vista = null;
            $this->reset([
                'codigo',          
                'nombre',
                'descripcion',
                'sucursal_id',
                'categoria_id',
                'proveedor_id',
                'precio_compra',
                'precio_venta',
                'fecha'
            ]);
            $this->resetPage();
        }
        $this->resetValidation();
    }
}
