<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\CierrePlanillaDiaria;
use App\Models\Gasto;
use App\Models\GastoSucursal;
use App\Models\Ingreso;
use App\Models\IngresoSucursal;
use App\Models\Perdida;
use App\Models\PerdidaProducto;
use App\Models\SueldoSucursal;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\Validate;

class Libro extends Component
{
    #[Validate('required', message: 'La Sucursal es requerida.')]
    #[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $desde;
    #[Validate('after_or_equal:desde', message: 'La Fecha no puede ser menor.')]
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $hasta;
    public $sucursal;

    public $ingresos = [];
    public $gastos = [];
    public $sueldos = [];
    public $perdidas = [];
    public $resultados = [];

    public $fechas = [];

    public function render()
    {
        $sucursales = Sucursal::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        $ingresos = Ingreso::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        return view('livewire.libro-ingreso-gasto', compact('sucursales', 'ingresos'));
    }

    public function buscar()
    {
        $this->validate([
            'desde'       => 'required',
            'hasta'       => 'required|after_or_equal:desde'
        ]);

        $this->ingresos = [];
        $sucursales = Sucursal::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        $ingresos = Ingreso::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();

        foreach ($ingresos as $key => $ingreso) {
            $sucursalesIngreso = [];
            $totalIngreso = 0;

            foreach ($sucursales as $key => $sucursal) {
                $monto = IngresoSucursal::where('sucursal_id', $sucursal->id)
                ->where('ingreso_id', $ingreso->id)
                ->whereBetween('fecha', [$this->desde, $this->hasta])
                ->whereNull('deleted_at')
                ->sum('monto');
                array_push($sucursalesIngreso, [
                    'monto'     => $monto,
                ]);
                $totalIngreso += $monto;
            }

            array_push($this->ingresos, [
                'nombre'     => $ingreso->nombre,
                'sucursales' => $sucursalesIngreso,
                'total'      => $totalIngreso,
            ]);
        }

        $this->gastos = [];
        $sucursales = Sucursal::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        $gastos = Gasto::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();

        foreach ($gastos as $key => $gasto) {
            $sucursalesGasto = [];
            $totalGasto = 0;

            foreach ($sucursales as $key => $sucursal) {
                $monto = GastoSucursal::where('sucursal_id', $sucursal->id)
                ->where('gasto_id', $gasto->id)
                ->whereBetween('fecha', [$this->desde, $this->hasta])
                ->whereNull('deleted_at')
                ->sum('monto');
                array_push($sucursalesGasto, [
                    'monto'     => $monto,
                ]);
                $totalGasto += $monto;
            }

            array_push($this->gastos, [
                'nombre' => $gasto->nombre,
                'sucursales' => $sucursalesGasto,
                'total'      => $totalGasto,
            ]);
        }

        $this->perdidas = [];
        $sucursales = Sucursal::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        $perdidas = Perdida::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();

        foreach ($perdidas as $key => $perdida) {
            $sucursalesPerdida = [];
            $totalPerdida = 0;

            foreach ($sucursales as $key => $sucursal) {
                $monto = PerdidaProducto::where('sucursal_id', $sucursal->id)
                ->where('perdida_id', $perdida->id)
                ->whereBetween('fecha', [$this->desde, $this->hasta])
                ->whereNull('deleted_at')
                ->sum('monto');
                array_push($sucursalesPerdida, [
                    'monto'     => $monto,
                ]);
                $totalPerdida += $monto;
            }

            array_push($this->perdidas, [
                'nombre'        => $perdida->nombre,
                'sucursales'    => $sucursalesPerdida,
                'total'         => $totalPerdida,
            ]);
        }

        $this->sueldos = [];
        foreach ($sucursales as $key => $sucursal) {
            $monto = SueldoSucursal::where('sucursal_id', $sucursal->id)
            ->whereBetween('fecha', [$this->desde, $this->hasta])
            ->whereNull('deleted_at')
            ->sum('monto');
            array_push($this->sueldos, [
                'monto'     => $monto,
            ]);
        }



        $this->resultados = [];

        array_push($this->resultados, [
            'gastos'        => $this->gastos,
            'ingresos'      => $this->ingresos,
            'perdidas'      => $this->perdidas,
            'sueldos'       => $this->sueldos,
        ]);

        if (count($this->resultados)>0) {
            $this->dispatch('alerta', [
                'title'             => 'Informe de gastos e ingresos cargado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        }
    }

}
