<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\CierrePlanillaDiaria;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\Validate;

class ResumenRendicion extends Component
{
    #[Validate('required', message: 'La Sucursal es requerida.')]
    #[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $desde;
    #[Validate('after_or_equal:desde', message: 'La Fecha no puede ser menor.')]
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $hasta;
    public $sucursal;

    public $resultados = [];
    public $fechas = [];

    public function render()
    {
        $sucursales = Sucursal::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        return view('livewire.resumen-rendicion', compact('sucursales'));
    }

    public function buscar()
    {
        $this->validate([
            'desde'       => 'required',
            'hasta'       => 'required|after_or_equal:desde'
        ]);

        $hasta = strtotime($this->hasta);
        $desde = strtotime($this->desde);
        $hoy=$hasta;
        $this->fechas = [];
        do {
            array_push($this->fechas, date('Y-m-d', $hoy));
            $hoy-=(24*60*60);
        } while ($hoy>=$desde);

        if (count($this->fechas)>0) {
            $this->dispatch('alerta', [
                'title'             => 'Resumen de rendiciones cargado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        }

    }

}
