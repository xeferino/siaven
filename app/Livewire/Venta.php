<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Venta as Sale;
use App\Models\DetalleVenta as DetalleSale;
use App\Models\Sucursal;
use App\Models\Documento;
use App\Models\Pago;
use App\Models\Cliente;
use App\Models\Producto;
use App\Models\PrecioProducto;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;
use Illuminate\Support\Facades\Auth;

class Venta extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar', 'anular', 'venta', 'saldo'];

    public $modal = 'hidden';
    public $id;
    public $venta_id;
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $fecha;
    #[Validate('required', message: 'La Fecha pago es requerida.')]
    public $fecha_pago;
    //#[Validate('required', message: 'La Sucursal es requerida.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'El tipo de documento es requerido.')]
    #[Validate('integer', message: 'El tipo de documento debe ser entero.')]
    public $documento_id;
    #[Validate('required', message: 'El cliente es requerido.')]
    #[Validate('integer', message: 'El cliente debe ser entero.')]
    public $cliente_id;
    #[Validate('required', message: 'El tipo de pago es requerido.')]
    #[Validate('integer', message: 'El tipo de pago debe ser entero.')]
    public $pago_id;
    #[Validate('required', message: 'El numero de documento es requerido.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    #[Validate('unique:ventas,nro_doc', message: 'El numero de documento ya existe.')]
    public $numero_documento;
    //#[Validate('required', message: 'El numero de cheque documento es requerido.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $numero_cheque;
    #[Validate('required', message: 'El numero de cheque del banco es requerido.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $numero_banco_cheque;
    //#[Validate('required', message: 'La venta, si o no esta paga es requerida.')]
    //#[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $pagada;
    #[Validate('integer', message: 'El Producto debe ser entera.')]
    #[Validate('required', message: 'El Producto es requerido.')]
    public $producto_id;
    #[Validate('required', message: 'La Cantidad es requerida.')]
    #[Validate('numeric', message: 'La Cantidad debe ser numerica.')]
    #[Validate('min:1', message: 'La Cantidad debe ser mayor a cero.')]
    public $cantidad = 0;
    #[Validate('required', message: 'El Precio Unitario es requerida.')]
    #[Validate('numeric', message: 'El Precio Unitario debe ser numerico.')]
    public $precio_unitario_venta = 0;
    #[Validate('required', message: 'El Total es requerido.')]
    #[Validate('numeric', message: 'El Total debe ser numerico.')]
    public $total_venta = 0;
    #[Validate('required', message: 'El stock inicial es requerido.')]
    public $stock = 1;
    public $monto = 0;
    public $tipo;
    public $vista = null;
    public $cliente_idF;
    

    public function mount()
    { 
        if (Auth::user()->type == 'sucursal') {
            $this->sucursal_id  = Auth::user()->sucursal_id;
            $this->fecha        = date('Y-m-d');
        } else {
            $this->sucursal_id = 1;
        }    
    }
    public function render()
    {
        if ($this->cliente_idF > 0) {
            if (Auth::user()->type == 'admin') {
                $ventas = Sale::where('nota', null)
                            ->where('cliente_id', $this->cliente_idF)
                            ->orderByDesc('fecha')
                            ->paginate(10);
            } else {
                $ventas = Sale::where('sucursal_id', Auth::user()->sucursal_id)
                        ->where('nota', null)
                        ->where('cliente_id', $this->cliente_idF)
                        ->orderByDesc('fecha')
                        ->paginate(10);
            }
        } else {
            if (Auth::user()->type == 'admin') {
                $ventas = Sale::where('nota', null)
                        ->orderByDesc('fecha')
                        ->paginate(10);
            } else {
                $ventas = Sale::where('sucursal_id', Auth::user()->sucursal_id)
                        ->where('nota', null)
                        ->orderByDesc('fecha')
                        ->paginate(10);
            }
        }
        $this->resetPage();

        $sucursales     = Sucursal::whereNull('deleted_at')->get();
        $productos      = Producto::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        $clientes       = Cliente::whereNull('deleted_at')->orderBy('razon_social', 'ASC')->get();
        $documentos     = Documento::whereNull('deleted_at')->get();
        $pagos          = Pago::whereNull('deleted_at')->get();
        
        return view('livewire.venta', compact(
            'ventas',
            'sucursales',
            'productos',
            'clientes',
            'documentos',
            'pagos'
        ));
    }

    public function agregar($value) {
        if ($value>0) {
            $producto = Producto::find($value);
            if ($producto){
                $this->producto_id = $value;
                $this->precio_unitario_venta = $producto::precio($value, $this->fecha) ?? 0;
            }
        }
    }

    public function total($value) {
        if ($value>0) {
            $this->total_venta = round($this->precio_unitario_venta*$value, 2);
        }
    }

    public function guardar() {
        $this->validate([
            'documento_id'              => 'required|integer',
            'pago_id'                   => 'required|integer',
            'cliente_id'                => 'required|integer',
            'numero_documento'          => isset($this->venta_id) ? 'required|unique:ventas,nro_doc,'.$this->venta_id : 'required|unique:ventas,nro_doc',
            'fecha'                     => 'required',
            'fecha_pago'                => $this->venta_id ? 'required' : 'nullable',

        ]);

        if ($this->venta_id) {
            $venta  = Sale::find($this->venta_id);
            $venta->fecha = $this->fecha;
            $venta->documento_id = $this->documento_id;
            $venta->cliente_id = $this->cliente_id;
            $venta->pago_id = $this->pago_id;
            $venta->sucursal_id = $this->sucursal_id;
            $venta->nro_doc = $this->numero_documento;
            $venta->pagada = $this->pagada == 'si' ? 1 : 0;
            $venta->fecha_pago = $this->fecha_pago;
            
            if ($venta->save()) {
                $this->venta_id = null;
                $this->dispatch('alerta', [
                    'title'             => 'Venta actualizada exitosamente',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          =>'top-right'
                ]);
                $this->modalAction('close');
                $this->resetPage();
            }
        } else {
            $cliente = Cliente::find($this->cliente_id);
            $credito = $cliente->credito->monto ?? 0;
            $tipoPago = Pago::find($this->pago_id);
    
            if (($credito>0 && $tipoPago->nombre == 'Credito') or ($credito == 0 or $tipoPago->nombre != 'Credito')) {
                $venta = Sale::where('documento_id', $this->documento_id)
                        ->where('nro_doc', $this->numero_documento)
                        ->whereNull('deleted_at')
                        ->first();
                if ($venta) {
                    $this->dispatch('alerta', [
                        'title'             => 'Venta duplicada',
                        'timer'             => 3000,
                        'icon'              => 'error',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          =>'top-right'
                    ]);
                } else {
                    $sale = Sale::create([
                        'fecha'             => $this->fecha,
                        'cliente_id'        => $this->cliente_id,
                        //'monto'             => $this->monto,
                        'estado'            => 0,
                        'pagada'            => $tipoPago->nombre == 'Credito' ? 0 : 1,
                        'fecha_pago'        => $tipoPago->nombre == 'Credito' ? null : $this->fecha,
                        'pago_id'           => $this->pago_id,
                        'documento_id'      => $this->documento_id,
                        'nro_doc'           => $this->numero_documento,
                        'sucursal_id'       => $this->sucursal_id,
                        'created_at'        => now(),
                        'updated_at'        => now()
                    ]);
                    if ($sale) {
                        $this->venta_id = $sale->id;
                        $this->dispatch('venta-guardada', [
                            'title'             => 'Venta registrada exitosamente, redireccionando...',
                            'timer'             => 3000,
                            'icon'              => 'success',
                            'toast'             => true,
                            'showConfirmButton' => false,
                            'position'          =>'top-right'
                        ]);
                        $this->modalAction('close');
                        $this->resetPage();
                    }
                }
        
            } else {
                $this->dispatch('alerta', [
                    'title'             => 'El cliente '.$cliente->razon_social.' no posee creditos, elija un tipo de pago diferente',
                    'timer'             => 3000,
                    'icon'              => 'error',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          =>'top-right'
                ]);
            }        
        }

    }

    public function editar($id) {
        $venta  = Sale::find($id);
        $this->venta_id = $id;
        $this->fecha = $venta->fecha;
        $this->documento_id = $venta->documento_id;
        $this->cliente_id = $venta->cliente_id;
        $this->pago_id = $venta->pago_id;
        $this->sucursal_id = $venta->sucursal_id;
        $this->numero_documento = $venta->nro_doc;
        $this->pagada = $venta->pagada ? 'si' : 'no';
        $this->fecha_pago = $venta->fecha_pago;
        $this->vista = 'editar';
        $this->modalAction('open');   
    }
    
    public function alerta($id, $tipo = null) {
        $this->id = $id;
        $venta  = Sale::find($id);
        $titulo = '¿Desea Eliminar la venta?';
        $boton  = 'Eliminar';
        $evento = 'eliminar-venta';
        $value = 0;
        $input = null;
        $this->tipo = $tipo;

        if ($tipo == 'anular') {
            $titulo = $venta->estado ? '¿Desea reactivar la venta?' : '¿Desea anular la venta?';
            $evento = 'anular-venta';
            $boton  = $venta->estado ? 'Aceptar' : 'Anular';
        }

        if ($tipo == 'descuento') {
            $titulo     = '¿Desea agregar descuento?';
            $evento     = 'descuento-venta';
            $input      = 'text';
            $boton      = 'Aceptar';
            $value      =  $venta ? $venta->descuento ?? 0 : 0;
        }
        
        $this->dispatch($evento,[
            'title'                 => 'Venta #'.str_pad($venta->id, 6, "0", STR_PAD_LEFT),
            'input'                 => $input,
            'text'                  => $titulo,
            'inputValue'            => $value,
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => $boton,
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function saldo () 
    {
        if ($this->monto == 0 or $this->monto == null or !is_numeric($this->monto)) {
            $this->dispatch('alerta', [
                'title'             => 'Monto incorrecto.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            if (isset($this->tipo) && $this->tipo == 'descuento') {
                $sale = Sale::find($this->id);
                $monto = $sale->cliente->credito->monto ?? 0;
                if ($monto>0) {
                    $descuento        = $sale->cliente->credito->monto-($sale->cliente->credito->monto - $this->monto);
                    $credito          = $sale->descuento == $this->monto ? $sale->cliente->credito->monto : ($sale->cliente->credito->monto-$sale->descuento)+$this->monto;
                    $sale->cliente->credito->update(['monto' => $credito]);
                }
                $sale->descuento  = $descuento ?? $this->monto;
                
                if ($sale->save()) {
                    
                    $this->id = null;
                    $this->monto = 0;
                    $this->dispatch('alerta', [
                        'title'             => 'Descuento actualizado exitosamente.',
                        'timer'             => 3000,
                        'icon'              => 'success',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          => 'top-right'
                    ]);
                } 
            }
        }
    }

    public function eliminar() {
        $venta = Sale::find($this->id);
        if ($venta->delete()) {
            $this->id = null;
            $this->dispatch('alerta', [
                'title'             => 'Venta eliminada exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }
    
    public function anular() {
        $venta = Sale::find($this->id);
        $venta->estado = $venta->estado ? 0 : 1;
        if ($venta->save()) {
            $this->id = null;
            $this->dispatch('alerta', [
                'title'             => $venta->estado ? 'Venta anulada exitosamente.' :  'Venta reativada exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            if (Auth::user()->type == 'admin') {
                $this->reset([
                    'sucursal_id',
                    'documento_id',
                    'pago_id',
                    'cliente_id',
                    'numero_documento',
                    'numero_cheque',
                    'numero_banco_cheque',
                    'fecha',
                    'fecha_pago',
                    'pagada',
                    'stock'                     
                ]);
            } else {
                $this->reset([
                    'documento_id',
                    'pago_id',
                    'cliente_id',
                    'numero_documento',
                    'numero_cheque',
                    'numero_banco_cheque',
                    'fecha',
                    'fecha_pago',
                    'pagada',
                    'stock'                     
                ]);
            }
        }
        $this->resetValidation();
    }

    public function venta() {
        return redirect()->route('ventas.detalles', ['id' => $this->venta_id]);

    }
}
