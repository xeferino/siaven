<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Venta as Sale;
use App\Models\DetalleVenta as DetalleSale;
use App\Models\Sucursal;
use App\Models\Producto;
use App\Models\Cliente;
use App\Models\Pago;
use App\Models\PrecioProducto;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;
use Illuminate\Support\Facades\Auth;

class DetalleVenta extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar', 'anular'];

    public $modal = 'hidden';
    public $id;
    public $detalle_venta_id;
    #[Validate('required', message: 'La Sucursal es requerida.')]
    #[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'La Venta es requerido.')]
    #[Validate('integer', message: 'La Venta debe ser entero.')]
    public $venta_id;
    #[Validate('integer', message: 'El Producto debe ser entera.')]
    #[Validate('required', message: 'El Producto es requerido.')]
    public $producto_id;
    #[Validate('required', message: 'La Cantidad es requerida.')]
    #[Validate('numeric', message: 'La Cantidad debe ser numerica.')]
    #[Validate('min:0', message: 'La Cantidad debe ser mayor a cero.')]
    public $cantidad = 0;
    #[Validate('required', message: 'El Precio Unitario de venta es requerida.')]
    #[Validate('numeric', message: 'El Precio Unitario de venta debe ser numerico.')]
    #[Validate('min:0', message: 'El Precio Unitario de venta debe ser mayor a cero.')]
    public $precio_unitario_venta = 0;
    #[Validate('required', message: 'El Precio Unitario de guia es requerida.')]
    #[Validate('numeric', message: 'El Precio Unitario de guia debe ser numerico.')]
    #[Validate('min:1', message: 'El Precio Unitario de guia debe ser mayor a cero.')]
    public $precio_unitario_guia = 0;
    #[Validate('required', message: 'El Total de producto es requerido.')]
    #[Validate('numeric', message: 'El Total de producto debe ser numerico.')]
    #[Validate('min:0', message: 'El Total de producto debe ser mayor a cero.')]
    public $total_venta = 0;
    //#[Validate('required', message: 'El Total de guia es requerido.')]
    //#[Validate('integer', message: 'El Total de guia debe ser numerico.')]
    //#[Validate('min:1', message: 'El Total de guia debe ser mayor a cero.')]
    public $total_guia = 0;

    public function render()
    {
        $venta = Sale::find($this->id);
        if ($venta) {
            $this->sucursal_id = $venta->sucursal_id;
            $this->venta_id = $venta->id;
            $ventas = DetalleSale::where('venta_id', $venta->id)->whereNull('deleted_at')->paginate(10);
        }

        $sucursales = Sucursal::whereNull('deleted_at')->get();
        $productos  = Producto::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        return view('livewire.detalle-venta', compact('ventas', 'sucursales', 'productos', 'venta'));
    }


    public function agregar($value) {
        if ($value>0) {
            $producto = Producto::find($value);
            if ($producto){
                $venta = Sale::find($this->id);
                $this->producto_id = $value;
                $this->precio_unitario_venta = $producto::precio($value, $venta->fecha) ?? 0;
            }
        }
    }

    public function total($value) {
        if ($value>0) {
            $this->total_venta = round($this->precio_unitario_venta*$value, 2);
        }
    }

    public function guia($value) {
        if ($value>0) {
            $this->total_guia = $value*$this->cantidad;
        }
    }

    public function guardar() {

        $this->validate([
            'venta_id'                 => 'required|integer',
            'producto_id'              => 'required|integer',
            'cantidad'                 => 'required|numeric|min:0',
            'precio_unitario_venta'    => 'required|numeric|min:0',
            'total_venta'              => 'required|numeric|min:0',
        ]);

        $venta = Sale::find($this->id);
        
        $cliente = Cliente::find($venta->cliente_id);
        $credito = $cliente->credito->monto ?? 0;
        $tipoPago = Pago::find($venta->pago_id);

        if (($credito>0 && $tipoPago->nombre == 'Credito') or $tipoPago->nombre != 'Credito') {
            if ($credito>=($this->cantidad*$this->precio_unitario_venta)) {
                $venta = DetalleSale::create([
                    'venta_id'          => $this->venta_id,
                    'producto_id'       => $this->producto_id,
                    'cantidad'          => $this->cantidad,
                    'precio_unitario'   => $this->precio_unitario_venta,
                    'estado'            => 0,
                    'created_at'        => now(),
                    'updated_at'        => now()
                ]);
        
                if ($venta) {
                    $detalle = DetalleSale::selectRaw('detalle_ventas.precio_unitario, ventas.fecha')
                                ->leftJoin('ventas', 'detalle_ventas.venta_id', '=', 'ventas.id')
                                ->orderByDesc('ventas.fecha')
                                ->whereNull('ventas.deleted_at')
                                ->whereNull('detalle_ventas.deleted_at')
                                ->limit(1)
                                ->get();
                    
                    Producto::find($this->producto_id)->update(['precio_venta' => $detalle[0]->precio_unitario]);
                    $cliente->credito->update(['monto' =>  $credito-($this->cantidad*$this->precio_unitario_venta)]);
                    $this->dispatch('alerta', [
                        'title'             => 'Prodcuto registrado exitosamente.',
                        'timer'             => 3000,
                        'icon'              => 'success',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          =>'top-right'
                    ]);
                    $this->modalAction('close');
                    $this->resetPage(); 
                }
            } else {
                $this->dispatch('alerta', [
                    'title'             => 'El valor '.moneda($this->cantidad*$this->precio_unitario_venta).' de la venta supera los creditos del cliente '.moneda($credito).'',
                    'timer'             => 3000,
                    'icon'              => 'error',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          =>'top-right'
                ]);
            }
        } else {
            $this->dispatch('alerta', [
                'title'             => 'El cliente '.$cliente->razon_social.' no posee creditos, para agregar productos',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          =>'top-right'
            ]);
        }

    }

    public function alerta($id, $tipo = null) {
        $this->detalle_venta_id = $id;
        $venta = DetalleSale::find($id);
        $titulo = '¿Desea Eliminar el producto?';
        $boton = 'Eliminar';
        $evento = 'eliminar-detalle-venta';
        if ($tipo == 'anular') {
            $titulo = $venta->estado ? '¿Desea reactivar el producto?' : '¿Desea anular el producto?';
            $evento = 'anular-detalle-venta';
            $boton  = $venta->estado ? 'Aceptar' : 'Anular';
        }
        $this->dispatch($evento, [
            'title'                 => $venta->producto->nombre,
            'text'                  => $titulo,
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => $boton,
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $venta = DetalleSale::find($this->detalle_venta_id);
        if ($venta->delete()) {
            $this->detalle_venta_id = null;
            $this->dispatch('alerta', [
                'title'             => 'Producto eliminado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }

    public function anular() {
        $venta = DetalleSale::find($this->detalle_venta_id);
        $venta->estado = $venta->estado ? 0 : 1;
        if ($venta->save()) {
            $this->detalle_venta_id = null;
            $this->dispatch('alerta', [
                'title'             => $venta->estado ? 'Producto anulado exitosamente.' :  'Producto reativado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }
    
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->detalle_venta_id = null;
            $this->reset([
                'venta_id', 
                'producto_id', 
                'precio_unitario_venta',
                'total_venta',
                'cantidad',
                'precio_unitario_guia',
                'total_guia',
                'cantidad'
            ]);
        }
        $this->resetValidation();
    }
}
