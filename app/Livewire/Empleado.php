<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Empleado as Persona;
use App\Models\Sucursal;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;
use Illuminate\Support\Facades\Auth;

class Empleado extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar'];

    public $modal = 'hidden';
    public $id;
    #[Validate('required', message: 'El RUT es requerido.')]
    #[Validate('unique:clientes,rut', message: 'El RUT es ya esta en uso.')]
    public $rut;
    #[Validate('required', message: 'La Razon social es requerida.')]
    public $razon_social;
    #[Validate('required', message: 'La Direccion es requerida.')]
    #[Validate('min:3', message: 'La Direccion debe contener 3 letras como minimo.')]
    public $direccion;
    #[Validate('required', message: 'La Ciudad es requerida.')]
    public $ciudad;
    #[Validate('required', message: 'El Giro es requerida.')]
    public $giro;
    #[Validate('required', message: 'El Contacto es requerida.')]
    public $contacto;
    //#[Validate('required', message: 'El Email es requerida.')]
    #[Validate('email', message: 'El Email es invalido.')]
    public $email;
    #[Validate('required', message: 'El Telefono 1 es requerido.')]
    #[Validate('min:9', message: 'El Telefono 1 debe contener 9 caracteres como minimo.')]
    #[Validate('integer', message: 'El Telefono 1 debe ser numerico.')]
    public $telefono1;
    //#[Validate('required', message: 'El Telefono 2 es requerido.')]
    #[Validate('min:9', message: 'El Telefono 2 debe contener 9 caracteres como minimo.')]
    #[Validate('integer', message: 'El Telefono 2 debe ser numerico.')]
    public $telefono2;
    #[Validate('required', message: 'La Sucursal es requerida.')]
    public $sucursal_id;
    
    public function render()
    {
        $empleados = Auth::user()->type == 'sucursal' 
                    ? Persona::where('sucursal_id', Auth::user()->sucursal_id)->paginate(10) 
                    : Persona::paginate(10);
        $sucursales = Auth::user()->type == 'sucursal' 
                    ? Sucursal::where('id', Auth::user()->sucursal_id)->get() 
                    : Sucursal::all();
        return view('livewire.empleado', compact('empleados','sucursales'));
    }

    public function guardar() {

        $this->validate([
            'rut'           => isset($this->id) ? 'required|unique:empleados,rut,'.$this->id : 'required|unique:empleados,rut',
            'razon_social'  => 'required',
            'ciudad'        => 'required',
            'giro'          => 'required',
            'contacto'      => 'required',
            'email'         => 'nullable|email',
            'sucursal_id'   => 'required',
            'direccion'     => 'required|string|min:3',
            'telefono1'     => 'required|integer|min:9',
            'telefono2'     => 'nullable|integer|min:9',
        ]);

        $empleado = ($this->id) ? Persona::updateOrCreate(
            [
                'id' => $this->id,
            ],
            [
                'rut'           => $this->rut,
                'razon_social'  => $this->razon_social,
                'ciudad'        => $this->ciudad,
                'giro'          => $this->giro,
                'contacto'      => $this->contacto,
                'email'         => $this->email,
                'sucursal_id'   => $this->sucursal_id,
                'direccion'     => $this->direccion,
                'telefono1'     => $this->telefono1,
                'telefono2'     => $this->telefono2,
                'created_at'    => now(),
                'updated_at'    => now()
            ])
        : Persona::create(
            [
                'rut'           => $this->rut,
                'razon_social'  => $this->razon_social,
                'ciudad'        => $this->ciudad,
                'giro'          => $this->giro,
                'contacto'      => $this->contacto,
                'email'         => $this->email,
                'sucursal_id'   => $this->sucursal_id,
                'direccion'     => $this->direccion,
                'telefono1'     => $this->telefono1,
                'telefono2'     => $this->telefono2,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

        if ($empleado) {
            $this->dispatch('alerta', [
                'title'             => ($this->id) ? 'Empleado actualizado exitosamente.' : 'Empleado registrado exitosamente.',
                'timer'             =>3000,
                'icon'              =>'success',
                'toast'             =>true,
                'showConfirmButton' => false,
                'position'          =>'top-right'
            ]);

            $this->modalAction('close');
            $this->reset([
                'rut',          
                'razon_social',
                'ciudad',
                'giro',
                'contacto',
                'email',
                'sucursal_id',
                'direccion',
                'telefono1',
                'telefono2'
            ]);
            $this->resetPage();
        }
    }

    public function editar($idEmpleado) {
        $this->id = $idEmpleado;
        $empleado = Persona::find($this->id);
        $this->rut           = $empleado->rut;
        $this->razon_social  = $empleado->razon_social;
        $this->ciudad        = $empleado->ciudad;
        $this->giro          = $empleado->giro;
        $this->contacto      = $empleado->contacto;
        $this->email         = $empleado->email;
        $this->sucursal_id   = $empleado->sucursal_id;
        $this->direccion     = $empleado->direccion;
        $this->telefono1     = $empleado->telefono1;
        $this->telefono2     = $empleado->telefono2;
        $this->modalAction('open');
    }

    public function alerta($id) {
        $this->id = $id;
        $empleado = Persona::find($id);
        $this->dispatch('eliminar-empleado',[
            'title'                 => $empleado->razon_social,
            'text'                  => '¿Desea Eliminar el empleado?',
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Eliminar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $empleado = Persona::find($this->id);
        if ($empleado->delete()) {
            $this->id = null;
            $this->dispatch('alerta', [
                'title'             => 'Empleado eliminado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->reset([
                'rut',          
                'razon_social',
                'ciudad',
                'giro',
                'contacto',
                'email',
                'sucursal_id',
                'direccion',
                'telefono1',
                'telefono2'
            ]);
        }
        $this->resetValidation();
    }
}
