<?php

namespace App\Livewire;

use App\Models\DetalleTraspaso;
use App\Models\Documento as TipoDocumento;
use App\Models\Pago as TipoPago;
use App\Models\StockSucursal;
use Livewire\Component;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\Sucursal;
use App\Models\Traspaso;
use App\Models\GastoSucursal;
use App\Models\Gasto as TipoGasto;
use App\Models\Ingreso as TipoIngreso;
use App\Models\Perdida as TipoPerdida;
use App\Models\IngresoSucursal;
use App\Models\PerdidaProducto;
use App\Models\PrecioProducto;
use App\Models\SueldoSucursal;
use App\Models\Empleado;
use App\Models\Venta;
use App\Models\DetalleVenta;
use App\Models\CierrePlanillaDiaria as Planilla;
use Illuminate\Support\Facades\Auth;
use Livewire\Attributes\Validate;

use function Lambdish\Phunctional\group_by;

class PlanillaDiaria extends Component
{
    protected $listeners = ['saldo', 'planilla', 'eliminar'];

    public $modal = 'hidden';
    #[Validate('required', message: 'La Sucursal es requerida.')]
    #[Validate('integer', message: 'La Sucursal debe ser entera.')]
    public $sucursal_id;
    #[Validate('required', message: 'La Fecha es requerida.')]
    public $fecha;
    public $producto_id;
    public $gasto_id;
    #[Validate('required', message: 'El tipo de gasto es requerido.')]
    #[Validate('integer', message: 'El tipo de gasto debe ser entero.')]
    public $gasto_tipo_id;
    #[Validate('required', message: 'El tipo de ingreso es requerido.')]
    #[Validate('integer', message: 'El tipo de ingreso debe ser entero.')]
    public $ingreso_tipo_id;
    #[Validate('required', message: 'El tipo de perdida es requerido.')]
    #[Validate('integer', message: 'El tipo de perdida debe ser entero.')]
    public $perdida_id;
    public $sueldo_id;
    public $ingreso_id;
    #[Validate('required', message: 'El empleado es requerido.')]
    #[Validate('integer', message: 'El empleado debe ser entero.')]
    public $empleado_id;
    #[Validate('required', message: 'El producto es requerido.')]
    #[Validate('integer', message: 'El producto debe ser entero.')]
    public $producto_perdida_id;
    public $titulo;
    public $tipo;
    #[Validate('required', message: 'El monto es requerido.')]
    #[Validate('numeric', message: 'El monto debe ser numerico.')]
    #[Validate('min:0', message: 'El monto debe ser mayor a cero.')]
    public $monto = 0;
    public $sucursal = 0;
    #[Validate('required', message: 'La cantidad es requerida.')]
    #[Validate('numeric', message: 'La cantidad debe ser numerica.')]
    #[Validate('min:0', message: 'La cantidad debe ser mayor a cero.')]
    public $cantidad = 0;
    public $resultados =  [];
    public $prodIds =  [];
    public $tipoGastos;
    public $tipoIngresos;
    public $tipoPagos;
    public $tipoPerdidas;
    public $tipoDocumentos;
    public $gastos;
    public $ventas;
    public $documentos;
    public $perdidas;
    public $ingresos;
    public $sueldos;
    public $productos;
    public $empleados;
    public $tramite;
    public $estado;
    public $diferenciaCaja;
    public $productosJson;
    
    
    public function mount()
    {
        $this->tipoGasto();
        $this->tipoIngreso();
        $this->tipoPago();
        $this->tipoPerdida();
        $this->tipoDocumento();
        $this->gastoSucursal();
        $this->ventaSucursal();
        $this->sueldoSucursal();
        $this->perdidaSucursal();
        $this->ingresoSucursal();
        $this->empleadoSucursal();
        
        if (Auth::user()->type == 'sucursal') {
            $this->fecha = date('Y-m-d');
            $this->sucursal_id = Auth::user()->sucursal_id;
            $this->buscar();
        }        
    }

    public function render()
    {
        $sucursales     = Sucursal::whereNull('deleted_at')->get();
        $productos      = $this->productos();
        $categorias     = Categoria::whereNull('deleted_at')->get();
        $gastos         = $this->gastoSucursal();
        $ventas         = $this->ventaSucursal();
        $sueldos        = $this->sueldoSucursal();
        $perdidas       = $this->perdidaSucursal();
        $ingresos       = $this->ingresoSucursal();
        $empleados      = $this->empleadoSucursal();

        return view('livewire.planilla-diaria', compact(
            'sucursales',
            'gastos',
            'ventas',
            'sueldos',
            'perdidas',
            'ingresos',
            'productos',    
            'empleados',
            'categorias'
        ));
    }

    public function buscar()
    {
        $this->validate([
            'sucursal_id' => 'required|integer',
            'fecha'       => 'required'
        ]);

        $productos = Producto::whereNull('deleted_at')->orderBy('nombre', 'ASC')->get();
        $this->resultados = [];
        $this->prodIds = []; 
        foreach ($productos as $key => $item) {
            $producto           = Producto::find($item->id);
            $nombre             = $producto->nombre;
            $saldo_anterior     = round(Sucursal::stockAyer($producto->id, $this->sucursal_id, $this->fecha)[0]->saldo ?? 0, 2);
            $recibido           = round(DetalleTraspaso::cantidad($producto->id, $this->sucursal_id, $this->fecha)[0]->cantidad ?? 0, 2);
            $saldo_teorico      = round($saldo_anterior+$recibido, 2);
            $saldo_registrado   = round(Sucursal::stockHoy($producto->id, $this->sucursal_id, $this->fecha)[0]->saldo ?? 0, 2);
            $venta_calculda     = round($saldo_teorico-$saldo_registrado, 2);
            $valor_unitario     = round(Producto::precio($producto->id, $this->fecha), 2);
            $total              = round($venta_calculda*$valor_unitario, 2);
            
            if ($total != 0) {
                array_push($this->resultados, [
                    'producto'          => $producto,
                    'nombre'            => $nombre,
                    'saldo_anterior'    => $saldo_anterior,
                    'recibido'          => $recibido,
                    'saldo_teorico'     => $saldo_teorico,
                    'saldo_registrado'  => $saldo_registrado,
                    'venta_calculda'    => $venta_calculda,
                    'valor_unitario'    => $valor_unitario,
                    'total'             => $total
                ]);
            }

            array_push($this->prodIds, $item->id);
        }
        if (count($this->resultados)>0) {
            $this->dispatch('alerta', [
                'title'             => 'Planilla cargada exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]) ;
        }
        
        $this->sucursal = Sucursal::find($this->sucursal_id);
        $this->planillaEstadoSucursal();
        $this->productos();
    }

    public function saldo ()
    {
        if (!is_numeric($this->monto)) {
            $this->dispatch('alerta', [
                'title'             => 'Monto incorrecto',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            if (isset($this->tipo) && $this->tipo == 'gasto') {
                $gasto = GastoSucursal::where('id', $this->gasto_id)
                        ->where('sucursal_id', $this->sucursal_id)
                        ->where('fecha', $this->fecha)
                        ->whereNull('deleted_at')
                        ->update(['monto' => $this->monto]);
                if ($gasto) {
                    $this->gasto_id = null;
                    $this->monto = 0;
                    $this->gastoSucursal();
                    $this->dispatch('alerta', [
                        'title'             => 'Gasto actualizado exitosamente.',
                        'timer'             => 3000,
                        'icon'              => 'success',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          => 'top-right'
                    ]);
                } else {
                    $this->dispatch('alerta', [
                        'title'             => 'Ha ocurrido un error, intente nuevamente.',
                        'timer'             => 3000,
                        'icon'              => 'error',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          => 'top-right'
                    ]);
                }
            } elseif (isset($this->tipo) && $this->tipo == 'sueldo') {
                $sueldo = SueldoSucursal::where('id', $this->sueldo_id)
                        ->where('sucursal_id', $this->sucursal_id)
                        ->where('fecha', $this->fecha)
                        ->whereNull('deleted_at')
                        ->update(['monto' => $this->monto]);
    
                if ($sueldo) {
                    $this->sueldo_id = null;
                    $this->monto = 0;
                    $this->sueldoSucursal();
                    $this->dispatch('alerta', [
                        'title'             => 'Sueldo actualizado exitosamente.',
                        'timer'             => 3000,
                        'icon'              => 'success',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          => 'top-right'
                    ]);
                } else {
                    $this->dispatch('alerta', [
                        'title'             => 'Ha ocurrido un error, intente nuevamente.',
                        'timer'             => 3000,
                        'icon'              => 'error',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          => 'top-right'
                    ]);
                }
            } elseif (isset($this->tipo) && $this->tipo == 'ingreso') {
                $ingreso = IngresoSucursal::where('id', $this->ingreso_id)
                    ->where('sucursal_id', $this->sucursal_id)
                    ->where('fecha', $this->fecha)
                    ->whereNull('deleted_at')
                    ->update(['monto' => $this->monto]);
    
                if ($ingreso) {
                    $this->ingreso_id = null;
                    $this->monto = 0;
                    $this->ingresoSucursal();
                    $this->dispatch('alerta', [
                        'title'             => 'Ingreso actualizado exitosamente.',
                        'timer'             => 3000,
                        'icon'              => 'success',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          => 'top-right'
                    ]);
                } else {
                    $this->dispatch('alerta', [
                        'title'             => 'Ha ocurrido un error, intente nuevamente.',
                        'timer'             => 3000,
                        'icon'              => 'error',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          => 'top-right'
                    ]);
                }
            } elseif (isset($this->tipo) && $this->tipo == 'perdida') {
                $perdida = PerdidaProducto::where('id', $this->perdida_id)
                    ->where('sucursal_id', $this->sucursal_id)
                    ->where('fecha', $this->fecha)
                    ->whereNull('deleted_at')
                    ->update(['cantidad' => $this->monto]);
                if ($perdida) {
                    $this->perdida_id = null;
                    $this->monto = 0;
                    $this->perdidaSucursal();
                    $this->dispatch('alerta', [
                        'title'             => 'Perdida actualizada exitosamente.',
                        'timer'             => 3000,
                        'icon'              => 'success',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          => 'top-right'
                    ]);
                } else {
                    $this->dispatch('alerta', [
                        'title'             => 'Ha ocurrido un error, intente nuevamente.',
                        'timer'             => 3000,
                        'icon'              => 'error',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          => 'top-right'
                    ]);
                }
            } else {
                $stock = StockSucursal::where('producto_id', $this->producto_id)
                        ->where('sucursal_id',$this->sucursal_id)
                        ->where('fecha', $this->fecha)
                        ->whereNull('deleted_at')
                        ->first();
        
                $stock = ($stock) ? StockSucursal::updateOrCreate(
                    [
                        'producto_id'   => $this->producto_id,
                        'sucursal_id'   => $this->sucursal_id,
                        'fecha'         => $this->fecha,
                    ],
                    [
                        'fecha'         => $this->fecha,
                        'sucursal_id'   => $this->sucursal_id,
                        'producto_id'   => $this->producto_id,
                        'saldo'         => $this->monto,
                        'created_at'    => now(),
                        'updated_at'    => now()
                    ])
                : StockSucursal::create([
                    'fecha'         => $this->fecha,
                    'sucursal_id'   => $this->sucursal_id,
                    'producto_id'   => $this->producto_id,
                    'saldo'         => $this->monto,
                    'created_at'    => now(),
                    'updated_at'    => now()
                ]);
        
                if ($stock) {
                    $this->producto_id = null;
                    $this->monto = 0;
                    $this->buscar();
                    $this->dispatch('alerta', [
                        'title'             => 'Saldo registrado exitosamente.',
                        'timer'             => 3000,
                        'icon'              => 'success',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          => 'top-right'
                    ]);
                } else {
                    $this->dispatch('alerta', [
                        'title'             => 'Ha ocurrido un error, intente nuevamente.',
                        'timer'             => 3000,
                        'icon'              => 'error',
                        'toast'             => true,
                        'showConfirmButton' => false,
                        'position'          => 'top-right'
                    ]);
                }
            }
        }
    }

    public function alerta($id, $tipo = null) 
    {
        $titulo = null;
        $value = 0;
        $input = 'text';
        $text = '¿Desea actualizar el monto?';
        $evento = 'saldo-planilla';
        $this->tipo = $tipo;
        if ($tipo == 'gasto') {
            $this->gasto_id = $id;
            $gasto = GastoSucursal::where('id', $this->gasto_id)
                    ->where('sucursal_id', $this->sucursal_id)
                    ->where('fecha', $this->fecha)
                    ->whereNull('deleted_at')
                    ->first();
            $value =  $gasto ? $gasto->monto : 0;
        } elseif ($tipo == 'sueldo') {
            $this->sueldo_id = $id;
            $sueldo = SueldoSucursal::where('id', $this->sueldo_id)
                ->where('sucursal_id', $this->sucursal_id)
                ->where('fecha', $this->fecha)
                ->whereNull('deleted_at')
                ->first();
            $value =  $sueldo ? $sueldo->monto : 0;
        } elseif ($tipo == 'ingreso') {
            $this->ingreso_id = $id;
            $ingreso = IngresoSucursal::where('id', $this->ingreso_id)
                ->where('sucursal_id', $this->sucursal_id)
                ->where('fecha', $this->fecha)
                ->whereNull('deleted_at')
                ->first();
            $value =  $ingreso ? $ingreso->monto : 0;
        } elseif ($tipo == 'perdida') {
            $this->perdida_id = $id;
            $perdida = PerdidaProducto::where('id', $this->perdida_id)
                ->where('sucursal_id', $this->sucursal_id)
                ->where('fecha', $this->fecha)
                ->whereNull('deleted_at')
                ->first();
            $value =  $perdida ? $perdida->cantidad : 0;
            $text = '¿Desea actualizar la cantidad?';

        } elseif ($tipo == 'rendida' or $tipo == 'cerrada' or $tipo == 'abierta') {
            $this->tramite = $tipo;
            $sucursal   = Sucursal::find($id);
            $this->sucursal_id = $id;
            $input      = null;
            $titulo     = 'Sucursal '.$sucursal->nombre;
            $text       = 'Realmente quieres '.$tipo.' la planilla de hoy '. $this->fecha;
            $evento     = 'planilla-sucursal';
        } elseif ($tipo == 'ingreso-eliminar') {
            $this->ingreso_id = $id;
            $evento     = $tipo;
            $input      = null;
            $text       = '¿Desea eliminar el ingreso?';
            $titulo     = null;
        } elseif ($tipo == 'perdida-eliminar') {
            $this->perdida_id = $id;
            $evento     = $tipo;
            $input      = null;
            $text       = '¿Desea eliminar la perdida?';
            $titulo     = null;
        } elseif ($tipo == 'gasto-eliminar') {
            $this->gasto_id = $id;
            $evento     = $tipo;
            $input      = null;
            $text       = '¿Desea eliminar el gasto?';
            $titulo     = null;
        } elseif ($tipo == 'sueldo-eliminar') {
            $this->sueldo_id = $id;
            $evento     = $tipo;
            $input      = null;
            $text       = '¿Desea eliminar el sueldo?';
            $titulo     = null;
        } else {
            $titulo = 'Saldo';
            $this->producto_id = $id;
            $stock = StockSucursal::where('producto_id', $this->producto_id)
                    ->where('sucursal_id',$this->sucursal_id)
                    ->where('fecha', $this->fecha)
                    ->whereNull('deleted_at')
                    ->first();
            $value =  $stock ? $stock->saldo : 0;
        }

        $this->dispatch($evento,[
            'title'                 => $titulo,
            'input'                 => $input,
            'text'                  => $text,
            'inputValue'            => $value,
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Aceptar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $delete = null;
        $titulo = null;
        if ($this->tipo == 'ingreso-eliminar') {
            $ingreso = IngresoSucursal::where('id', $this->ingreso_id)->delete();
            if ($ingreso) {
                $delete = $ingreso;
                $this->ingreso_id = null;
                $titulo = 'Ingreso eliminado exitosamente.';
            }
        }

        if ($this->tipo == 'gasto-eliminar') {
            $gasto = GastoSucursal::where('id', $this->gasto_id)->delete();
            if ($gasto) {
                $delete = $gasto;
                $this->gasto_id = null;
                $titulo = 'Gasto eliminado exitosamente.';
            }
        }

        if ($this->tipo == 'perdida-eliminar') {
            $perdida = PerdidaProducto::where('id', $this->perdida_id)->delete();
            if ($perdida) {
                $delete = $perdida;
                $this->perdida_id = null;
                $titulo = 'Perdida eliminada exitosamente.';
            }
        }

        if ($this->tipo == 'sueldo-eliminar') {
            $sueldo = SueldoSucursal::where('id', $this->sueldo_id)->delete();
            if ($sueldo) {
                $delete = $sueldo;
                $this->sueldo_id = null;
                $titulo = 'Sueldo eliminado exitosamente.';
            }
        }
        
        if ($delete) {
            $this->dispatch('alerta', [
                'title'             => $titulo,
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->dispatch('alerta', [
                'title'             => 'Ha ocurrido un error, intente nuevamente.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        }
    }


    public function vista ($value)
    {
        if ($this->estado == 'rendida' or $this->estado == 'cerrada') {
            $this->dispatch('alerta', [
                'title'             => 'Planilla '.$this->estado.' no puedes hacer modificaciones.',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            $this->modalAction('open');
            $this->titulo = 'Registrar '.$value;
            $this->tipo = $value;
        }
    }

    public function planilla ()
    {        
        if ($this->tramite == 'cerrada') {
            //tipo de pago nula es contado
            $venta = Venta::create([
                'fecha'         => $this->fecha,
                'fecha_pago'    => $this->fecha,
                'nota'          => 'venta calculada',
                'sucursal_id'   => $this->sucursal_id,
                'pagada'        => 1,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);
            //tipo de pago nulo es  diferencia de caja
            $gasto = GastoSucursal::create([
                'sucursal_id'   => $this->sucursal_id,
                'gasto_id'      => $this->gasto_tipo_id,
                'fecha'         => $this->fecha,
                'monto'         => -$this->diferenciaCaja,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);
        }
               
        $productos = Producto::whereIn('id', $this->prodIds)->whereNull('deleted_at')->get();
        foreach ($productos as $item) {
            if ($this->tramite == 'cerrada') {
                $producto           = Producto::find($item->id);
                $nombre             = $producto->nombre;
                $saldo_anterior     = Sucursal::stockAyer($producto->id, $this->sucursal_id, $this->fecha)[0]->saldo ?? 0;
                $recibido           = DetalleTraspaso::cantidad($producto->id, $this->sucursal_id, $this->fecha)[0]->cantidad ?? 0;
                $saldo_teorico      = $saldo_anterior+$recibido;
                $saldo_registrado   = Sucursal::stockHoy($producto->id, $this->sucursal_id, $this->fecha)[0]->saldo ?? 0;
                $venta_calculda     = round($saldo_teorico-$saldo_registrado, 2);
                $valor_unitario     = Producto::precio($producto->id, $this->fecha);
                $total              = $venta_calculda*$valor_unitario;

                $ventasCredito = DetalleVenta::selectRaw('sum(detalle_ventas.cantidad) as ventacredito')
                            ->leftJoin('ventas', 'detalle_ventas.venta_id', '=', 'ventas.id')
                            ->where('ventas.nota', '<>', 'venta calculada')
                            ->where('detalle_ventas.producto_id', $producto->id)
                            ->where('ventas.sucursal_id', $this->sucursal_id)
                            ->where('ventas.fecha', $this->fecha)
                            ->whereNull('ventas.deleted_at')
                            ->whereNull('detalle_ventas.deleted_at')
                            ->get();

                $ventaCalculada = $venta_calculda-$ventasCredito[0]->ventacredito;
                if ($ventaCalculada!=0) {
                    DetalleVenta::create([
                        'cantidad'          => $ventaCalculada,
                        'venta_id'          => $venta->id,
                        'producto_id'       => $producto->id,
                        'precio_unitario'   => $valor_unitario,
                        'created_at'        => now(),
                        'updated_at'        => now()
                    ]);
                }

            }
            $stock = StockSucursal::where('producto_id', $item->id)
                ->where('sucursal_id', $this->sucursal_id)
                ->where('fecha', $this->fecha)
                ->whereNull('deleted_at')
                ->first();

            if (!$stock) {
                StockSucursal::create([
                    'fecha'       => $this->fecha,
                    'sucursal_id' => $this->sucursal_id,
                    'producto_id' => $item->id,
                    'saldo'       => 0,
                    'created'     => now(),
                    'updated'     => now(),
                ]);
            }
        }
     
        $planilla = Planilla::where('sucursal_id', $this->sucursal_id)->where('fecha', $this->fecha)->first();
        if ($planilla) {
            $planilla->estado       = $this->tramite;
            $planilla->responsable  = Auth::user()->name;
            $planilla->save();
        } else {
            $planilla = Planilla::create([
                'fecha'         => $this->fecha,
                'estado'        => $this->tramite,
                'sucursal_id'   => $this->sucursal_id,
                'responsable'   => Auth::user()->name,
            ]);
        }

        $this->estado = Planilla::find($planilla->id)->estado;

        $this->dispatch('alerta', [
            'title'             => 'Planilla '.$this->tramite.' exitosamente',
            'timer'             => 3000,
            'icon'              => 'success',
            'toast'             => true,
            'showConfirmButton' => false,
            'position'          => 'top-right'
        ]);
    }

    public function guardar ($value)
    {
        if ($this->tipo == 'gasto') {
            $this->validate([
                'sucursal_id'   => 'required|integer',
                'gasto_tipo_id' => 'required|integer',
                'fecha'         => 'required',
                'monto'         => 'required|numeric|min:0'
            ]);

            $tipoGasto = GastoSucursal::create([
                'sucursal_id'   => $this->sucursal_id,
                'gasto_id'      => $this->gasto_tipo_id,
                'fecha'         => $this->fecha,
                'monto'         => $this->monto,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

            if ($tipoGasto) {
                $this->dispatch('alerta', [
                    'title'             => 'Gasto registrado exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);

                $this->gastoSucursal();
                $this->modalAction('close');
                $this->reset([
                    'gasto_tipo_id', 
                    'monto',
                ]);
            }
        }

        if ($this->tipo == 'ingreso') {
            $this->validate([
                'sucursal_id'       => 'required|integer',
                'ingreso_tipo_id'   => 'required|integer',
                'fecha'             => 'required',
                'monto'             => 'required|numeric|min:0'
            ]);

            $tipoIngreso = IngresoSucursal::create([
                'sucursal_id'   => $this->sucursal_id,
                'ingreso_id'    => $this->ingreso_tipo_id,
                'fecha'         => $this->fecha,
                'monto'         => $this->monto,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

            if ($tipoIngreso) {
                $this->dispatch('alerta', [
                    'title'             => 'Ingreso registrado exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);

                $this->ingresoSucursal();
                $this->modalAction('close');
                $this->reset([
                    'ingreso_tipo_id', 
                    'monto',
                ]);
            }
        }

        if ($this->tipo == 'perdida') {
            $this->validate([
                'sucursal_id'           => 'required|integer',
                'producto_perdida_id'   => 'required|integer',
                'perdida_id'            => 'required|integer',
                'fecha'                 => 'required',
                'monto'                 => 'required|numeric|min:0',
                'cantidad'              => 'required|numeric|min:0'
            ]);

            $tipoPerdida = PerdidaProducto::create([
                'sucursal_id'   => $this->sucursal_id,
                'producto_id'   => $this->producto_perdida_id,
                'perdida_id'    => $this->perdida_id,
                'fecha'         => $this->fecha,
                'cantidad'      => $this->cantidad,
                'monto'         => $this->monto,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

            if ($tipoPerdida) {
                $this->dispatch('alerta', [
                    'title'             => 'Perdida registrada exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);

                $this->perdidaSucursal();
                $this->modalAction('close');
                $this->reset([
                    'cantidad', 
                    'producto_perdida_id', 
                    'perdida_id', 
                    'monto',
                ]);
            }
        }

        if ($this->tipo == 'sueldo') {
            $this->validate([
                'sucursal_id'           => 'required|integer',
                'empleado_id'           => 'required|integer',
                'fecha'                 => 'required',
                'monto'                 => 'required|numeric|min:0'
            ]);

            $sueldo = SueldoSucursal::create([
                'sucursal_id'   => $this->sucursal_id,
                'empleado_id'   => $this->empleado_id,
                'fecha'         => $this->fecha,
                'monto'         => $this->monto,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

            if ($sueldo) {
                $this->dispatch('alerta', [
                    'title'             => 'Sueldo registrado exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);

                $this->sueldoSucursal();
                $this->modalAction('close');
                $this->reset([
                    'empleado_id', 
                    'monto',
                ]);
            }
        }
    }

    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
            $this->fecha = date('Y-m-d');
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->tipo  = null;
        }
        $this->resetValidation();
    }

    public function productos()
    {
        $this->productos  = Producto::whereNull('deleted_at')->whereIn('id', $this->prodIds)->orderBy('nombre', 'ASC')->get();
    }
    public function tipoGasto()
    {
        $this->tipoGastos = TipoGasto::whereNull('deleted_at')->get();
    }

    public function tipoIngreso()
    {
        $this->tipoIngresos = TipoIngreso::whereNull('deleted_at')->get();
    }

    public function tipoPerdida()
    {
        $this->tipoPerdidas = TipoPerdida::whereNull('deleted_at')->get();
    }

    public function tipoPago()
    {
        $this->tipoPagos = TipoPago::whereNull('deleted_at')->get();
    }

    public function tipoDocumento()
    {
        $this->tipoDocumentos = TipoDocumento::whereNull('deleted_at')->get();
    }

    public function planillaEstadoSucursal ()
    {
        $estado = Planilla::where('sucursal_id', $this->sucursal_id)
                ->where('fecha', $this->fecha)
                ->first();
        $this->estado = $estado ? $estado->estado == 'abierta' ? 'Sin rendir' : $estado->estado : 'Sin rendir';
    }

    public function gastoSucursal()
    {
        $this->gastos = GastoSucursal::where('sucursal_id', $this->sucursal_id)
                    ->where('fecha', $this->fecha)
                    ->orderByDesc('id')
                    ->whereNull('deleted_at')
                    ->get();
    }

    public function ventaSucursal()
    {
        $this->ventas = Venta::where('sucursal_id', $this->sucursal_id)
                        ->where('fecha', $this->fecha)
                        ->where('nota', null)
                        ->whereNull('deleted_at')
                        ->orderByDesc('id')
                        ->get();
    }

    
    public function sueldoSucursal()
    {
        $this->sueldos = SueldoSucursal::where('sucursal_id', $this->sucursal_id)
                        ->where('fecha', $this->fecha)
                        ->whereNull('deleted_at')
                        ->orderByDesc('id')
                        ->get();
    }

    public function perdidaSucursal()
    {
        $this->perdidas = PerdidaProducto::where('sucursal_id', $this->sucursal_id)
                        ->where('fecha', $this->fecha)
                        ->whereNull('deleted_at')
                        ->orderByDesc('id')
                        ->get();
    }

    public function ingresoSucursal()
    {
        $this->ingresos = IngresoSucursal::where('sucursal_id', $this->sucursal_id)
                        ->where('fecha', $this->fecha)
                        ->whereNull('deleted_at')
                        ->orderByDesc('id')
                        ->get();
    }
    
    public function prodcutoTraspoSucursal()
    {
        $this->productos = Auth::user()->type == 'sucursal' 
        ? Traspaso::selectRaw('productos.*')
            ->join('detalle_traspasos', 'detalle_traspasos.traspaso_id', '=', 'traspasos.id')
            ->join('productos', 'detalle_traspasos.producto_id', '=', 'productos.id')
            ->groupBy('productos.id')
            ->where('traspasos.sucursal_id', Auth::user()->sucursal_id)
            ->where('traspasos.fecha', $this->fecha)
            ->where('detalle_traspasos.deleted_at', null)
            ->where('traspasos.deleted_at', null)
            ->get()
        : Traspaso::selectRaw('productos.*')
            ->join('detalle_traspasos', 'detalle_traspasos.traspaso_id', '=', 'traspasos.id')
            ->join('productos', 'detalle_traspasos.producto_id', '=', 'productos.id')
            ->groupBy('productos.id')
            ->where('traspasos.sucursal_id', $this->sucursal_id)
            ->where('traspasos.fecha', $this->fecha)
            ->where('detalle_traspasos.deleted_at', null)
            ->where('traspasos.deleted_at', null)
            ->get();   
    }

    public function empleadoSucursal()
    {
        $this->empleados = Auth::user()->type == 'sucursal'
        ? Empleado::where('sucursal_id', Auth::user()->sucursal_id)
            ->whereNull('deleted_at')
            ->orderByDesc('id')
            ->get()
        : Empleado::where('sucursal_id', $this->sucursal_id)
            ->whereNull('deleted_at')
            ->orderByDesc('id')
            ->get();
    }

    public function precio($value) {
        if ($value>0) {
            $producto = Producto::find($value);
            if ($producto){
                $this->monto = $producto::precio($value, $this->fecha) ?? 0;
            }
        }
    }
}
