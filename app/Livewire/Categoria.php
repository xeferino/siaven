<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Categoria as TipoCategoria;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;

class Categoria extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar'];

    public $modal = 'hidden';
    public $id;
    #[Validate('required', message: 'El nombre es requerido.')]
    #[Validate('min:3', message: 'El nombre debe contener 3 letras como minimo.')]
    public $nombre;

   
    public $telefono;

    public function render()
    {
        $categorias = TipoCategoria::paginate(10);
        return view('livewire.categoria', compact('categorias'));
    }

    public function guardar() {

        $this->validate([
            'nombre'     => 'required|string|min:3',
        ]);

        $categoria = ($this->id) ? TipoCategoria::updateOrCreate(
            [
                'id' => $this->id,
            ],
            [
                'nombre'        => $this->nombre,
                'created_at'    => now(),
                'updated_at'    => now()
            ])
        : TipoCategoria::create(
            [
                'nombre'        => $this->nombre,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

        if ($categoria) {
            $this->dispatch('alerta', [
                'title'             => ($this->id) ? 'Categoria actualizada exitosamente.' : 'Categoria registrada exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);

            $this->modalAction('close');
            $this->reset(['nombre']);
            $this->resetPage();
        }
    }
    public function editar($id) {
        $this->id = $id;
        $categoria = TipoCategoria::find($this->id);
        $this->nombre       = $categoria->nombre;
        $this->modalAction('open');
    }

    public function alerta($id) {
        $this->id = $id;
        $categoria = TipoCategoria::find($id);
        $this->dispatch('eliminar-categoria',[
            'title'                 => $categoria->nombre,
            'text'                  => '¿Desea Eliminar la categoria?',
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Eliminar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $categoria = TipoCategoria::find($this->id);
        $count = $categoria->productos()->count();
        if ($count>0) {
            $this->dispatch('alerta', [
                'title'             => 'La categoria, no puede ser eliminada, ya esta asociada a un producto',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            if ($categoria->delete()) {
                $this->id = null;
                $this->dispatch('alerta', [
                    'title'             => 'Categoria eliminada exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            } else {
                $this->dispatch('alerta', [
                    'title'             => 'Ha ocurrido un error, intente nuevamente.',
                    'timer'             => 3000,
                    'icon'              => 'error',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            }
        }
    }
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->reset(['nombre']);
        }
        $this->resetValidation();
    }
}
