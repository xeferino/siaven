<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Ingreso as TipoIngreso;
use Livewire\WithPagination;
use Livewire\Attributes\Validate;

class Ingreso extends Component
{
    use WithPagination;
    protected $listeners = ['eliminar'];

    public $modal = 'hidden';
    public $id;
    #[Validate('required', message: 'El nombre es requerido.')]
    #[Validate('min:3', message: 'El nombre debe contener 3 letras como minimo.')]
    public $nombre;

   
    public $telefono;

    public function render()
    {
        $ingresos = TipoIngreso::paginate(10);
        return view('livewire.ingreso', compact('ingresos'));
    }

    public function guardar() {

        $this->validate([
            'nombre'     => 'required|string|min:3',
        ]);

        $ingreso = ($this->id) ? TipoIngreso::updateOrCreate(
            [
                'id' => $this->id,
            ],
            [
                'nombre'        => $this->nombre,
                'created_at'    => now(),
                'updated_at'    => now()
            ])
        : TipoIngreso::create(
            [
                'nombre'        => $this->nombre,
                'created_at'    => now(),
                'updated_at'    => now()
            ]);

        if ($ingreso) {
            $this->dispatch('alerta', [
                'title'             => ($this->id) ? 'Tipo de ingreso actualizado exitosamente.' : 'Tipo de ingreso registrado exitosamente.',
                'timer'             => 3000,
                'icon'              => 'success',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);

            $this->modalAction('close');
            $this->reset(['nombre']);
            $this->resetPage();
        }
    }
    public function editar($id) {
        $this->id = $id;
        $ingreso = TipoIngreso::find($this->id);
        $this->nombre       = $ingreso->nombre;
        $this->modalAction('open');
    }

    public function alerta($id) {
        $this->id = $id;
        $ingreso = TipoIngreso::find($id);
        $this->dispatch('eliminar-ingreso',[
            'title'                 => $ingreso->nombre,
            'text'                  => '¿Desea Eliminar el tipo ingreso?',
            'icon'                  => 'question',
            'toast'                 => false,
            'showConfirmButton'     => true,
            'confirmButtonText'     => 'Eliminar',
            'showCancelButton'      => true,
            'cancelButtonText'      => 'Cancelar',
        ]);
    }

    public function eliminar() {
        $ingreso = TipoIngreso::find($this->id);
        if ($ingreso->sucursales()->count()>0) {
            $this->dispatch('alerta', [
                'title'             => 'El tipo de ingreso, no puede ser eliminado ya esta asociado a una sucursal',
                'timer'             => 3000,
                'icon'              => 'error',
                'toast'             => true,
                'showConfirmButton' => false,
                'position'          => 'top-right'
            ]);
        } else {
            if ($ingreso->delete()) {
                $this->id = null;
                $this->dispatch('alerta', [
                    'title'             => 'Tipo de ingreso eliminado exitosamente.',
                    'timer'             => 3000,
                    'icon'              => 'success',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            } else {
                $this->dispatch('alerta', [
                    'title'             => 'Ha ocurrido un error, intente nuevamente.',
                    'timer'             => 3000,
                    'icon'              => 'error',
                    'toast'             => true,
                    'showConfirmButton' => false,
                    'position'          => 'top-right'
                ]);
            }
        }
    }
    
    public function modalAction($action) {

        if ($action == 'open') {
            $this->modal = 'block';
        }

        if ($action == 'close') {
            $this->modal = 'hidden';
            $this->id = null;
            $this->reset(['nombre']);
        }
        $this->resetValidation();
    }
}
