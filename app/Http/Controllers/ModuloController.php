<?php

namespace App\Http\Controllers;

use App\Models\Gasto;
use Illuminate\Http\Request;
use App\Models\Sucursal;
use App\Models\Cliente;
use App\Models\Compra;
use App\Models\Venta;
use App\Models\Documento;
use App\Models\Empleado;
use App\Models\Ingreso;
use App\Models\Pago;
use App\Models\Perdida;
use App\Models\Proveedor;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\Traspaso;
use App\Models\User AS Usuario;
use Illuminate\Support\Facades\Auth;

class ModuloController extends Controller
{
    public function dashboard()
    {
        return view('dashboard');
    }

    public function tablas()
    {
        $sucursales = Sucursal::count();
        $clientes = Cliente::count();
        $productos = Producto::count();
        $proveedores = Proveedor::count();
        
        return view('tablas', compact('sucursales', 'clientes', 'productos', 'proveedores'));
    }

    public function informes()
    {
        return view('informes');
    }

    public function movimientos()
    {
        $traspasos  = Traspaso::count();
        $compras    = Compra::count();
        $ventas     = Auth::user()->type == 'admin' 
                        ? Venta::whereNull('nota')->count() 
                        : Venta::where('sucursal_id', Auth::user()->sucursal_id)
                          ->whereNull('nota')
                          ->count();
        return view('movimientos', compact('traspasos', 'compras', 'ventas'));
    }

    public function configuraciones()
    {
        $usuarios = Usuario::count();
        $gastos = Gasto::count();
        $pagos = Pago::count();
        $documentos = Documento::count();
        $empleados = Empleado::count();
        $perdidas = Perdida::count();
        $ingresos = Ingreso::count();
        $categorias = Categoria::count();
        
        return view('configuraciones',  compact(
            'usuarios',
            'gastos',
            'pagos',
            'documentos',
            'empleados',
            'perdidas',
            'ingresos',
            'categorias'
        ));
    }
}