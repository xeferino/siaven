<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ["deleted_at"];
    protected $table = 'ingresos';
    protected $guarded = ['id'];

    public function sucursales()
    {
        return $this->hasMany(IngresoSucursal::class, 'ingreso_id', 'id');
    }
}
