<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class GastoSucursal extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ["deleted_at"];
    protected $table = 'gasto_sucursales';
    protected $guarded = ['id'];

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }

    public function tipo()
    {
        return $this->belongsTo(Gasto::class, 'gasto_id', 'id');
    }
}
