<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ["deleted_at"];
    protected $table = 'documentos';
    protected $guarded = ['id'];

    public function Compras()
    {
        return $this->hasMany(Compra::class);
    }

    public function Ventas()
    {
        return $this->hasMany(Venta::class);
    }
}
