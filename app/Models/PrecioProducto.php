<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class PrecioProducto extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ["deleted_at"];
    protected $table = 'precio_productos';
    protected $guarded = ['id'];

    public function producto()
    {
        return $this->belongsTo(Producto::class);
    }
}
