<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Perdida extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ["deleted_at"];
    protected $table = 'perdidas';
    protected $guarded = ['id'];

    public function productos()
    {
        return $this->hasMany(PerdidaProducto::class, 'perdida_id', 'id');
    }
}
