<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class DetalleTraspaso extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ["deleted_at"];
    protected $table = 'detalle_traspasos';
    protected $guarded = ['id'];

    public function traspaso()
    {
        return $this->belongsTo(Traspaso::class);
    }

    public function producto()
    {
        return $this->belongsTo(Producto::class);
    }

    public static function cantidad($producto_id, $sucursal_id, $fecha) {
        return DetalleTraspaso::selectRaw('detalle_traspasos.cantidad, traspasos.fecha ')
                ->leftJoin('traspasos', 'detalle_traspasos.traspaso_id', '=', 'traspasos.id')
                ->where('traspasos.fecha', $fecha)
                ->where('detalle_traspasos.producto_id', $producto_id)
                ->where('traspasos.sucursal_id', $sucursal_id)
                ->whereNull('traspasos.deleted_at')
                ->whereNull('detalle_traspasos.deleted_at')
                ->limit(1)
                ->get();
    }
}
