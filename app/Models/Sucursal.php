<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Models\StockSucursal;

class Sucursal extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ["deleted_at"];
    protected $table = 'sucursales';
    protected $guarded = ['id'];


    public static function stockAyer($producto_id, $sucursal_id, $fecha) {
        return  StockSucursal::selectRaw('saldo, fecha ')
                    ->where('producto_id', $producto_id)
                    ->where('sucursal_id', $sucursal_id)
                    ->where('fecha', '<', $fecha)
                    ->whereNull('deleted_at')
                    ->orderByDesc('fecha')
                    ->limit(1)
                    ->get();
    }

    public static function stockHoy($producto_id, $sucursal_id, $fecha) {
        return  StockSucursal::selectRaw('saldo, fecha ')
                    ->where('producto_id', $producto_id)
                    ->where('sucursal_id', $sucursal_id)
                    ->where('fecha', $fecha)
                    ->whereNull('deleted_at')
                    ->limit(1)
                    ->get();
    }
}
