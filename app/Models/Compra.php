<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Models\DetalleCompra;
class Compra extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ["deleted_at"];
    protected $table = 'compras';
    protected $guarded = ['id'];

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }

    public function proveedor()
    {
        return $this->belongsTo(Proveedor::class);
    }

    public function pago()
    {
        return $this->belongsTo(Pago::class);
    }

    public function documento()
    {
        return $this->belongsTo(Documento::class);
    }

    public static function total($id) {
        $total = DetalleCompra::selectRaw('sum(cantidad*precio_unitario) AS total')
                    ->where('compra_id', $id)
                    ->where('estado', '<>', 1)
                    ->whereNull('deleted_at')
                    ->get();
        return $total[0]->total ?? 0;

    }

    public static function totalGuia($id) {
        $total = DetalleCompra::selectRaw('sum(cantidad*precio_guia) AS total')
                    ->where('compra_id', $id)
                    ->where('estado', '<>', 1)
                    ->whereNull('deleted_at')
                    ->get();
        return $total[0]->total ?? 0;

    }
}
