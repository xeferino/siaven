<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CierrePlanillaDiaria extends Model
{
    use HasFactory;
    
    protected $table = 'cierre_planilla_diarias';
    protected $guarded = ['id'];

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }
}
