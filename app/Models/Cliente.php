<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Sucursal;

class Cliente extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ["deleted_at"];
    protected $table = 'clientes';
    protected $guarded = ['id'];

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }

    public function historial()
    {
        return $this->hasMany(CreditoCliente::class);
    }

    public function credito()
    {
        return $this->hasOne(CreditoCliente::class)->orderByDesc('id');
    }

    public function ventas()
    {
        return $this->hasMany(Venta::class);
    }
}
