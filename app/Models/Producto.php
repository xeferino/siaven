<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Models\PrecioProducto;
use App\Models\DetalleCompra;
use App\Models\Compra;
use App\Models\DetalleVenta;
use App\Models\Venta;

class Producto extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ["deleted_at"];
    protected $table = 'productos';
    protected $guarded = ['id'];

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }

    public function proveedor()
    {
        return $this->belongsTo(Proveedor::class);
    }

    public function historial()
    {
        return $this->hasMany(PrecioProducto::class);
    }

    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }

    public function precios()
    {
        return $this->hasOne(PrecioProducto::class)
                ->where('fecha', '<=', date('Y-m-d'))
                ->whereNull('deleted_at')
                ->orderByDesc('fecha')
                ->limit(1);
    }

    public static  function precio($id, $fecha)
    {
        $precio = PrecioProducto::where('fecha', '<=', $fecha)
                ->where('producto_id', $id)
                ->orderByDesc('fecha')
                ->whereNull('deleted_at')
                ->first();
        if (!$precio) {
            $precio = PrecioProducto::where('fecha', '>=', $fecha)
                ->where('producto_id', $id)
                ->orderByDesc('fecha')
                ->whereNull('deleted_at')
                ->first();
        }
        return $precio->precio_venta;
    }

    public static  function stock($producto_id, $fecha = null)
    {
        $stock1 = DetalleCompra::selectRaw('sum(detalle_compras.cantidad) as ingreso')
                    ->leftJoin('compras', 'detalle_compras.compra_id', '=', 'compras.id')
                    ->where('compras.fecha', '<=', $fecha ?? date('Y-m-d'))
                    ->where('compras.estado', '<>', 1)
                    ->where('detalle_compras.estado', '<>', 1)
                    ->where('detalle_compras.producto_id', $producto_id)
                    ->whereNull('compras.deleted_at')
                    ->whereNull('detalle_compras.deleted_at')
                    ->get();

        $stock2 = DetalleVenta::selectRaw('sum(detalle_ventas.cantidad) as egreso')
                    ->leftJoin('ventas', 'detalle_ventas.venta_id', '=', 'ventas.id')
                    ->where('ventas.fecha', '<=', $fecha ?? date('Y-m-d'))
                    ->where('ventas.estado', '<>', 1)
                    ->where('ventas.sucursal_id', 1)
                    ->where('detalle_ventas.producto_id', $producto_id)
                    ->whereNull('ventas.deleted_at')
                    ->whereNull('detalle_ventas.deleted_at')
                    ->get();

        $stock3 = DetalleTraspaso::selectRaw('sum(detalle_traspasos.cantidad) as egreso')
                    ->leftJoin('traspasos', 'detalle_traspasos.traspaso_id', '=', 'traspasos.id')
                    ->where('traspasos.fecha', '<=', $fecha ?? date('Y-m-d'))
                    ->where('detalle_traspasos.producto_id', $producto_id)
                    ->whereNull('traspasos.deleted_at')
                    ->whereNull('detalle_traspasos.deleted_at')
                    ->get();

        return ($stock1[0]->ingreso - $stock2[0]->egreso - $stock3[0]->egreso);
	}

    public function compras()
    {
        return $this->hasMany(DetalleCompra::class, 'producto_id', 'id');
    }

    public function ventas()
    {
        return $this->hasMany(DetalleVenta::class, 'producto_id', 'id');
    }

    public function stockSucursal()
    {
        return $this->hasMany(StockSucursal::class, 'producto_id', 'id');
    }
}
