<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Gasto extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ["deleted_at"];
    protected $table = 'gastos';
    protected $guarded = ['id'];

    public function sucursales()
    {
        return $this->hasMany(GastoSucursal::class, 'gasto_id', 'id');
    }
}
