<?php
if (! function_exists('current_user')) {
    function current_user()
    {
        return auth()->user();
    }
}


if (! function_exists('moneda')) {
    function moneda($value) {
        return "$ " . number_format($value, 2, ",", ".");
    }
}