<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ModuloController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Route::view('/', 'welcome');
Route::redirect('/', '/login', 301);

/* Route::get("dashboard", [ModuloController::class, "dashboard"])
    ->middleware(['auth', 'verified'])
    ->name('dashboard'); */

    Route::group(['middleware' => ['auth']], function () {

        Route::get("tablas", [ModuloController::class, "tablas"])
            ->middleware('roles:admin,despachador')
            ->name('tablas');

        Route::get("movimientos", [ModuloController::class, "movimientos"])
            ->middleware('roles:admin,sucursal,despachador')
            ->name('movimientos');

        Route::get("informes", [ModuloController::class, "informes"])
            ->middleware('roles:admin,sucursal')
            ->name('informes');

        Route::get("configuraciones", [ModuloController::class, "configuraciones"])
            ->middleware('roles:admin')
            ->name('configuraciones');

        /* Route::view('profile', 'profile')
            ->name('profile'); */

        Route::get('tablas/sucursales', function () {
            return view('sucursales');
        })->middleware('roles:admin')
        ->name('sucursales');

        Route::get('tablas/clientes', function () {
            return view('clientes');
        })->middleware('roles:admin')
        ->name('clientes');

        Route::get('tablas/clientes/historial/{id}', function ($id) {
            return view('clientes_historial', [
                'id' => $id
            ]);
        })->middleware('roles:admin')
        ->name('clientes.historial');
        

        Route::get('tablas/proveedores', function () {
            return view('proveedores');
        })->middleware('roles:admin')
        ->name('proveedores');

        Route::get('tablas/productos', function () {
            return view('productos');
        })->middleware('roles:admin,despachador')
        ->name('productos');

        Route::get('movimientos/traspasos', function () {
            return view('traspasos');
        })->middleware('roles:admin,despachador')
        ->name('traspasos');

        Route::get('movimientos/traspasos/detalle/{id}', function ($id) {
            return view('detalle_traspasos', [
                'id' => $id
            ]);
        })->middleware('roles:admin,despachador')
        ->name('traspasos.detalles');

        Route::get('movimientos/compras', function () {
            return view('compras');
        })->middleware('roles:admin')
        ->name('compras');

        Route::get('movimientos/compras/detalle/{id}', function ($id) {
            return view('detalle_compras', [
                'id' => $id
            ]);
        })->middleware('roles:admin')
        ->name('compras.detalles');

        Route::get('movimientos/ventas', function () {
            return view('ventas');
        })->middleware('roles:admin,sucursal')
        ->name('ventas');

        Route::get('movimientos/ventas/detalle/{id}', function ($id) {
            return view('detalle_ventas', [
                'id' => $id
            ]);
        })->middleware('roles:admin,sucursal')
        ->name('ventas.detalles');

        Route::get('movimientos/solicitudes', function () {
            return view('solicitudes');
        })->middleware('roles:admin,sucursal')
        ->name('solicitudes');

        Route::get('informes/planillas', function () {
            return view('planillas');
        })->middleware(['auth', 'roles:admin,sucursal'])
        ->name('planillas');

        Route::get('informes/inventario-valorado', function () {
            return view('inventario_valorado');
        })->middleware(['auth', 'roles:admin'])
        ->name('informes.inventario');

        Route::get('informes/ventas-productos', function () {
            return view('ventas_productos');
        })->middleware(['auth', 'roles:admin'])
        ->name('informes.productos');

        Route::get('informes/ventas-clientes', function () {
            return view('ventas_clientes');
        })->middleware(['auth', 'roles:admin'])
        ->name('informes.clientes');

        Route::get('informes/resumen-rendiciones', function () {
            return view('resumen_rendiciones');
        })->middleware(['auth', 'roles:admin'])
        ->name('informes.resumen');

        Route::get('informes/rentabilidad-sucursal', function () {
            return view('rentabilidad_sucursal');
        })->middleware(['auth', 'roles:admin'])
        ->name('informes.rentabilidad');

        Route::get('informes/ingresos-egresos-sucursal', function () {
            return view('libros');
        })->middleware(['auth', 'roles:admin'])
        ->name('informes.libros');

        Route::get('informes/resumen-despacho', function () {
            return view('resumen_despacho');
        })->middleware(['auth', 'roles:admin'])
        ->name('informes.despacho');

        Route::get('informes/solicitud-producto', function () {
            return view('solicitud_productos');
        })->middleware(['auth', 'roles:admin'])
        ->name('informes.solicitud');
        
        Route::get('informes/cxc', function () {
            return view('cxc');
        })->middleware('roles:admin')
        ->name('informes.cxc');

        Route::get('configuraciones/usuarios', function () {
            return view('usuarios');
        })->middleware('roles:admin')
        ->name('usuarios');

        Route::get('configuraciones/tipos/gastos', function () {
            return view('gastos');
        })->middleware('roles:admin')
        ->name('gastos');

        Route::get('configuraciones/tipos/pagos', function () {
            return view('pagos');
        })->middleware('roles:admin')
        ->name('pagos');

        Route::get('configuraciones/tipos/documentos', function () {
            return view('documentos');
        })->middleware('roles:admin')
        ->name('documentos');

        Route::get('configuraciones/tipos/perdidas', function () {
            return view('perdidas');
        })->middleware('roles:admin')
        ->name('perdidas');

        Route::get('configuraciones/tipos/ingresos', function () {
            return view('ingresos');
        })->middleware('roles:admin')
        ->name('ingresos');

        Route::get('configuraciones/empleados', function () {
            return view('empleados');
        })->middleware('roles:admin')
        ->name('empleados');

        Route::get('configuraciones/categorias', function () {
            return view('categorias');
        })->middleware('roles:admin')
        ->name('categorias');
    });
require __DIR__.'/auth.php';
