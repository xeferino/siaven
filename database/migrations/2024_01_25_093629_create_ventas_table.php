<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->id();
            $table->date('fecha')->nullable();
            $table->foreignId('cliente_id')->nullable();
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->foreignId('factura_id')->nullable();
            $table->foreign('factura_id')->references('id')->on('facturas');
            $table->decimal('monto', 10, 2)->nullable();
            $table->decimal('cuadreguia', 10, 2)->nullable();
            $table->decimal('descuento', 10, 2)->nullable();
            $table->integer('nro_guia')->nullable();
            $table->boolean('estado')->default('0')->nullable();
            $table->boolean('pagada')->default('0')->nullable();
            $table->date('fecha_pago')->nullable();
            $table->foreignId('pago_id')->nullable();
            $table->foreign('pago_id')->references('id')->on('pagos');
            $table->string('nro_doc')->nullable();
            $table->string('nro_cheque_banco')->nullable();
            $table->foreignId('sucursal_id')->nullable();
            $table->foreign('sucursal_id')->references('id')->on('sucursales');
            $table->foreignId('documento_id')->nullable();
            $table->foreign('documento_id')->references('id')->on('documentos');
            $table->string('nota')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ventas');
    }
};
