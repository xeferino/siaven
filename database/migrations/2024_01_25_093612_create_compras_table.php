<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('compras', function (Blueprint $table) {
            $table->id();
            $table->date('fecha')->nullable();
            $table->foreignId('proveedor_id')->nullable();
            $table->foreign('proveedor_id')->references('id')->on('proveedores');
            $table->decimal('monto', 10, 2)->nullable();
            $table->boolean('estado')->default('0')->nullable();
            $table->boolean('pagada')->default('0')->nullable();
            $table->date('fecha_pago')->nullable();
            $table->foreignId('pago_id')->nullable();
            $table->foreign('pago_id')->references('id')->on('pagos');
            $table->foreignId('documento_id')->nullable();
            $table->foreign('documento_id')->references('id')->on('documentos');
            $table->string('nro_documento')->nullable();
            $table->string('nro_cheque')->nullable();
            $table->string('nro_cheque_banco')->nullable();
            $table->string('nota')->nullable();
            $table->foreignId('sucursal_id')->nullable();
            $table->foreign('sucursal_id')->references('id')->on('sucursales');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('compras');
    }
};
