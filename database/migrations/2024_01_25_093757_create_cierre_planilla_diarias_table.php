<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cierre_planilla_diarias', function (Blueprint $table) {
            $table->id();
            $table->date('fecha')->nullable();
            $table->enum('estado',['abierta', 'rendida', 'cerrada'])->nullable();
            $table->foreignId('sucursal_id')->nullable();
            $table->foreign('sucursal_id')->references('id')->on('sucursales');
            $table->string('responsable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cierre_planilla_diarias');
    }
};
