<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        

         $sucursal = \App\Models\Sucursal::create([
            'nombre'        => 'Casa Matriz',
            'direccion'     => 'Bodega despachadora',
            'telefono'      => null,
            'created_at'    => now(),
            'updated_at'    => now(),
            'deleted_at'    => null,
         ]);

         \App\Models\User::factory()->create([
            'id'             => 1,
            'name'           => 'SuperAdmin',
            'email'          => 'siaven.admin@test.com',
            'password'       => '$2y$10$whYgg3JU0QGlc0Y6mKAXGeK9Xn0gwYLvfWGv8K2CeRxEw8n5ix2qO', // *3153102124%
            'type'           => 'admin',
            'activo'         => 1,
            'remember_token' => 'yvx9HGbeSY3neo6j99NyQXCHtiU6HGc8itogXmYsapFC5AQN4wHmUweJgllt',
            'sucursal_id'    => $sucursal->id,
            'created_at'     => now(),
            'updated_at'     => now(),
            'deleted_at'     => null,
         ]);
    }
}
